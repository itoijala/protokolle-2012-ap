\input{header/report.tex}

\SetExperimentNumber{311}

\addbibresource{lit.bib}

\begin{document}
  \maketitlepage{Der Hall-Effekt und Elektrizitätsleitung in Metallen}{15.05.2012}{22.05.2012}
  \section{Ziel}
    Ziel des Versuchs ist es, durch die Messung der makroskopischen Größen des elektrischen Widerstands und der Hall-Spannung mehrere mikroskopischen Eigenschaften verschiedener Metalle zu berechnen.
  \section{Theorie}
    Metalle gehören zu den kristallinen Festkörpern und sind durch ihre hohe elektrische Leitfähigkeit ausgezeichnet.
    Die äußersten Elektronen der Atome bilden ein quantenmechanisches System und können sich frei in dem Ionengitter bewegen.
    Aufgrund des Pauli-Prinzips kann jeweils nur ein Elektron eines Systems einen Quantenzustand besetzen.
    Dies führt dazu, dass nur zwei Elektronen (mit unterschiedlichem Spin) die gleiche Energie haben können.
    Aus diesem Grund teilt sich das Energiespektrum der Elektronen in näherungsweise kontinuierliche Bänder und verbotene Zonen auf, in denen keine erlaubten Zustände existieren (Abbildung \ref{fig:bänder}).
    \begin{figure}
      \includegraphics{graphic/bänder.pdf}
      \caption{Beispiel eines Energiespektrums \cite{anleitung311}}
      \label{fig:bänder}
    \end{figure}
    Da bei Metallen das höchste besetze Band nicht vollständig gefüllt ist, können Elektronen bei angelegtem elektrischen Feld sich frei durch das Metall bewegen, indem sie vom Feld beschleunigt werden und zwischen den quasi-kontinuierlichen Energieniveaus des Leitungsbands wechseln.
    Bei Isolatoren ist das oberste Band leer und der Abstand zum darunterliegenden Band ist zu hoch, um von den Elektronen überwunden zu werden.
    Wenn die Ionen exakt periodisch angeordnet sind, wechselwirken die Leitungselektronen nicht mit den Atomrümpfen oder untereinander, die elektrische Leitfähigkeit wäre unendlich hoch.
    Da in natürlichen Kristallen immer Fehler vorkommen, entsteht durch die Wechselwirkung mit den Atomrümpfen eine Reibung, die als elektrischer Widerstand beobachtbar ist.
    \subsection{Berechnung der Leitfähigkeit}
      Aufgrund von Kristallfehlern stoßen die Elektronen mit Atomrümpfen und Strukturdefekten zusammen.
      Die mittlere Flugzeit $\mean{\tau}$ wird als Mittelwert der Zeit definiert, in der sich die Elektronen frei bewegen können.
      Ist das elektrische Feld $\v{E}$ angelegt, beträgt die Beschleunigung eines Elektrons
      \begin{eqn}
        \v{a} = - \frac{e}{m} \v{E} ,
      \end{eqn}
      wobei $e$ die Elementarladung und $m$ die Masse des Elektrons ist.
      Die Geschwindigkeitsänderung des Elektrons in der stoßfreien Zeit $\mean{\tau}$ ist
      \begin{eqn}
        \mean{\Del{\v{v}}} = - \frac{e}{m} \v{E} \mean{\tau} .
      \end{eqn}
      Da das Elektron nach einem Stoß in eine zufällige Richtung fliegt, fängt die Beschleunigung nach jedem Stoß von vorne an.
      Die mittlere Driftgeschwindigkeit $\mean{\v{v_\t{d}}}$ wird wegen der konstanten Beschleunigung als
      \begin{eqn}
        \mean{\v{v_\t{d}}} = \frac{1}{2} \mean{\Del{\v{v}}}
      \end{eqn}
      definiert.
      Sie beschreibt die mittlere Geschwindigkeit der Elektronen in Richtung des Stroms.

      In einem homogenen Leiter der Querschnittsfläche $Q$ und der Länge $L$ kann die Stromdichte durch
      \begin{eqn}
        \j = - e n \mean{\v{v_\t{d}}} = \frac{1}{2} \frac{e^2}{m} n \mean{\tau} E \eqlabel{eqn:j}
      \end{eqn}
      berechnet werden, wenn pro Volumeneinheit $n$ Elektronen vorhanden sind.
      Da der Leiter homogen ist, kann $\j$ durch den Strom $I$ und $E$ durch die angelegte Spannung $U$ ausgedrückt werden.
      Dann gilt
      \begin{eqn}
        I = \frac{1}{2} \frac{e^2}{m} \frac{Q}{L} n \mean{\tau} U ,
      \end{eqn}
      wobei die Proportionalitätskonstante zwischen $I$ und $U$ als elektrische Leitfähigkeit
      \begin{eqn}
        S = \frac{1}{2} \frac{e^2}{m} \frac{Q}{L} n \mean{\tau}
      \end{eqn}
      bezeichnet wird.
      Für den elektrischen Widerstand gilt
      \begin{eqn}
        R = \frac{1}{S} = 2 \frac{m}{e^2} \frac{L}{Q} \frac{1}{n \mean{\tau}} . \eqlabel{eqn:R}
      \end{eqn}
      Um geometrieunabhängige Größen zu bekommen, werden die spezifische Leitfähigkeit $\sigma$ und der spezifische Widerstand $\rho$ definiert.
      Es gilt
      \begin{eqns}[rCcCl]
        \sigma &=& S \frac{L}{Q} &=& \frac{1}{2} \frac{e^2}{m} n \mean{\tau} \\
        \rho &=& R \frac{Q}{L} &=& 2 \frac{m}{e^2} \frac{1}{n \mean{\tau}} .
      \end{eqns}
    \subsection{Der Hall-Effekt}
      Der Hall-Effekt kann an einer Stromdurchflossenen Leiterplatte (Dicke $d$, Breite $b$) in einem homogenen Magnetfeld $\v{B}$ als Spannung (Hall-Spannung $U_\t{H}$) quer zur Stromrichtung beobachtet werden (Abbildung \ref{fig:hall}).
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/hall.pdf}
        \caption{Aufbau zur Messung der Hall-Spannung \cite{anleitung311}}
        \label{fig:hall}
      \end{figure}
      Die Hall-Spannung entsteht durch die Lorentzkraft, die aufgrund des Magnetfelds auf die Elektronen wirkt.
      Die Elektronen werden von der Kraft
      \begin{eqn}
        F_\t{L} = e \mean{v_\t{d}} B
      \end{eqn}
      in Richtung des Punkts B abgelenkt.
      Diese Ladung erzeugt ein elektrisches Feld $\v{E}$, das in $y$-Richtung zeigt.
      Es stellt sich ein Gleichgewicht ein, wenn die Lorentzkraft und die Coloumbkraft $F_\t{C}$ gleich groß sind:
      \begin{eqns}
        F_\t{L} &=& F_\t{C} \\
        e \mean{v_\t{d}} B &=& e E .
      \end{eqns}
      Für die Hall-Spannung gilt
      \begin{eqn}
        U_\t{H} = E b = \mean{v_\t{d}} B b ,
      \end{eqn}
      wobei sich $\mean{v_\t{d}}$ durch den Strom $I_\t{q}$ berechnen lässt.
      Es gilt
      \begin{eqn}
        \j = \frac{I_\t{q}}{b d} = - e n \mean{v_\t{d}} ,
      \end{eqn}
      woraus für die Hall-Spannung
      \begin{eqn}
        U_\t{H} = - \frac{1}{e} \frac{B I_\t{q}}{d} \frac{1}{n}
      \end{eqn}
      und für $n$
      \begin{eqn}
        n = - \frac{1}{e} \frac{B I_\t{q}}{d U_\t{H}} \eqlabel{eqn:n}
      \end{eqn}
      folgt.
    \subsection{Berechnung mikroskopischer Größen}
      Durch Messung der Hall-Spannung $U_\t{H}$ und des Widerstands $R$ können mit \eqref{eqn:R} und \eqref{eqn:n} die Elektronendichte $n$ und die mittlere Flugzeit $\mean{\tau}$ berechnet werden:
      \begin{eqns}
        \mean{\tau} &=& - 2 \frac{m}{e} \frac{L d}{Q} \frac{U_\t{H}}{B R I_\t{q}} \\
        &=& 2 \frac{m}{e^2} \frac{L}{Q} \frac{1}{R n} . \eqlabel{eqn:tau}
      \end{eqns}
      Weitere interessante mikroskopische Größen sind die mittlere freie Weglänge $\mean{l}$, die Totalgeschwindigkeit $v_\t{t}$, die mittlere Driftgeschwindigkeit $\mean{v_\t{d}}$, die Beweglichkeit $\mu$ und die Anzahl der Ladungsträger pro Atom $z$.

      Die mittlere freie Weglänge $\mean{l}$ ist der Weg, den ein Elektron im Mittel zwischen Stößen zurücklegen kann.
      Sie ist gegeben durch
      \begin{eqn}
        \mean{l} = v_\t{t} \mean{\tau} , \eqlabel{eqn:l}
      \end{eqn}
      wobei $v_\t{t}$ die Totalgeschwindigkeit ist.
      Die Totalgeschwindigkeit entsteht durch die Wärmebewegung der Materie und ist unabhängig von der Driftgeschwindigkeit $\mean{v_\t{d}}$.
      Die Energieverteilung der Elektronen folgt der Fermi-Dirac-Verteilung.
      Die Wahrscheinlichkeit, ein Elektron mit der Energie $E + \dif{E}$ zu finden, beträgt
      \begin{eqn}
        \f{f}(E) \dif{E} = \cfrac{1}{\exp(\cfrac{E - E_\t{F}}{k_\t{B} T}) + 1} \dif{E} ,
      \end{eqn}
      wobei $k_\t{B}$ die Boltzmannkonstante und $T$ die Temperatur des Materials ist.
      Die Fermi-Energie $E_\t{F}$ ist gegeben durch
      \begin{eqn}
        E_\t{F} = \frac{h^2}{2 m} \g(\frac{3}{8 \PI} n)^{\! \frac{2}{3}} ,
      \end{eqn}
      wobei $h$ das Plancksche Wirkungsquantum ist.
      Die Leitungselektronen sind die, die eine Energie $E \approx E_\t{F}$ haben.
      Daraus folgt für die Totalgeschwindigkeit
      \begin{eqn}
        v_\t{t} = \sqrt{\frac{2 E_\t{F}}{m}} = \frac{h}{m} \g(\frac{3}{8 \PI} n)^{\! \frac{1}{3}} . \eqlabel{eqn:v_t}
      \end{eqn}

      Die Beweglichkeit der Elektronen $\mu$ ist definiert als die Proportionalitätskonstante zwischen $\mean{\v{v_\t{d}}}$ und $\v{E}$:
      \begin{eqn}
        \mean{\v{v_\t{d}}} = \mu \v{E} .
      \end{eqn}
      Aus \eqref{eqn:j} folgt
      \begin{eqn}
        \mu = - \frac{1}{2} \frac{e}{m} \mean{\tau} \eqlabel{eqn:mu}
      \end{eqn}
      und
      \begin{eqn}
        \mean{v_\t{d}} = - \frac{\j}{e n} . \eqlabel{eqn:v_d}
      \end{eqn}
      Für die Anzahl $z$ der Ladungsträger pro Atom ergibt sich
      \begin{eqn}
        z = n \frac{M}{\rho} , \eqlabel{eqn:z}
      \end{eqn}
      wobei $M$ die Masse eines Atoms und $\rho$ die Dichte des Materials ist.
    \subsection{Löcherleiter}
      Bei manchen Materialien überlappen sich die Energiebänder, was es den Elektronen ermöglicht, vom unteren in das obere Band zu wechseln.
      Dabei verbleiben Lücken im unteren Band, die sich wie positive Ladungsträger verhalten und Löcher genannt werden.
      Sie unterliegen auch dem Hall-Effekt, was es durch Bestimmung des Vorzeichens der Hall-Spannung möglich macht, die Leiterart zu bestimmen.
      Dies ist allerdings nur dann möglich, wenn die Elektronen nur eine kleine Rolle bei der Elektrizitätsleitung spielen, was aber auf viele Materialien zutrifft.
  \section{Aufbau und Durchführung}
    In der ersten Messung wird das Magnetfeld $B$ des verwendeten Spulenpaars als Funktion des Stroms $I$ gemessen.
    Hierzu wird das Magnetfeld bei verschiedenen Strömen mit einem digitalen Gaußmeter gemessen.
    Der Strom wird erst von null bis zum Maximalstrom gedreht un dann zurück zu null, es ergibt sich eine Hysteresekurve.

    In der zweiten Messung werden die Abmessungen der verwendeten Drähte und Leiterplatten bestimmt.
    Die Dicken $2r$ der Drähte und $d$ der Platten werden mit einer Mikrometerschraube gemessen.
    Die Längen $L$ der Drähte sind auf der Apparatur aufgeführt.

    Bei der dritten Messung werden die Widerstände $R$ der Drähte gemessen.
    Hierzu werden sie an ein digitales Widerstandsmessgerät angeschlossen.

    Bei der letzten Messung wird die Hall-Spannung an den Leiterplatten gemessen.
    Hierzu werden die Leiterplatten in das näherungsweise homogene Magnetfeld zwischen zwei Spulen gebracht.
    Zwei Konstantstromgeräte werden verwendet, um die Spulen mit dem Strom $I_\t{Spule}$ und die Leiterplatte mit dem Strom $I_\t{Speise}$ zu versorgen.
    Die Hall-Spannung $U_\t{H}$ wird mit einem angeschlossenen digitalen Voltmeter gemessen.
    Der Maximale Spulenstrom wird eingestellt und der Speisestrom von null bis zum Maximum gedreht.
    Dann wird der Speisestrom auf dem Maximalwert gelassen und der Spulenstrom runtergedreht.

    Im Versuch wurder die Materialien Silber (\ce{Ag}), Kupfer (\ce{Cu}), Tantal (\ce{Ta}) und Zink (\ce{Zn}) verwendet.
  \section{Messwerte}
    In Tabelle \ref{tab:B} sind die Messwerte der Hysteresemessung aufgeführt.
    Dabei wurde erst der Stroms erhöht und dann verkleinert.
    In den Tabellen \ref{tab:L}, \ref{tab:R} und \ref{tab:M} sind die gemessenen Abmessungen der Proben, die gemessenen Widerstände und Literaturwerte der Atommassen und Materialdichten aufgeführt.
    Die Messwerte der Hall-Spannungsmessungen sind in den Tabellen \ref{tab:Ag} bis \ref{tab:Zn} aufgeführt.
    \begin{table}
      \input{table/B.tex}
      \caption{Messwerte für die Hysteresekurve}
      \label{tab:B}
    \end{table}
    \begin{table}
      \input{table/L.tex}
      \caption{Abmessungen der Proben}
      \label{tab:L}
    \end{table}
    \begin{table}
      \input{table/R.tex}
      \caption{Widerstände der verwendeten Elemente}
      \label{tab:R}
    \end{table}
    \begin{table}
      \input{table/M.tex}
      \caption{Massen \cite{nist-chemistry-webbook} und Dichten \cite{nist-star} der Elemente}
      \label{tab:M}
    \end{table}
    \begin{table}
      \input{table/Ag.tex}
      \caption{Hallmessung für Silber}
      \label{tab:Ag}
    \end{table}
    \begin{table}
      \input{table/Cu.tex}
      \caption{Hallmessung für Kupfer}
      \label{tab:Cu}
    \end{table}
    \begin{table}
      \input{table/Ta.tex}
      \caption{Hallmessung für Tantal}
      \label{tab:Ta}
    \end{table}
    \begin{table}
      \input{table/Zn.tex}
      \caption{Hallmessung für Zink}
      \label{tab:Zn}
    \end{table}
  \section{Auswertung}
    \input{statistics.tex}
    \input{linregress.tex}
    Die Mittelwerte der Abmessungen $r$ und $d$ der Proben sind in Tabelle \ref{tab:r} aufgeführt.
    Die Messwerte der Hysteresemessung sind in Abbildung \ref{fig:B} geplottet.
    Dabei sind die roten Messwerte für den höher werdenden Strom und die blauen für den kleiner werdenden.

    Um $n$ zu berechnen, werden die linearen Regressionen
    \begin{eqns}
      U_\t{H} &=& A_I I_\t{Speise} + C_I \\
      U_\t{H} &=& A_B B + C_B
    \end{eqns}
    verwendet.
    Aus den Steigungen kann $n$ durch
    \begin{eqns}
      n_I &=& - \frac{1}{e d} \frac{B}{A_I} \\
      n_B &=& - \frac{1}{e d} \frac{I_\t{Speise}}{A_B}
    \end{eqns}
    bestimmt werden.
    Alle anderen zu berechnenden mikroskopischen Größen können aus $n$ durch die Formeln \eqref{eqn:tau}, \eqref{eqn:v_t}, \eqref{eqn:l}, \eqref{eqn:mu}, \eqref{eqn:v_d} für $j = \SI{1}{\ampere\per\square\milli\meter}$ und \eqref{eqn:z} bestimmt werden.
    Die liearen Regressionen sind in den Abbildungen \ref{fig:Ag-I} bis \ref{fig:Zn-B} und ihre Ergebnisse in den Tabellen \ref{tab:regress-Ag}, \ref{tab:regress-Cu}, \ref{tab:regress-Ta} und \ref{tab:regress-Zn} aufgetragen.
    Die berechneten mikroskopischen Größen stehen in den Tabellen \ref{tab:result-Ag}, \ref{tab:result-Cu}, \ref{tab:result-Ta} und \ref{tab:result-Zn}.
    \begin{table}
      \input{table/r.tex}
      \caption{Abmessungen der Proben}
      \label{tab:r}
    \end{table}
    \begin{figure}
      \includegraphics[width=\textwidth]{graph/B.pdf}
      \caption{Eingezeichnete Hysteresemesswerte}
      \label{fig:B}
    \end{figure}
    \begin{figure}
      \includegraphics[width=\textwidth]{graph/Ag-I.pdf}
      \caption{Lineare Regression für das Verhältnis zwischen Hallspannung und Speisestrom für Silber}
      \label{fig:Ag-I}
    \end{figure}
    \begin{figure}
      \includegraphics[width=\textwidth]{graph/Ag-B.pdf}
      \caption{Lineare Regression für das Verhältnis zwischen Hallspannung und Magnetfeld für Silber}
      \label{fig:Ag-B}
    \end{figure}
    \begin{table}
      \OverfullCenter{\input{table/regress-Ag.tex}}
      \caption{Ergebnisse der linearen Regressionen für Silber}
      \label{tab:regress-Ag}
    \end{table}
    \begin{table}
      \OverfullCenter{\input{table/result-Ag.tex}}
      \caption{Errechnete mikroskopische Größen für Silber}
      \label{tab:result-Ag}
    \end{table}
    \begin{figure}
      \includegraphics[width=\textwidth]{graph/Cu-I.pdf}
      \caption{Lineare Regression für das Verhältnis zwischen Hallspannung und Speisestrom für Kupfer}
      \label{fig:Cu-I}
    \end{figure}
    \begin{figure}
      \includegraphics[width=\textwidth]{graph/Cu-B.pdf}
      \caption{Lineare Regression für das Verhältnis zwischen Hallspannung und Magnetfeld für Kupfer}
      \label{fig:Cu-B}
    \end{figure}
    \clearpage
    \begin{table}
      \OverfullCenter{\input{table/regress-Cu.tex}}
      \caption{Ergebnisse der linearen Regressionen für Kupfer}
      \label{tab:regress-Cu}
    \end{table}
    \begin{table}
      \OverfullCenter{\input{table/result-Cu.tex}}
      \caption{Errechnete mikroskopische Größen für Kupfer}
      \label{tab:result-Cu}
    \end{table}
    \begin{figure}
      \includegraphics[width=\textwidth]{graph/Ta-I.pdf}
      \caption{Lineare Regression für das Verhältnis von Hallspannung und Speisestrom für Tantal}
      \label{fig:Ta-I}
    \end{figure}
    \begin{figure}
      \includegraphics[width=\textwidth]{graph/Ta-B.pdf}
      \caption{Lineare Regression für das Verhältnis zwischen Hallspannung und Speisestrom für Tantal}
      \label{fig:Ta-B}
    \end{figure}
    \begin{table}
      \OverfullCenter{\input{table/regress-Ta.tex}}
      \caption{Ergebnisse der linearen Regressionen für Tantal}
      \label{tab:regress-Ta}
    \end{table}
    \begin{table}
      \OverfullCenter{\input{table/result-Ta.tex}}
      \caption{Errechnete mikroskopische Größen für Tantal}
      \label{tab:result-Ta}
    \end{table}
    \begin{figure}
      \includegraphics[width=\textwidth]{graph/Zn-I.pdf}
      \caption{Lineare Regression für das Verhältnis zwischen Hallspannung und Speisestrom für Zink}
      \label{fig:Zn-I}
    \end{figure}
    \begin{figure}
      \includegraphics[width=\textwidth]{graph/Zn-B.pdf}
      \caption{Lineare Regression für das Verhältnis zwischen Hallspannung und Speisestrom für Zink}
      \label{fig:Zn-B}
    \end{figure}
    \begin{table}
      \OverfullCenter{\input{table/regress-Zn.tex}}
      \caption{Ergebnisse der linearen Regressionen für Zink}
      \label{tab:regress-Zn}
    \end{table}
    \begin{table}
      \OverfullCenter{\input{table/result-Zn.tex}}
      \caption{Errechnete mikroskopische Größen für Zink}
      \label{tab:result-Zn}
    \end{table}
    \begin{table}
      \input{table/literature.tex}
      \caption{Literaturwerte \cite{tipler}}
      \label{tab:literature}
    \end{table}
  \section{Diskussion}
    Bei der Aufnahme der Hysteresekurve ist aufgefallen, dass bei keinem Strom trotzdem ein Magnetfeld zu messen ist; dies ist auf die Restmagnetisierung des Metalls, sowie das Erdmagnetfeld zurückzuführen.
    Da der Abstand der Kurven sehr klein ist, die Hysteresekurve also eine kleine Fläche bekommt, wurden bei weiteren Berechnungen nur die roten Messwerte verwendet.

    Die Messungen der Hall-Spannung für Silber und Kupfer waren sehr willkürlich, da die Spannung über große Intervalle oszilliert hat.
    Wolfram war nicht messbar. 
    Die statistischen Schwankungen bei Silber sind bei den linearen Regressionen erkennbar, halten sich jedoch in Grenzen, was einen vertretbaren Fehler für die mikroskopischen Größen zu Folge hat.
    Bei Kupfer ist bei den Messwerten für die Hallspannung und den Speisestrom keine Linearität ersichtlich; im Graphen für das Verhältnis zwischen Hallspannung und Magnetfeld gibt es große statistische Schwankungen was große Fehler bei der Berechnung nach sich zog, wie in Tabelle \ref{tab:result-Cu} ersichtlich ist.
    Tantal und Zink erschienen während der Messung fehlerarm; die Widerstandsmessung war durch die direkte Messung genau.
    Bei der Regression fallen bei beiden Graphen hohe Abweichungen von der Linearität auf; trotzdem sind die berechneten Werte mit einem größeren, aber vertrebaren Fehler behaftet.
    Für Zink wird die Linearität der Messwerte sehr deutlich; dies äußerst sich auch im größtenteils kleinen Fehler der berechneten Größen.

    Aus dem Vorzeichen der Hall-Spannung folgt, dass Kupfer und Zink überwiegend Elektronenleiter und Tantal und Silber Löcherleiter sind.

    In Tabelle \ref{tab:literature} sind einige Literaturwerte zum Vergleich aufgeführt.
    Für Silber und Zink passt $n$ gut zum ermittelten Wert $n_B$, wohingegen $n_I$ geringfügig abweicht.
    Die Zahl der Ladungsträger pro Atom weicht stark vom gemessenen Wert ab; Gründe dafür wurden oben genannt.
    Bei Kupfer unterscheiden sich die Werte für $n$ um vier Größenordnungen, bei $z$ um eine; gleiches gilt für Tantal für den Wert von $z$.
    Bei der Messung sind also große Abweichungen von den Literaturwerten aufgetreten.
  \makebibliography
\end{document}
