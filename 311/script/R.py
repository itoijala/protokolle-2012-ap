from labtools import *

elements = ['Ag',
            'Cu',
            'Ta',
            'Zn']

R = np.loadtxt(lt.file('data/R'), unpack=True)

np.save(lt.new('result/R.npy'), unc.unumpy.uarray((R, [0] * len(R))))

t = lt.SymbolColumnTable()
t.add(lt.Column([[ r'\ce{{{:s}}}'.format(element) for element in elements ]], 'c', header='Element'))
t.add_si_column('R', R, r'\ohm', places=0)
t.savetable(lt.new('table/R.tex'))
