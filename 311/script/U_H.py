from labtools import *

elements = ["Ag", # lt.file('data/Ag') lt.new('table/Ag.tex') savefig(lt.new('graph/Ag-I.tex')) savefig(lt.new('graph/Ag-B.tex'))
            "Cu", # lt.file('data/Cu') lt.new('table/Cu.tex') savefig(lt.new('graph/Cu-I.tex')) savefig(lt.new('graph/Cu-B.tex'))
            "Ta", # lt.file('data/Ta') lt.new('table/Ta.tex') savefig(lt.new('graph/Ta-I.tex')) savefig(lt.new('graph/Ta-B.tex'))
            "Zn"] # lt.file('data/Zn') lt.new('table/Zn.tex') savefig(lt.new('graph/Zn-I.tex')) savefig(lt.new('graph/Zn-B.tex'))

I, B = np.loadtxt(lt.file('data/B'), unpack=True)
B *= 1e-3
I_B = dict(np.array((I[:len(I) / 2 + 1], B[:len(B) / 2 + 1])).T)

I_speise = []
I_spule = []
U_H = []
for element in elements:
    a, b, c = np.loadtxt(lt.file('data/' + element), unpack=True)
    I_speise.append(a)
    I_spule.append(b)
    U_H.append(c)
I_speise = np.array(I_speise)
I_spule = np.array(I_spule)
U_H = np.array(U_H)

B = []
for i in I_spule:
    b = []
    for j in i:
        b.append(I_B[j])
    b = np.array(b)
    B.append(b)
B = np.array(B)

num = [10,
       10,
       7,
       10]

mask = [[np.array([0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1]), np.array([0, 0, 0, 0, 0, 1, 0])],
        [np.array([0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1]), np.array([0, 0, 0, 1, 0, 0, 0])],
        [np.array([0, 0, 0, 0, 0, 0, 0, 0         ]), np.array([1, 0, 0, 1, 0, 0, 0])],
        [np.array([0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]), np.array([0, 0, 0, 0, 0, 0, 0])]]

I = []
C = []
U_H_I = []
U_H_B = []
for i in range(len(I_speise)):
    I.append(np.ma.masked_array(data=I_speise[i][:num[i] + 1], mask=mask[i][0]))
    C.append(np.ma.masked_array(data=B[i][num[i]:], mask=mask[i][1]))
    U_H_I.append(np.ma.masked_array(data=U_H[i][:num[i] + 1], mask=mask[i][0]))
    U_H_B.append(np.ma.masked_array(data=U_H[i][num[i]:], mask=mask[i][1]))

A_I = []
C_I = []
A_B = []
C_B = []
for i in range(len(U_H)):
    a, b = lt.linregress(I[i][~I[i].mask], U_H_I[i][~U_H_I[i].mask])
    A_I.append(a)
    C_I.append(b)
    a, b = lt.linregress(C[i][~C[i].mask], U_H_B[i][~U_H_B[i].mask])
    A_B.append(a)
    C_B.append(b)
A_I = np.array(A_I)
C_I = np.array(C_I)
A_B = np.array(A_B)
C_B = np.array(C_B)

np.save(lt.new('result/A.npy'), (A_I, C_I, np.array([ B[i][0] for i in range(len(U_H)) ]), A_B, C_B, np.array([ I_speise[i][-1] for i in range(len(U_H)) ])))

settings = [[1e6, r'\micro\volt', 0, -0.25, 10.25, -0.0001,  0.0011,  0, 1e6, '\micro',  'upper left', -0.02, 0.9, 0,     0.001,     0, 1e6,   '\micro',        'upper right'],
            [1,   r'\volt',       1, -0.25, 10.25,  -1.95, -0.95,   1, 1,   '',        'lower right', -0.07, 0.9, 0, -2.1, 1, 1, '', 'upper left'],
            [1,   r'\volt',       2, -0.25, 7.25,  -2.4,     -0.4,    1, 1,   '',        'lower left', -0.02, 0.9, -2.33,    -1.68,    1, 1,   '',        'upper left'],
            [1e6, r'\micro\volt', 0, -0.25, 10.25, -0.00005, 0.00075, 0, 1e6, r'\micro', 'lower right', -0.02, 0.9, 0.00069,  0.000717, 0, 1e6, r'\micro', 'lower left']]

for i in range(len(U_H)):
    s = settings[i]

    t = lt.SymbolColumnTable()
    t.add_si_column(r'I_\t{Speise}', I_speise[i], r'\ampere')
    t.add_si_column(r'I_\t{Spule}', I_spule[i], r'\ampere', places=1)
    t.add_si_column(r'U_\t{H}', U_H[i] * s[0], s[1], places=s[2])
    t.savetable(lt.new('table/' + elements[i] + '.tex'))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    x = np.linspace(s[3], s[4], 10000)
    ax.plot(x, lt.vals(A_I[i] * x + C_I[i]), 'b-', label='Regressionsgerade')
    ax.plot(I[i][I[i].mask].data, U_H_I[i][U_H_I[i].mask].data, 'gx', label='Messwerte nicht in Regression')
    ax.plot(I[i][~I[i].mask], U_H_I[i][~U_H_I[i].mask], 'rx', label='Messwerte')
    ax.xaxis.set_view_interval(s[3], s[4], True)
    ax.yaxis.set_view_interval(s[5], s[6], True)
    ax.xaxis.set_major_formatter(lt.SiFormatter())
    ax.yaxis.set_major_formatter(lt.SiFormatter(s[7], factor=s[8]))
    ax.set_xlabel(r'\silabel{I_\t{Speise}}{\ampere}')
    ax.set_ylabel(r'\silabel{U_\t{H}}{' + s[9] + r'\volt}')
    lt.ax_reverse_legend(ax, s[10])
    fig.tight_layout()
    lt.savefig(fig, lt.new('graph/' + elements[i] + '-I.tex'))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    x = np.linspace(s[11], s[12], 10000)
    ax.plot(x, lt.vals(A_B[i] * x + C_B[i]), 'b-', label='Regressionsgerade')
    ax.plot(C[i][C[i].mask].data, U_H_B[i][U_H_B[i].mask].data, 'gx', label='Messwerte nicht in Regression')
    ax.plot(C[i][~C[i].mask], U_H_B[i][~U_H_B[i].mask], 'rx', label='Messwerte')
    ax.xaxis.set_view_interval(s[11], s[12], True)
    ax.yaxis.set_view_interval(s[13], s[14], True)
    ax.xaxis.set_major_formatter(lt.SiFormatter(1))
    ax.yaxis.set_major_formatter(lt.SiFormatter(s[15], factor=s[16]))
    ax.set_xlabel(r'\silabel{B}{\tesla}')
    ax.set_ylabel(r'\silabel{U_\t{H}}{' + s[17] + r'\volt}')
    lt.ax_reverse_legend(ax, s[18])
    fig.tight_layout()
    lt.savefig(fig, lt.new('graph/' + elements[i] + '-B.tex'))
