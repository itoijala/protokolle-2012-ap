from labtools import *

n_Ag, z_Ag = np.loadtxt(lt.file('data/literaturw/Ag'))
n_Cu, z_Cu = np.loadtxt(lt.file('data/literaturw/Cu'))
z_Ta = np.loadtxt(lt.file('data/literaturw/Ta'))
n_Zn, z_Zn = np.loadtxt(lt.file('data/literaturw/Zn'))


n_Cu *= 1e27
n_Ag *= 1e27
n_Zn *= 1e27

t = lt.SymbolRowTable()
t.add_si_row(r'n_\t{Ag}', n_Ag * 1e-27, r'\per\cubic\meter', exp='e27')
t.add_si_row(r'z_\t{Ag}', z_Ag)
t.add_hrule()
t.add_si_row(r'n_\t{Cu}', n_Cu * 1e-27, r'\per\cubic\meter', exp='e27')
t.add_si_row(r'z_\t{Cu}', z_Cu)
t.add_hrule()
t.add_si_row(r'z_\t{Ta}', z_Ta)
t.add_hrule()
t.add_si_row(r'n_\t{Zn}', n_Zn * 1e-27, r'\per\cubic\meter', exp='e27')
t.add_si_row(r'z_\t{Zn}', z_Zn)
t.savetable(lt.new('table/literature.tex'))
