from labtools import *

I, B = np.loadtxt(lt.file('data/B'), unpack=True)
B *= 1e-3

t = lt.SymbolColumnTable()
t.add_si_column('I', I, r'\ampere', places=1)
t.add_si_column('B', B * 1e3, r'\milli\tesla', figures=4)
t.savetable(lt.new('table/B.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(I[:len(I) / 2 + 1], B[:len(I) / 2 + 1], 'rx', label='Messwerte hoch')
ax.plot(I[len(I) / 2:], B[len(I) / 2:], 'bx', label='Messwerte runter')
ax.xaxis.set_view_interval(-0.1, 5.1, True)
ax.yaxis.set_view_interval(-0.05, 1.3, True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter(1))
ax.set_xlabel(r'\silabel{I}{\ampere}')
ax.set_ylabel(r'\silabel{B}{\tesla}')
ax.legend(loc='lower right')
fig.savefig(lt.new('graph/B.tex'), bbox_inches='tight', pad_inches=0)
