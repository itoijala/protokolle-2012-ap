from labtools import *

R = np.load(lt.file('result/R.npy'))
r, d, L = np.load(lt.file('result/sizes.npy'))
M, rho = np.load(lt.file('result/M.npy'))

A_I, C_I, B, A_B, C_B, I = np.load(lt.file('result/A.npy'))

A = np.array([A_I, A_B])
C = np.array([C_I, C_B])

elements = ["Ag", # lt.new('table/regress-Ag.tex') lt.new('table/result-Ag.tex')
            "Cu", # lt.new('table/regress-Cu.tex') lt.new('table/result-Cu.tex')
            "Ta", # lt.new('table/regress-Ta.tex') lt.new('table/result-Ta.tex')
            "Zn"] # lt.new('table/regress-Zn.tex') lt.new('table/result-Zn.tex')

e = lt.constant('elementary charge')
m = lt.constant('electron mass')
h = lt.constant('Planck constant')

Q = np.pi * r**2

n = -1 / e / d * np.array([B, I]) / A

tau = 2 * m / e**2 * L / Q / R / n
v_t = h / m * (3 / 8 / np.pi * unc.unumpy.fabs(n))**(1 / 3)
l = v_t * tau
mu = - 1 / 2 * e / m * tau
v_d = - 1e-6 / e / n
z = n * M / rho

settings = [[[1e6,   r'\micro\volt\per',                    ''],     # Ag
             [1e6,   r'\micro\volt',                        ''],
             [1e-27, r'\per\cubic\meter',                   'e27'],
             [1e15,  r'\second',                            'e-15'],
             [1e-3,  r'\milli\meter\per\second',            ''],
             [1e12,  r'\meter',                             'e-12'],
             [1e3,   r'\milli\coulomb\second\per\kilogram', ''],
             [1e15,  r'\meter\per\second',                  'e-15'],
             [1e-3,  '',                                    'e3']],
            [[1,     r'\volt\per',                          ''],     # Cu
             [1,     r'\volt',                              ''],
             [1e-24, r'\per\cubic\meter',                   'e24'],
             [1e12,  r'\second',                            'e-12'],
             [1e-3,  r'\milli\meter\per\second',            ''],
             [1e6,   r'\micro\meter',                       ''],
             [1,     r'\coulomb\second\per\kilogram',       ''],
             [1e12,  r'\meter\per\second',                  'e-12'],
             [1, '',                                        '']],
            [[1,     r'\volt\per',                          ''],     # Tn
             [1,     r'\volt',                              ''],
             [1e-24, r'\per\cubic\meter',                   'e24'],
             [1e12,  r'\second',                            'e-12'],
             [1e-3,  r'\milli\meter\per\second',            ''],
             [1e6,   r'\micro\meter',                       ''],
             [1,     r'\coulomb\second\per\kilogram',       ''],
             [1e12,  r'\meter\per\second',                  'e-12'],
             [1, '',                                        '']],
            [[1e6,   r'\micro\volt\per',                    ''],     # Zn
             [1e6,   r'\micro\volt',                        ''],
             [1e-27, r'\per\cubic\meter',                   'e27'],
             [1e15,  r'\second',                            'e-15'],
             [1e-3,  r'\milli\meter\per\second',            ''],
             [1e12,  r'\meter',                             'e-12'],
             [1e3,   r'\milli\coulomb\second\per\kilogram', ''],
             [1e15,  r'\meter\per\second',                  'e-15'],
             [1e-3,  '',                                    'e3']]]

a = [r'\ampere', r'\tesla']

def column(tab, header, values):
    col = lt.SiColumn([], header)
    for val in values:
        col.add_cell([lt.round_to(str(float(lt.vals(val))), str(float(lt.stds(val))))])
        col.add_cell([lt.round_figures(str(float(lt.stds(val))), 1)])
    tab.add(col)

def row_header(symbol, unit, exp):
    return (r'${0:s}$ & ' + (r'\SI{{{2:s}}}' if exp != '' else r'\si') + r'{{{1:s}}}').format(symbol, unit, exp)

def row(tab, symbol, unit, exp):
    tab.add(lt.Row(row_header(r'\errPhantom{{{:s}}}'.format(symbol), unit, exp)))
    tab.add(lt.Row(row_header(r'\err{{{:s}}}'.format(symbol), unit, exp)))

for i in range(len(elements)):
    s = settings[i]

    t = lt.SymbolRowTable()
    t.add_si_row('A_I', A[0][i] * s[0][0], s[0][1] + a[0], exp=s[0][2])
    t.add_si_row('C_I', C[0][i] * s[1][0], s[1][1],        exp=s[1][2])
    t.add_hrule()
    t.add_si_row('A_B', A[1][i] * s[0][0], s[0][1] + a[1], exp=s[0][2])
    t.add_si_row('C_B', C[1][i] * s[1][0], s[1][1],        exp=s[1][2])
    t.savetable(lt.new('table/regress-' + elements[i] + '.tex'))

    t = lt.Table('c c')
    row(t, 'n',               s[2][1], s[2][2])
    row(t, r'\mean{\tau}',    s[3][1], s[3][2])
    row(t, r'v_\t{t}',        s[4][1], s[4][2])
    row(t, r'\mean{l}',       s[5][1], s[5][2])
    row(t, r'\mu',            s[6][1], s[6][2])
    row(t, r'\mean{v_\t{d}}', s[7][1], s[7][2])
    row(t, 'z',               s[8][1], s[8][2])
    column(t, '$n_I$', [ x[0][i] * s[0] for x, s in zip((n, tau, v_t, l, mu, v_d, z), s[2:]) ])
    column(t, '$n_B$', [ x[1][i] * s[0] for x, s in zip((n, tau, v_t, l, mu, v_d, z), s[2:]) ])
    t.savetable(lt.new('table/result-' + elements[i] + '.tex'))
