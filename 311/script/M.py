from labtools import *

elements = ['Ag',
            'Cu',
            'Ta',
            'Zn']

M = np.loadtxt(lt.file('data/M'), unpack=True)
rho = np.loadtxt(lt.file('data/rho'), unpack=True)
rho *= 1e-3

np.save(lt.new('result/M.npy'), (unc.unumpy.uarray((M * sp.constants.u, [0] * len(M))), unc.unumpy.uarray((rho, [0] * len(rho)))))

t = lt.SymbolColumnTable()
t.add(lt.Column([[ r'\ce{{{:s}}}'.format(element) for element in elements ]], 'c', header='Element'))
t.add_si_column('M', M, r'\atomicmassunit', figures=[7, 5, 8, 4])
t.add_si_column(r'\rho', rho * 1e3, r'\gram\per\cubic\meter', figures=6)
t.savetable(lt.new('table/M.tex'))
