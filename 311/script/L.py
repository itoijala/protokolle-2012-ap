from labtools import *

def meanofj(values, j):
    return lt.mean(np.split(values, len(values) // j), axis=-1)

def create_column(symbol, values, j=1, unit="", exp="", places=0):
    col = lt.SiColumn([], labtools.table.si.build_header(symbol, unit, exp, False))
    values = np.split(values, len(values) // j)
    for vals in values:
        cell = []
        for v in vals:
            if np.isfinite(v):
                cell.append(lt.round_places(str(v), places))
            else:
                cell.append('')
        col.add_cell(cell)
    return col

elements = ["Ag",
            "Cu",
            "Ta",
            "Zn"]

L = np.loadtxt(lt.file('data/L'), unpack=True)
L *= 1e-2
r = np.loadtxt(lt.file('data/2r'), unpack=True)
r *= 1e-3
r /= 2
d = np.loadtxt(lt.file('data/d'), unpack=True)
d *= 1e-3

r2 =np.loadtxt(lt.file('data/2r-error'), unpack=True)
r2 *= 1e-6
r2 /= 2
r2 = unc.unumpy.uarray((r2[0], r2[1]))

r_mean= meanofj(r, 4)
d_mean = meanofj(d, 4)

r_mean = np.array([r2[0], r_mean[0], r_mean[1], r2[1]])

np.save(lt.new('result/sizes.npy'), (r_mean, d_mean, L))

r = np.array([r2[0].nominal_value] + [np.nan] * 3 + list(r) + [r2[1].nominal_value] + [np.nan] * 3)
delta_r = np.array([r2[0].std_dev(), np.nan, np.nan, r2[1].std_dev()])

t = lt.Table("S[table-format=2.0]", headerRow=lt.Row(r'\multicolumn{1}{c}{Element}'))
for element in elements:
    t.add(lt.Row(r'\ce{{{:s}}}'.format(element)))
t.add(create_column('L', L * 1e2, 1, r'\centi\meter', places=1))
t.add(create_column('2r', r * 2 * 1e3, 4, r'\milli\meter', places=3))
t.add(create_column('2 \err{r}', delta_r * 2 * 1e3, 1, r'\milli\meter', places=3))
t.add(create_column('d', d * 1e3, 4, r'\milli\meter', places=3))
t.savetable(lt.new("table/L.tex"))

t = lt.SymbolColumnTable()
t.add(lt.Column([[ r'\ce{{{:s}}}'.format(element) for element in elements ]], 'c', header='Element'))
t.add_si_column(r'\mean{r}', r_mean * 1e3, r'\milli\meter', zero_error_figures=2)
t.add_si_column(r'\mean{d}', d_mean * 1e3, r'\milli\meter')
t.savetable(lt.new('table/r.tex'))
