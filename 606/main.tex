\input{header/report.tex}

\addbibresource{lit.bib}

\SetExperimentNumber{606}

\begin{document}
  \maketitlepage{Messung der Suszeptibilität paramagnetischer Substanzen}{12.06.2012}{19.06.2012}
  \section{Ziel}
    Ziel des Versuchs ist es, die Suszeptibilität einiger seltener Erden zu bestimmen.
  \section{Theorie}
    Der Zusammenhang zwischen der magnetischen Flussdichte $\v{B}$ und der magnetischen Feldstärke $\v{H}$ in Materie ist gegeben durch
    \begin{eqn}
      \v{B} = \mu_0 \v{H} + \v{M} ,
    \end{eqn}
    wobei $\mu_0$ die magnetische Feldkonstante und $\v{M}$ die Magnetisierung des Materials ist.
    Für die Magnetisierung gilt
    \begin{eqns}
      \v{M} &=& \mu_0 N \mean{\v{\mu}} \eqlabel{eqn:M} \\
            &=& \mu_0 \chi \v{H} ,
    \end{eqns}
    wobei $\mean{\v{\mu}}$ das mittlere magnetische Moment im Material und $N$ die Anzahl der Momente pro Volumeneinheit ist.
    Die Größe $\chi$ wird Suszeptibilität genannt und kann, abhängig vom Material, eine komplizierte Funktion von $\v{H}$ und der Temperatur $T$ sein.

    Der Diamagnetismus ist eine allgemeine Eigenschaft jedes Atoms.
    Dabei wird vom äußeren Feld ein magnetisches Moment induziert, das dem äußeren Feld entgegengerichtet ist.
    Der Paramagnetismus wird nur bei Atomen beobachtet, die einen Drehimpuls besitzen.
    Die magnetischen Momente des Atoms orientieren sich relativ zum äußeren Feld, was aber wegen der thermischen Bewegung ein temperaturabhängiger Vorgang ist.

    Der Drehimpuls eines Atoms besteht aus dem Bahndrehimpuls der Elektronen, ihrem Spin und dem Kerndrehimpuls, wobei letzteres vernachlässigt werden kann.
    Bei nicht zu großen Feldern gilt für den Gesamtdrehimpuls
    \begin{eqn}
      \v{J} = \v{L} + \v{S} ,
    \end{eqn}
    wobei $\v{L}$ der Bahndrehimpuls und $\v{S}$ der Spin ist.
    Für sie gilt
    \begin{eqns}
      \v{L} &=& \sum \v{l}_i \\
      \v{S} &=& \sum \v{s}_i ,
    \end{eqns}
    wobei $\v{l}_i$ und $\v{s}_i$ die Bahndrehimpulse und Spins der einzelnen Elektronen sind.
    Für die magnetischen Momente, die aus den Drehimpulsanteilen resultieren, gilt
    \begin{eqns}
      \v{\mu}_\v{L} &=& - \frac{\mu_\t{B}}{\hbar} \v{L} \\
      \v{\mu}_\v{S} &=& - g_S \frac{\mu_\t{B}}{\hbar} \v{S} ,
    \end{eqns}
    wobei $g_S$ das gyromagnetische Verhältnis des freien Elektrons und
    \begin{eqn}
      \mu_\t{B} = \frac{\hbar}{2} \frac{e}{m}
    \end{eqn}
    das bohrsche Magneton ist; wobei $e$ die Elementarladung und $m$ die Elektronenmasse ist.
    Mit
    \begin{eqns}
      \abs{\v{L}} &=& \hbar \sqrt{L \g(L + 1)} \\
      \abs{\v{S}} &=& \hbar \sqrt{S \g(S + 1)} \\
      \abs{\v{J}} &=& \hbar \sqrt{J \g(J + 1)}
    \end{eqns}
    gilt für die magnetischen Momente
    \begin{eqns}
      \abs{\v{\mu}_\v{L}} &=& \mu_\t{B} \sqrt{L \g(L + 1)} \\
      \abs{\v{\mu}_\v{S}} &=& \mu_\t{B} \sqrt{S \g(S + 1)} .
    \end{eqns}
    Hierbei sind $L$, $S$ und $J$ die Bahndrehimpuls-, Spin- und Gesamtdrehimpulsquantenzahlen des Atoms.

    Nur die Komponente $\v{\mu}_\v{J}$ des gesamten magnetischen Moments $\v{\mu}$, die parallel zu $\v{J}$ ist, ist messbar.
    Für sie ergibt sich
    \begin{eqn}
      \abs{\v{\mu}_\v{J}} = \abs{\v{\mu}_\v{L}} \cos.\alpha. + \abs{\v{\mu}_\v{S}} \cos.\beta.
    \end{eqn}
    mit
    \begin{eqns}
      \cos.\alpha. &=& \frac{\abs{\v{J}}^2 + \abs{\v{L}}^2 - \abs{\v{S}}^2}{2 \abs{\v{J}} \abs{\v{L}}} \\
      \cos.\beta. &=& \frac{\abs{\v{J}}^2 - \abs{\v{L}}^2 + \abs{\v{S}}^2}{2 \abs{\v{J}} \abs{\v{S}}} .
    \end{eqns}
    Insgesamt ergibt sich
    \begin{eqn}
      \abs{\v{\mu}_\v{J}} = \mu_\t{B} \frac{\g(1 + g_S) J \g(J + 1) + \g(g_S - 1) \g[S \g(S + 1) - L \g(L + 1)]}{2 \sqrt{J \g(J + 1)}} .
    \end{eqn}
    Mit der Näherung $g_S \approx 2$ gilt
    \begin{eqn}
      \abs{\v{\mu}_\v{J}} \approx \mu_\t{B} g_J \sqrt{J \g(J + 1)} ,
    \end{eqn}
    wobei
    \begin{eqn}
      g_J = \frac{3 J \g(J + 1) + S \g(S + 1) - L \g(L + 1)}{2 J \g(J + 1)} \eqlabel{eqn:g_J}
    \end{eqn}
    der Landé-Faktor des Atoms ist.

    Ein weiteres Ergebnis der Quantenmechanik liefert die Aussage, dass die Komponente von $\v{\mu}_\v{J}$ in Feldrichtung $\mu_{\v{J}_z}$ nur diskrete Werte annehmen kann.
    Es gilt
    \begin{eqn}
      \mu_{\v{J}_z} = - \mu_\t{B} g_J m , \quad m \in \set{\nu \in \setN_0}{\abs{\nu} \le J}
    \end{eqn}
    mit der Orientierungsquantenzahl $m$.
    Die Potentielle Energie jedes der $2 J + 1$ Unterniveaus ist gegeben durch
    \begin{eqn}
      E_m = - \Dot{\v{\mu}_\v{J}}{\v{B}} = \mu_\t{B} g_J m B .
    \end{eqn}
    Diese Aufspaltung der Niveaus wird als Zeeman-Effekt bezeichnet.

    Die Besetzungshäufigkeit eines Energieniveaus $E$ abhängig von der Temperatur $T$ ist gegeben durch die Boltzmann-Verteilung
    \begin{eqn}
      \f{b}(E, T) = \exp(- \frac{E}{k T}) ,
    \end{eqn}
    wobei $k$ die Boltzmann-Konstante ist.
    Für das mittlere magnetische Moment ergibt sich
    \begin{eqn}
      \mean{\mu} = \frac{\displaystyle \quad\! \sum_{\mathclap{m = - J}}^J \f{\mu_{\v{J}_z}}(m) \f{b}(E_m, T)}{\displaystyle \sum_{\mathclap{m = - J}}^J \f{b}(E_m, T)}
      = - \mu_\t{B} g_J \frac{\displaystyle \quad\! \sum_{\mathclap{m = - J}}^J m \exp(- \frac{\mu_\t{B} g_J m B}{k T} )}{\displaystyle \sum_{\mathclap{m = - J}}^J \exp(- \frac{\mu_\t{B} g_J m B}{k T} )} ,
    \end{eqn}
    woraus sich die Magnetisierung $\v{M}$ nach \eqref{eqn:M} berechnen lässt.
    Für Raumtemperatur und $B \le \SI{1}{\tesla}$, also für
    \begin{eqn}
      \frac{\mu_\t{B} g_J m B}{k T} \ll 1
    \end{eqn}
    kann die Näherung
    \begin{eqn}
      \exp(- \frac{\mu_\t{B} g_J m B}{k T}) = 1 - \frac{\mu_\t{B} g_J m B}{k T} + \LandauO(\g[\frac{\mu_\t{B} g_J m B}{k T}]^2)
    \end{eqn}
    verwendet werden, woraus folgt
    \begin{eqn}
      M = \frac{1}{3} \mu_0 \mu_\t{B}^2 g_J^2 N \frac{J \g(J + 1) B}{k T} .
    \end{eqn}
    Daraus lässt sich die Suszeptibilität zu
    \begin{eqn}
      \chi = \frac{1}{3} \mu_0 \mu_\t{B}^2 g_J^2 N \frac{J \g(J + 1)}{k T} \propto \frac{1}{T} \eqlabel{eqn:chi}
    \end{eqn}
    bestimmen, was die Aussage des curieschen Gesetzes des Paramagnetismus ist.
    \subsection{Hundsche Regeln}
      Die Atome der seltenen Erden besitzen die vollen Schalen des Xenon-Atoms.
      Da aber der Paramagnetismus auch bei Ionen gemessen werden kann, können diese Elektronen nicht die Ursache des Effekts sein.
      Verantwortlich sind die Elektronen der 4f-Schale, von denen die Elemente unterschiedliche Anzahlen bestizen.
      Der Drehimpuls kann durch die hundschen Regeln berechnet werden:
      \begin{enumerate}
        \item
          Die Spins $s_i$ der Elektronen richten sich so aus, dass der Gesamtspin
          \begin{eqn}
            S = \sum_i s_i
          \end{eqn}
          maximal wird.
          Dabei muss das Pauli-Verbot beachtet werden, wonach zwei Elektronen nicht allen ihren Quantenzahlen übereinstimmen können.
        \item
          Die Bahndrehimpulse $l_i$ der Elektronen kombinieren so, dass der Gesamtbahndrehimpuls
          \begin{eqn}
            L = \sum_i m_i
          \end{eqn}
          maximal wird, wobei $m_i$ die magnetische Quantenzahl des $i$-ten Elektrons ist.
          Dabei müssen die Spins aus der ersten Regel beachtet werden.
        \item
          Für den Gesamtdrehimpuls ergibt sich $J= \abs{L - S}$, falls die Schale weniger als zur Hälfte gefüllt ist, sonst ergibt sich $J = L + S$.
          Diese Formeln liefern das gleiche Ergebnis, falls die Schale genau zur Hälfte gefüllt ist, da der Bahndrehimpuls in diesem Fall verschwindet.
      \end{enumerate}
      Die Suszeptibilität kann aus dem Gesamtdrehimpuls über \eqref{eqn:g_J} und \eqref{eqn:chi} berechnet werden.
  \section{Aufbau und Durchführung}
    \subsection{Güte eines Selektivverstärkers}
      Zur Bestimmung der Suszeptibilität wird ein Selektivverstärker verwendet, der eine Frequenz im Signal verstärkt und alle anderen Frequenzen unterdrückt.
      Zur Bestimmung seiner Güte wird am Eingang ein Sinusgenerator verwendet und das Signal am Ausgang mit einem Millivoltmeter gemessen.
      Zuerst wird die Frequenz gesucht, bei der das Signal maximal wird.
      Diese wird dann zur Suszeptibilitätsbestimmung verwendet.
      Es werden dann 15 bis 20 Messwerte der Frequenz $\nu$ und der Ausgangsspanunng $U_\t{a}$ um die bestimmte Frequenz aufgenommen.
      Außerdem wird die Ausgangsspannung $U_\t{e}$ des Sinusgenerators gemessen.
      \FloatBarrier
    \subsection{Suszeptibilität}
      Zur Messung der Suszeptibilität wird eine Brückenschaltung wie in Abbildung \ref{fig:brücke} verwendet.
      Sie ist an die weiteren Geräte wie in Abbildung \ref{fig:schaltung} angeschlossen.
      Die Brücke wird von einem Sinusgenerator mit einer Spannung der Frequenz versorgt, die die maximale Verstärkung des Selektivverstärkers vervorruft.
      In eine der Spulen kann die Probe eingeführt werden.
      Zuerst wird ohne Probe die Brückenspannung $U_\t{Br}$ minimiert, indem abwechselnd der Parallelwiderstand $R_\t{P}$ und das Potentiometer angepasst werden.
      Die Anzeige $R_0$ des Potentiometers und die Brückenspanung $U_{\t{Br}, 0}$ werden aufgeschrieben.
      Eine Probe wird gemessen, indem sie in die Spule eingeführt wird und die entstehende Brückenspannung $U_\t{Br}$ notiert wird.
      Dann wird die neue Einstellung $R$ des Potentiometers gesucht, die die Brückenspannung mit Probe minimiert.
      Die Messung wird abwechselnd für verschiedene Probenmaterialien wiederholt, sodass jede Probe fünf mal gemessen wird.
      Vor Beginn der Messung wird verifiziert, dass die Verstärkungsfaktoren des Selektivverstärkers und des Linearverstärkers wirklich 10 sind.
      \begin{figure}
        \includegraphics{graphic/brücke.pdf}
        \caption{Verwendete Brückenschaltung \cite{anleitung606}}
        \label{fig:brücke}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/schaltung.pdf}
        \caption{Schaltung zur Messung der Suszeptibilität \cite{anleitung606}}
        \label{fig:schaltung}
      \end{figure}
  \section{Messwerte}
    \subsection{Bestimmung der Bandpassgüte}
    In Tabelle \ref{tab:güte} befindet sich die Durchgangsspannung $U_\t{a}$ in Abhängigkeit von der Frequenz $\nu$, des Weiteren steht dort die angelegte Eingangsspannung $U_\t{e}$.
      \begin{table}
        \input{table/güte.tex}
        \caption{Messwerte zu Bestimmung der Güte des Selektivverstärkers}
        \label{tab:güte}
      \end{table}
      \FloatBarrier
    \subsection{Ermittlung der Suszeptibilität}
    Die verwendeten Konstanten stehen in Tabelle \ref{tab:constants}; in Tabelle \ref{tab:b} sind die angenommene Raumtemperatur $T$, die Frequenz $\nu$, bei der die größte Spannung hinter dem Bandpass anliegt, die angelegte Speisespannung $U_\t{e}$, die minimale Brückenspannung $U_{\t{Br}, 0}$ und der dafür verwendete Abgleichwiderstand $R_0$ und die Masse $m_\ce{O}$ eines Sauerstoffatoms \cite{nist-atomic-weights}.
      Die Windungszahl $n$, Querschnittsfläche $A_\t{sp}$, Länge $l_\t{sp}$ und Widerstand $R_\t{sp}$ der verwendeten Spulen stehen in Tabelle \ref{tab:coil}.
      In Tabelle \ref{tab:m} stehen die Dichten $\rho$ eines Einkristalls aus dem Material, die Masse $m_\t{p}$ der Proben, die Massen $m$ der verwendeten Elemente \cite{nist-atomic-weights} und die Längen $l_\t{a}$ und $l_\t{g}$, also der Teil der Probe, die nicht in die Spule passt und die Gesamtlänge der Probe.
      Die Tabellen \ref{tab:Dy2O3} bis \ref{tab:Nd2O3} enthalten die Messwerte für die Brückenspannung $U_\t{Br}$, die nach Einführen der Proben gemessen wird, und die Abgleichwiderstände $R$.
      \begin{table}
        \input{table/constants.tex}
        \caption{Verwendete Konstanten \cite{scipy}}
        \label{tab:constants}
      \end{table}
      \begin{table}
        \input{table/coil.tex}
        \caption{Spulendaten}
        \label{tab:coil}
      \end{table}
      \begin{table}
        \input{table/b.tex}
        \caption{Geschätzte Raumtemperatur, maximale Durchlassfrequenz, Eingangsspannung, minimale Brückenspannung ohne Probe, Abgleichwiderstand bei minimaler Brückenspannung ohne Probe}
        \label{tab:b}
      \end{table}
      \begin{table}
        \input{table/m.tex}
        \caption{Verschiedene Parameter der Proben}
        \label{tab:m}
      \end{table}
      \begin{table}
        \input{table/Dy2O3.tex}
        \caption{Ermittelte Messwerte für $\ce{Dy2O3}$}
        \label{tab:Dy2O3}
      \end{table}
      \begin{table}
        \input{table/Gd2O3.tex}
        \caption{Ermittelte Messwerte für $\ce{Gd2O3}$}
        \label{tab:Gd2O3}
      \end{table}
      \begin{table}
        \input{table/Nd2O3.tex}
        \caption{Ermittelte Messwerte für $\ce{Nd2O3}$}
        \label{tab:Nd2O3}
      \end{table}
  \section{Auswertung}
    \subsection{Bestimmung der Güte}
      In Abbildung \ref{fig:güte} sind die Messwerte aus Tabelle \ref{tab:güte} aufgetragen.
      Die Güte ist durch
      \begin{eqn}
        Q = \frac{\nu_0}{\nu_+ - \nu_-}
      \end{eqn}
      definiert, wobei $\nu_0$ die Frequenz mit maximaler Spannung und $\nu_+$ und $\nu_-$ die Frequenzen mit der Spannung $U_\t{max} / \sqrt{2}$ sind.
      Dabei ist $\nu_+ > \nu_0$ und $\nu_- < \nu_0$.
      In Tabelle \ref{tab:Q} stehen die abgelesenen Größen $\nu_-$, $\nu_+$ und die Güte $Q$.
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/güte.pdf}
        \caption{Durchlassspannung des Bandpasses aufgetragen gegen die Frequenz, graphische Bestimmung der Güte}
        \label{fig:güte}
      \end{figure}
      \begin{table}
        \input{table/Q.tex}
        \caption{Frequenzen bei denen die Spannung auf das $\frac{1}{\sqrt{2}}$-fache abgefallen ist, sowie die daraus ermittelte Güte}
        \label{tab:Q}
      \end{table}
      \FloatBarrier
    \subsection{Ermittlung der Suszeptibilitäten}
      Für die Auswertung musste berücksichtigt werden, dass die seltenen Erden spröde sind.
      Für die effektive Querschnittsfläche einer Probe gilt
      \begin{eqn}
        A_\t{real} = \frac{m_\t{p}\g(1 - \frac{l_\t{a}}{l_\t{g}})}{\g(l_\t{g} - l_\t{a}) \rho} .
      \end{eqn}
      Da die Anzeige des Potentiometers $R_4$ ist, gilt
      \begin{eqn}
        R_3 = \SI{1000}{\ohm} - R_0 .
      \end{eqn}
      Dies hat aber keinen Einfluss auf die Differenz der Abgleichwiderstände
      \begin{eqn}
        \Del{R} = \abs{R_0 - R} .
      \end{eqn}
      Aus der Brückenschaltung folgt für die Suszeptibilität
      \begin{eqns}
        \chi_{U_\t{Br}} &=& \frac{U_\t{Br} - U_{\t{Br}, 0}}{U_\t{e}} \frac{4 l_\t{sp}}{\omega \mu_0 n^2 A_\t{real}} \sqrt{R_\t{sp}^2 + \g(\omega \mu_0 n^2 \frac{A_\t{sp}}{l_\t{sp}})^{\! 2}} \\
        \chi_R &=& 2 \frac{\Del{R}}{R_3} \frac{A_\t{sp}}{A_\t{real}} ,
      \end{eqns}
      wobei $\omega = 2 \PI \nu$ ist.
      Die berechneten experimentellen Suszeptibilitäten stehen in Tabelle \ref{tab:chi}.
      Die Anzahl $\epsilon$ der Elektronen auf der 4f-Schale und die daraus nach den hundschen Regeln ermittleten Theoriewerte für die Suszeptibilität stehen in Tabelle \ref{tab:chi_theor}, wobei mit der geschätzten Raumtemperatur aus Tabelle \ref{tab:b} nach \eqref{eqn:chi} gerechnet wurde.
      Für $N$ wurde
      \begin{eqn}
        N = 2 \frac{\rho}{m}
      \end{eqn}
      verwendet.
      \begin{table}
        \input{table/chi.tex}
        \caption{Experimentell ermittelte Suszeptibilität}
        \label{tab:chi}
      \end{table}
      \begin{table}
        \input{table/chi_theor.tex}
        \caption{Theoriewerte der Suszeptibilität}
        \label{tab:chi_theor}
      \end{table}
  \section{Diskussion}
    Beim Vergleich der am Bandpass eingestellten Güte $Q = 100$ und der berechneten fällt auf, dass die wahre Güte nur $15$ beträgt; dies unterscheidet sich um eine Größenordnung.

    Der Vergleich der auf zwei Arten ermittelten Suszeptibilität zeigt eine gute Übereinstimmung.
    Der statistische Fehler ist in allen Fällen klein.
    Systematische Fehler können durch die wechselne Temperatur der Proben aufgetreten sein.
    Des Weiteren konnte nicht genau vermessen werden, wie weit das Behältnis in der Spule war.
    Die Tatsache, dass die Brückenspannung nie ganz verschwindet, wurde durch subtrahieren der minimalen Spannung von allen ermittelten Brückenspannungen Rechnung getragen.
    Fehlerquellen könnten des Weiteren im Linearverstärker liegen, der eine hohe Ausfallwahrscheinlichkeit besitzt; direkt registriert wurde aber kein Ausfall, vor Beginn funktionierten sowohl der Bandpass als auch der Linearverstärker einwandfrei.
    Außerdem waren die Behältnisse nicht ganz vollständig mit den Proben gefüllt.
    Die theoretisch ermittelten Werte passen gut mit der Größenordnung der experimentell erhaltenen zusammen.
    Wenn man die angesprochenen Fehlerquellen minimiert, stellt dieses Experiment eine gute Möglichkeit dar, die Suszeptibilität zu ermitteln.
  \makebibliography
\end{document}
