from labtools import *

nu, U_a, places = np.loadtxt(lt.file('data/güte'), unpack=True)
nu_0 = np.loadtxt(lt.file('data/nu_max'))
U_a *= 1e-3
places = [ int(x) for x in places.astype(int) ]
U_e = np.loadtxt(lt.file('data/U_e_a'))
U_e *= 1e-3

a = np.array([(U_a[10] - U_a[9]) / (nu[10] - nu[9]), (U_a[11] - U_a[10]) / (nu[11] - nu[10])])
b = np.array([U_a[9] - a[0] * nu[9], U_a[10] - a[1] * nu[10]])
x = (np.max(U_a) / np.sqrt(2) - b) / a
x = unc.unumpy.uarray((x, [500] * 2))

Q = nu[np.argmax(U_a)] / (x[1] - x[0])

t1 = lt.SymbolRowTable()
t1.add_si_row(r'U_\t{e}', U_e * 1e3, r'\milli\volt', places=1)

t2 = lt.SymbolColumnTable()
t2.add_si_column(r'\nu', nu, r'\hertz', places=0)
t2.add_si_column(r'U_\t{a}', U_a * 1e3, r'\milli\volt', places=places)

t3 = lt.Table()
t3.add(lt.Row())
t3.add_hrule()
t3.add(lt.Row())
t3.add(lt.Column([t1, t2], '@{} c @{}'))
t3.savetable(lt.new('table/güte.tex'))

t = lt.SymbolRowTable()
t.add_si_row(r'\nu_0', nu_0 * 1e-3, r'\kilo\hertz')
t.add_si_row(r'\nu_-', x[0] * 1e-3, r'\kilo\hertz')
t.add_si_row(r'\nu_+', x[1] * 1e-3, r'\kilo\hertz')
t.add_si_row('Q', Q)
t.savetable(lt.new('table/Q.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(nu, U_a, 'rx', label='Messwerte')
ax.axhline(y=np.max(U_a) / np.sqrt(2), c='b', ls='--', label=r'$\displaystyle \frac{U_\t{max}}{\sqrt{2}}$')
ax.axvline(x=lt.vals(x[0]), c='g', ls='--', label=r'$\nu_-$')
ax.axvline(x=lt.vals(x[1]), c='c', ls='--', label=r'$\nu_+$')
ax.xaxis.set_view_interval(24500, 46500, True)
ax.xaxis.set_major_formatter(lt.SiFormatter(factor=1e-3))
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e3))
ax.set_xlabel(r'\silabel{\nu}{\kilo\hertz}')
ax.set_ylabel(r'\silabel{U_\t{a}}{\milli\volt}')
ax.legend(loc='upper right')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/güte.tex'))
