from labtools import *

k = lt.constant('Boltzmann constant')
mu_B = lt.constant('Bohr magneton')
u = lt.constant('unified atomic mass unit')
mu_0 = lt.vals(lt.constant('mag. constant'))

T = unc.ufloat((sp.constants.C2K([20]), 2))

_, _, _, rho, m, m_err, S, L, J, electrons = np.loadtxt(lt.file('data/theory'), unpack=True)
rho *= 1e3
m = unc.unumpy.uarray((m, m_err))
m = m * u

m_O, m_O_err = np.loadtxt(lt.file('data/m_O'), unpack=True)
m_O = unc.ufloat((m_O, m_O_err))
m_O = m_O * u

m = 2 * m + 3 * m_O

N = 2 * rho / m
g_J = (3 * J * (J + 1) + S * (S + 1) - L * (L + 1)) / (2 * J * (J + 1))
chi = 1 / 3 * mu_0 * mu_B**2 * g_J**2 * N * J * (J + 1) / k / T

t = lt.SymbolRowTable()
t.add_si_row('k', k * 1e23, r'\joule\per\kelvin', exp="e-23")
t.add_si_row(r'\mu_\t{B}', mu_B * 1e24, r'\joule\per\tesla', exp="e-24")
t.add_si_row(r'\si{\atomicmassunit}', u * 1e27, r'\kilogram', exp="e-27")
t.savetable(lt.new('table/constants.tex'))

t = lt.SymbolColumnTable()
t.add(lt.Column([[r'\ce{Dy2O3}', r'\ce{Gd2O3}', r'\ce{Nd2O3}']], 'c'))
t.add_si_column(r'\epsilon', electrons, places=0)
t.add_si_column('S', S, places=1)
t.add_si_column('L', L, places=0)
t.add_si_column('J', J, places=1)
t.add_si_column(r'\chi_\t{theor}', chi)
t.savetable(lt.new('table/chi_theor.tex'))
