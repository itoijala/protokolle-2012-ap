from labtools import *

elements = ['Dy', # lt.file('data/Dy2O3') lt.new("table/Dy2O3.tex")
            'Gd', # lt.file('data/Gd2O3') lt.new("table/Gd2O3.tex")
            'Nd'] # lt.file('data/Nd2O3') lt.new("table/Nd2O3.tex")

mu_0 = lt.vals(lt.constant('mag. constant'))

T = unc.ufloat((sp.constants.C2K([20]), 2))

n, A_coil, l_coil, R_coil = np.loadtxt(lt.file('data/coil'))
A_coil *= 1e-6
l_coil *= 1e-3

nu = np.loadtxt(lt.file('data/nu_max'))
R_0 = np.loadtxt(lt.file('data/R_0'))
R_0 *= 5e-3
U_Br_0 = np.loadtxt(lt.file('data/U_Br_0'))
U_Br_0 *= 1e-3 * 1e-2
U_e = np.loadtxt(lt.file('data/U_e_b'))

omega = 2 * np.pi * nu
R_3 = 1000 - R_0

m_p, l_a, l_g, rho, m, m_err, _, _, _, _ = np.loadtxt(lt.file('data/theory'), unpack=True)
m_p *= 1e-3
l_a *= 1e-2
l_g *= 1e-2
rho *= 1e3
m = unc.unumpy.uarray((m, m_err))

m_O, m_O_err = np.loadtxt(lt.file('data/m_O'), unpack=True)
m_O = unc.ufloat((m_O, m_O_err))

l = l_g - l_a
m_p = m_p * (1 - l_a / l_g)
A = m_p / l / rho

chi_Br = []
chi_R = []

for i in range(len(elements)):
    U_Br, R = np.loadtxt('data/' + elements[i] + '2O3', unpack=True)
    U_Br *= 1e-3 * 1e-2
    R *= 5e-3
    U_Br_mean = lt.mean(U_Br)
    R_mean = lt.mean(R)
    chi_Br.append(lt.mean((U_Br - U_Br_0) / U_e * 4 * l_coil / omega / mu_0 / n**2 / A[i] * np.sqrt(R_coil**2 + (omega * mu_0 * n**2 * A_coil / l_coil)**2)).item())

    chi_R.append(lt.mean(2 * np.abs(R_0 - R) / R_3 * A_coil / A[i]).item())

    t = lt.SymbolColumnTable()
    t.add_si_column(r'U_\t{Br}', U_Br * 1e3 * 1e2, r'\milli\volt', places=0)
    t.add_si_column('R', R / 5e-3, r'\milli\ohm', exp="5", places=0)
    t2 = lt.SymbolRowTable()
    t2.add_si_row(r'\mean{U_\t{Br}}', U_Br_mean * 1e3 * 1e2, r'\milli\volt', places=0)
    t2.add_si_row(r'\mean{R}', R_mean / 5e-3, r'\milli\ohm', exp="5", places=0)
    t3 = lt.Table()
    t3.add(lt.Row())
    t3.add_hrule()
    t3.add(lt.Row())
    t3.add(lt.Column([t, t2], "@{}c@{}"))
    t3.savetable(lt.new("table/" + elements[i] + "2O3.tex"))

t = lt.SymbolRowTable()
t.add_si_row('n', n, places=0)
t.add_si_row(r'A_\t{sp}', A_coil * 1e6, r'\square\milli\meter', places=1)
t.add_si_row(r'l_\t{sp}', l_coil * 1e3, r'\milli\meter', places=0)
t.add_si_row(r'R_\t{sp}', R_coil, r'\ohm')
t.savetable(lt.new('table/coil.tex'))

t = lt.SymbolRowTable()
t.add_si_row('T', sp.constants.K2C(T), r'\degreeCelsius', places=0)
t.add_si_row(r'\nu', nu * 1e-3, r'\kilo\hertz', places=1)
t.add_si_row(r'U_\t{e}', U_e, r'\volt', places=0)
t.add_si_row(r'U_{\t{Br}, 0}', U_Br_0 * 1e3 * 1e2, r'\milli\volt', places=1)
t.add_si_row('R_0', R_0 / 5e-3, r'\milli\ohm', exp="5", places=0)
t.add_si_row(r'm_\ce{O}', m_O, r'\atomicmassunit')
t.savetable(lt.new('table/b.tex'))

t = lt.SymbolColumnTable()
t.add(lt.Column([[r'\ce{Dy2O3}', r'\ce{Gd2O3}', r'\ce{Nd2O3}']], 'c'))
t.add_si_column(r'\rho', rho * 1e-3, r'\gram\per\cubic\centi\meter', places=2)
t.add_si_column(r'm_\t{p}', m_p * 1e3, r'\gram', places=1)
t.add_si_column('m', m, r'\atomicmassunit')
t.add_si_column(r'l_\t{a}', l_a * 1e2, r'\centi\meter', places=1)
t.add_si_column(r'l_\t{g}', l_g * 1e2, r'\centi\meter', places=1)
t.savetable(lt.new('table/m.tex'))

t = lt.SymbolColumnTable()
t.add(lt.Column([[r'\ce{Dy2O3}', r'\ce{Gd2O3}', r'\ce{Nd2O3}']], 'c'))
t.add_si_column(r'\chi_{U_\t{Br}}', chi_Br)
t.add_si_column(r'\chi_R', chi_R)
t.savetable(lt.new('table/chi.tex'))
