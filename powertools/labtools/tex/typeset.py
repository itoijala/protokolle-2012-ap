import os
import subprocess
from check import LogCheck
try:
    from clint.textui import colored
    red = colored.red
    yellow = colored.yellow
    green = colored.green
except:
    red = lambda s: s
    yellow = lambda s: s
    green = lambda s: s

def run(texfile, inputs='.', build_dir='build', bibinputs='.'):
    run_latex(texfile, inputs, build_dir)

    logfile = os.path.join(build_dir, texfile.replace('.tex', '') + '.log')
    if not os.path.exists(logfile):
        ValueError('LaTeX did not create logfile: ' + logfile)

    lc = LogCheck()
    lc.read(logfile)
    messages = list(lc.parse(True, True, True, True))

    if _biber_needed(messages):
        run_biber(texfile, bibinputs, build_dir)
        run_latex(texfile, inputs, build_dir)

    while lc.run_needed():
        run_latex(texfile, inputs, build_dir)
        lc = LogCheck()
        lc.read(logfile)
        messages = list(lc.parse(True, True, True, True))

    _print_logs(messages)

def run_biber(texfile, bibinputs, build_dir):
    print('Running '+green('Biber')+'...')
    texname = texfile.replace('.tex', '')
    log = os.path.join(build_dir, texname+'.blg')
    out = os.path.join(build_dir, texname+'.bbl')
    tar = os.path.join(build_dir, texname+'.bcf')

    cline = r'''BIBINPUTS={}: \
                biber \
                --logfile {} \
                --outfile {} \
                {}
                '''.format(":".join(bibinputs), log, out, tar)
    p = subprocess.Popen(cline, shell=True, stdout=subprocess.PIPE)
    out = p.communicate()[0].decode()
    for line in out.split('\n'):
        if line.startswith('WARN'):
            print(yellow('[WARNING]')+line[6:])
        if line.startswith('ERROR'):
            print(red('[ERROR]')+line[7:])

def run_latex(texfile, inputs, build_dir):
    texname = texfile.replace('.tex', '')

    if not os.path.exists(texfile):
        ValueError('LaTeX file does not exist: ' + texfile)

    if not os.path.exists(build_dir):
        os.makedirs(build_dir)

    cline = r'''TEXINPUTS={0}: \
                max_print_line=1048576 \
                lualatex --shell-escape \
                --interaction=batchmode \
                --output-directory={1} \
                {2}.tex
                '''.format(':'.join(inputs), build_dir, texname)
    print('Running '+green('LuaLaTeX')+'...')
    p = subprocess.Popen(cline, shell=True, stdout=subprocess.PIPE)
    p.communicate()

def _biber_needed(messages):
    for m in messages:
        if 'text' in m and m['text'].startswith('Please (re)run Biber on the file:'):
            return True
    return False

def _print_logs(messages):
    for m in messages:
        line = ''
        if 'kind' in m:
            if m['kind'] == 'error':
                line += red('[ERROR] ')
            elif m['kind'] == 'warning':
                line += yellow('[WARNING] ')
        if 'file' in m:
            line += m['file']
        if 'line' in m and m['line'] != None:
            line += ':' + m['line']
        line += ' '
        if 'text' in m:
            line += m['text']
        if 'page' in m:
            line += ' (page {})'.format(m['page'])
        print(line)

