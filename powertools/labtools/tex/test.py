#!/usr/bin/python3

from typeset import run

run('main.tex', inputs=['../powertools/', '../powertools/texmf//'], bibinputs=['../powertools'])
