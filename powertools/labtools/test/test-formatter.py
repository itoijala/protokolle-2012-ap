#!/usr/bin/env python3

from labtools import *

f = SiFormatter()
print(f.format_data(123.456))
print(f.format_data(1234))

f = SiFormatter(2)
print(f.format_data(123.456))
print(f.format_data(1234))

f = SiFormatter(-2)
print(f.format_data(123.456))
print(f.format_data(1234))

f = SiFormatter(2, 1e-5, exp=True)
print(f.format_data(123.456))
print(f.format_data(1234))
