\input{header/report.tex}

\SetExperimentNumber{203}

\begin{document}
  \maketitlepage{Verdampfungswärme und Dampfdruckkurve}{08.05.2012}{15.05.2012}
  \section{Ziel}
    Ziel des Versuchs ist es, den Wechsel des Aggregratzustandes von Wasser vom flüssigen in den gasförmigen in einem geschlossenen System zu untersuchen und die dafür benötigte Verdampfungsenergie zu errechen.
  \section{Theorie}
    In der Physik wird ein räumlich begrenztes System, das einen homogenen Stoff enthält, als Phase definiert.
    Beispiele für Phasen sind die Aggregratzustände fest, flüssig und gasförmig.
    Die Abhängigkeit der Aggregratzustände von Druck und Temperatur wird in so genannten Zustandsdiagrammen, wie in Abbildung \ref{fig:zustandsdiagramm}, visualisiert.
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/zustandsdiagramm.pdf}
      \caption{Beispiel eines Zustandsdiagramms \cite{anleitung203}}
      \label{fig:zustandsdiagramm}
    \end{figure}
    Dabei lässt die Variation der Freiheitsgrade Druck~$p$ und Temperatur~$T$ innerhalb der dort aufgezeichneten Fläche den jeweiligen Aggregratzustand invariant.
    Befinden sich die Freiheitsgrade im Bereich der durch die Punkte TP und KP gekennzeichnete Dampfdruckkurve, so existieren im System zwei Phasen nebeneinander.
    Da dieser Zustand nur im Bereich der Dampfdruckkurve auftritt, reduziert sich hier die Anzahl der Freiheitsgrade wegen der Abhängigkeit der Parameter Druck und Temperatur auf einen Freiheitsgrad.
    Im Punkt TP befindet sich das System in einer Überlagerung aller drei Aggregratzustände.
    Der Verlauf der Dampfdruckkurve wird im Wesentlichen von der stoffcharakteristischen und temperaturabhängigen Größe $\f{L}(T)$, die als Verdampfungswärme bezeichnet wird, beeinflusst.
    Erreicht das System die kritische Temperatur $T_\t{krit}$, so verschwindet die Verdampfungswärme im kritischen Punkt KP auf der Dampfdruckkurve.
    Bei Drücken kleiner als \SI{1}{\bar} kann $L$ als konstant angesehen werden.
    \subsection{Mikroskopische Vorgänge bei Kondensation und Verdampfung} \label{mikroskopisch}
      Befindet sich eine Flüssigkeit in einem evakuierten Gefäß, treten Teile der Flüssigkeit in den gasförmigen Zustand über und erhöhen durch Ausfüllen des über der Flüssigkeit befindlichen Raums den Druck.
      Dabei verlassen unter Überwindung der intermolekularen Kräfte diejenigen Moleküle die Flüssigkeit, deren kinetischen Energie entsprechend der Maxwellschen Geschwindigkeitsverteilung maximal ist.
      Die Energie, die für den Austritt aus der Flüssigkeit notwendig war, äußert sich in einer Temperaturerniedrigung der Flüssigkeit.
      Sie wird durch die bereits eingeführte molekulare Verdampfungswärme beschrieben; das ist die Energie, die benötigt wird, um eine Flüssigkeit in Dampf gleicher Temperatur überzuführen, beziehungsweise ist es die Energie, die beim umgekehrten Vorgang frei wird.
      Mikroskopisch betrachtet entsteht Druck durch Stoßprozesse zwischen den Molekülen, die gegen die Flüssigkeit oder die Gefäßwände prallen; es treten ebenso erneut Moleküle in die Flüssigkeit ein, sodass ein Kreislauf ensteht, dessen Fluss durch die Wasseroberfläche nach einer gewissen Zeit, im zeitlichen Mittel betrachtet, verschwindet; es treten also genauso viele Moleküle aus dem Wasser aus, wie sie durch Kondensationsvorgänge wieder ins Wasser zurückkehren.

      Makroskopisch betrachtet geschieht nichts mehr; der Stoff koexistiert jetzt im gasförmigen und flüssigen Zustand.
      Wegen der im zeitlichen Mittel konstanten Teilchenzahl im gasförmigen Zustand, wird makroskopisch ein sich nicht mehr verändernder Druck, der Sättigungsdampfdruck, gemessen.
      Der Sättigungsdampfdruck wird mit zunehmender Temperatur erst nach längerer Zeit erreicht, da die Maxwellsche Geschwindigkeitsverteilung in Richtung größerer Geschwindigkeit verschoben wird; somit steigt die Zahl der Teilchen, die die erforderliche Energie aufbringen, um die Flüssigkeit zu verlassen.
      Allerdings ist der Druck des koexistierenden Dampfes und der Flüssigkeit volumenunabhängig.
    \subsection{Herleitung einer Differentialgleichung für den Druckverlauf}
      Wird das Volumen $V$ der Phase geändert, so beginnt der in \ref{mikroskopisch} erläuterte Vorgang erneut, bis der Gleichgewichtszustand erreicht ist.
      Daher gilt die Allgemeine Gasgleichung
      \begin{eqn}
        pV=RT
      \end{eqn}
      hier nicht, wobei $R$ die allgemeine Gaskonstante ist.
      Durch Ausrechnen eines reversiblen Kreisprozesses, bei dem ein Mol eines Stoffes isotherm und isobar verdampft, beziehungsweise ebenso kondensiert, wird eine Differentialgleichung hergeleitet.
      Ein Beispiel eines solchen Kreisprozesses ist in Abbildung \ref{fig:kreisprozess} gegeben; hierbei ist der Druck gegen das Volumen aufgetragen.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/kreisprozess.pdf}
        \caption{Betrachteter Kreisprozess \cite{anleitung203}}
        \label{fig:kreisprozess}
      \end{figure}

      Sei zu Beginn der Ausgangszustand A dadurch gekennzeichnet, dass der Stoff nur im flüssigen Aggregratzustand vorkommt.
      Durch Hinzufügen einer Wärmemenge $\dif{Q_\t{AB}}$ nimmt das System den Druck $p + \dif{p}$ und die Temperatur $T + \dif{T}$ an und erreicht damit Zustand B.
      Für die Wärmemenge gilt der lineare Zusammenhang
      \begin{eqn}
        \dif{Q_\t{AB}} = C_\t{F} \dif{T} , \eqlabel{eqn:wärmemenge_dampf}
      \end{eqn}
      wobei $C_\t{F}$ die Molwärme der Flüssigkeit ist.
      Das System erreicht Zustand C, indem durch Zufuhr der molaren Verdampfungswärme
      \begin{eqn}
        \f{L}(T + \dif{T}) = \f{L}(T) + \dif{L} \eqlabel{eqn:verdampfung}
      \end{eqn}
      die Flüssigkeit isotherm und isobar verdampft, also nur noch Dampf des Volumens $V_\t{D}$ vorhanden ist.
      Bei diesem Vorgang leistet das System die Arbeit
      \begin{eqn}
        -A_\t{BC} = \g(p + \dif{p}) \g(V_\t{D} - V_\t{F}) ,
      \end{eqn}
      wobei $V_\t{D}$ die Volumen des Dampfs und $V_\t{F}$ das der Flüssigkeit ist.
      Beim Übergang in Zustand D wird der Dampf abgekühlt und gibt dabei die Wärmemenge
      \begin{eqn}
        -\dif{Q_\t{AB}} = C_\t{D} \dif{T} , \eqlabel{eqn:wärmemenge_kondens}
      \end{eqn}
      ab, wobei $C_\t{D}$ die Molwärme des Dampfes ist.
      Durch Zufuhr der mechanischen Energie
      \begin{eqn}
        A_\t{DA} = p\g(V_\t{D} - V_\t{F}) ,
      \end{eqn}
      also durch die Verkleinerung des Volumens, wird der Dampf zu der Flüssigkeit in Zustand A kondensiert.
      Aus der Summe der Gleichungen \eqref{eqn:wärmemenge_dampf}, \eqref{eqn:verdampfung} und \eqref{eqn:wärmemenge_kondens} ergibt sich die insgesamt dem System zugeführte Wärme
      \begin{eqn}
        \dif{Q_\t{ges}} = C_\t{F} \dif{T} - C_\t{D} \dif{T} + \f{L}(T) + \dif{L} - \f{L}(T) . \eqlabel{eqn:wärmemenge_gesamt}
      \end{eqn}
      Laut dem ersten Hauptsatz der Thermodynamik ist Gleichung \eqref{eqn:wärmemenge_gesamt} gleich der infinitesimalen Arbeit
      \begin{eqns}
        \dif{A_\t{DA}} &=& \g(V_\t{D} - V_\t{F}) \dif{p}, \\
        \itext{es gilt also}
        \g(C_\t{F} - C_\t{D}) \dif{T} + \dif{L} &=& \g(V_\t{D} - V_\t{F}) \dif{p} . \eqlabel{eqn:erg_1.haupt}
      \end{eqns}
      Weil hier ein reversibler Kreisprozess vorliegt, gilt nach dem zweiten Hauptsatz der Thermodynamik, dass die Summe der reduzierten Wärmemengen, also den jeweiligen Quotienten aus der Wärmemenge und der Temperatur, verschwindet.
      Es ergibt sich also
      \begin{eqn}
        \frac{C_\t{F} \dif{T}}{T} + \frac{L + \dif{L}}{T + \dif{T}} - \frac{C_\t{D} \dif{T}}{T} - \frac{L}{T} = 0 . \eqlabel{eqn:folg_2.haupt}
      \end{eqn}
      Durch eine Taylorentwicklung des zweiten Terms bis zur ersten Ordnung reduziert sich diese Gleichung auf
      \begin{eqn}
        \g(C_{F} - C_{D}) \dif{T} + \dif{L} - \frac{L \dif{T}}{T} = 0 . \eqlabel{eqn:erg_2.haupt}
      \end{eqn}
      Nach einem Vergleich von Gleichung \eqref{eqn:erg_1.haupt} mit \eqref{eqn:erg_2.haupt} ergibt sich schließlich die so genannte Clausius-Clapeyronsche Gleichung
      \begin{eqn}
        \g(V_\t{D} - V_\t{F}) \dif{p} = \frac{L}{T} \dif{T} . \eqlabel{eqn:clausius-clapeyron}
      \end{eqn}
      \subsubsection{Integration der Clausius-Clapeyronschen Gleichung}
         Da im Allgemeinen $V_\t{D}$, $V_\t{F}$ und $L$ komplizierte temperaturabhängige Funktionen sind, müssen vereinfachende Annahmen zur analytischen Integration von Gleichung \eqref{eqn:clausius-clapeyron} gemacht werden.

         Liegt die Systemtemperatur weit unter der kritischen Temperatur $T_\t{krit}$, so ist $V_\t{F}$ gegenüber $V_\t{D}$ zu vernachlässigen, es gilt die allgemeine Gasgleichung und $L$ ist druck- und temperaturunabhängig.
         Dann folgt für Gleichung \eqref{eqn:clausius-clapeyron}
         \begin{eqn}
           \frac{R}{p} \dif{p} = \frac{L}{T^2} \dif{T} . \eqlabel{eqn:clausius_clapeyron_einfach}
         \end{eqn}
         Nach Seperation der Variablen in Gleichung \eqref{eqn:clausius_clapeyron_einfach} ergibt sich schließlich für den Druck
         \begin{eqn}
           p = p_0 \exp(- \frac{L}{R} \frac{1}{T}) . \eqlabel{eqn:druck}
         \end{eqn}
         Nach ausschließlich kinetischen Überlegungen folgt eine Proportionalität zwischen dem Druck oberhalb der Flüssigkeit und der Zahl der Moleküle in der Dampfphase
         \begin{eqn}
           p \sim \exp(- \frac{W}{k_\t{B} T}) ,
         \end{eqn}
         wobei $k_\t {B}$ die Boltzmann-Konstante und $W$ die Energie ist, die die Moleküle benötigen, um die Flüssigkeit verlassen zu können.
  \section{Aufbau}
    \subsection{Niedrigdruckmessung}
      Der Aufbau für die Messungen unter $\SI{1}{\bar}$ ist in Abbildung \ref{fig:aufbau1} skizziert.
      Mit Hilfe einer Wasserstrahlpumpe wird im System ein Vakuum erzeugt.
      Die Pumpe ist vor einem Absperrhahn an eine Woulffsche Flasche angeschlossen, die dazu dient, zu verhindern, dass kaltes Wasser an heißes Glas gelangt.
      Die zwei weiteren Ausgänge der Woulffschen Flasche führen zu einem Belüftungsventil und über ein Drosselventil zur eigentlichen Messapparatur.
      Hinter dem Drosselventil befindet sich ein Rückflusskühler, der entsprechend an einen zweiten Wasserhahn angeschlossen ist; in ihm kann der Dampf kondensieren, sodass kein Wasser in das Manomateter gelangt.
      Der Rückflusskühler endet in einem Mehrhalskolben, in den zwei Thermometer hereinragen, die jeweils die Temperatur des Gases und des Wassers messen.
      Für die Überwachung des Drucks ist am oberen Ende des Rückflusskühlers das Manometer angebracht.
      Um das Wasser zu erhitzen, steht der Mehrhalskolben in einer strombetriebenen Heizhaube.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/aufbau1.pdf}
        \caption{Aufbau der Niedrigdruckmessung \cite{anleitung203}}
        \label{fig:aufbau1}
      \end{figure}
    \subsection{Hochdruckmessung}
      Für die Messungen über $\SI{1}{\bar}$ wird die Messaparatur aus Abbildung \ref{fig:aufbau2} verwendet.
      Sie besteht aus einem metallischen, verschließbaren  Hohlzylinder in dessen Hohlraum Wasser gefüllt wird.
      Der Zylinder kann mit einer Heizwicklung erhitzt werden und ist an einem Ende verschlossen.
      An der anderen Seite ist hinter einem U-Rohr, das aus dem Zylinder rausführt ein Manometer angebracht.
      Das U-Rohr wird gekühlt, bevor es das Manometer erreicht.
      Des Weiteren ist im Mantel des Hohlzylinders ein Temperaturfühler angebracht.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/aufbau2.pdf}
        \caption{Aufbau der Hochdruckmessung \cite{anleitung203}}
        \label{fig:aufbau2}
      \end{figure}
  \section{Durchführung}
    Bei der Niedrigdruckmessung wird das System evakuiert, das heißt auf den niedrigsten erreichbaren Druck gebracht.
    Anschließend wir die Heizhaube eingeschaltet, sodass das Wasser im Mehrhalskolben siedet.
    Damit wird der in der Theorie beschriebene Vorgang initiert.
    Druck $p$ und die Temperatur des Gases $T_\t{G}$ sowie des Wassers $T_\t{F}$ werden bei signifikanten Änderungen einer dieser Größen aufgeschrieben, bis das System annähernd Atmospährendruck oder eine Temperatur von ungefähr $\SI{100}{\degreeCelsius}$ erreicht hat.

    Bei der Hochdruckmessung wird der Hohlzylinder mit Wasser gefüllt, sodass keine Luft im Innenraum mehr vorhanden ist.
    Dann wird das System verschlossen und mit Strom erhitzt.
    Der Druck $p$ und die Temperatur $T$ werden bei signifikanten Änderungen einer der beiden Größen bis zu einem Druck von $\SI{15}{\bar}$ abgelesen.
  \section{Messwerte}
    Die Messwerte der Niedrigdruckmessung befinden sich in Tabelle \ref{tab:klein}, die der Hochdruckmessung in Tabelle \ref{tab:groß}.
    \begin{table}
      \OverfullCenter{\input{table/klein.tex}}
      \caption{Messwerte der Niedrigdruckmessung}
      \label{tab:klein}
    \end{table}
    \begin{table}
      \OverfullCenter{\input{table/groß.tex}}
      \caption{Messwerte der Hochdruckmessung}
      \label{tab:groß}
    \end{table}
  \section{Auswertung}
    \input{statistics.tex}
    \input{linregress.tex}
    \subsection{Niedrigdruckmessung}
      Bei den folgenden Berechnung wird von einer Wassermenge von \SI{1}{\mole} ausgegangen.
      Durch Umformung von Gleichung \eqref{eqn:druck} ergibt sich
      \begin{eqn}
        \ln(\frac{p}{\g[p]}) = A \frac{1}{T_\t{G}} + B  = - \frac{L}{R} \frac{1}{T_\t{G}} + B. \eqlabel{eqn:fitformel}
      \end{eqn}
      In Abbildung \ref{fig:klein} ist, entsprechend Formel \eqref{eqn:fitformel}, der Logarithmus des Dampfdrucks gegen die reziproke absolute Temperatur aufgetragen.
      Anhand dieser Werte wurde eine lineare Ausgleichsrechnung durchgeführt, die Regressionsgerade ist ebenfalls in Abbildung \ref{fig:klein} und die Ergebnisse in Tabelle \ref{tab:klein-L} eingetragen.
      Die Verdampfungswärme ergibt sich aus dem negativen Produkt von allgemeiner Gaskonstante $R$ und Steigungsparameter $A$ zu einem Wert von 
      \begin{eqn}
        \input{result/L.tex}.
      \end{eqn}
      \begin{table}
        \OverfullCenter{\input{table/klein-L.tex}}
        \caption{Verwendete Konstanten \cite{scipy}, Ergebnisse der linearen Regression, Berechnete Werte von $L$, $L_\t{a}$ und $L_\t{i}$}
        \label{tab:klein-L}
      \end{table}

      Um die Verdampfungswärme zu bestimmen, wird die Formel 
      \begin{eqn}
        L_\t{a} = \g(p + \dif{p}) \g(V_\t{D} - V_\t{F})
      \end{eqn}
      unter Verwendung der allgemeinen Gasgleichung zu
      \begin{eqn}
        L_\t{a} \approx \frac{R T}{N_\t{A}}
      \end{eqn}
      genähert, wobei T einen Wert von $\SI{373}{\kelvin}$ hat.
      Mit den gemessenen Werten ergibt sich
      \begin{eqn}
       \input{result/La.tex} .
      \end{eqn}
      Weiterhin gilt für die molare Verdampfungswärme pro Molekül
      \begin{eqns}
        L_\t{i} &=& \frac{L}{N_\t{A}} - L_\t{a} \\
        \input{result/Li.tex} .
      \end{eqns}
      Zusammenfassend sind die sich ergebenden Werte ebenfalls in Tabelle \ref{tab:klein-L} aufgeführt.
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/klein.pdf}
        \caption{Plot des Drucks gegen die Temperatur}
        \label{fig:klein}
      \end{figure}
      \FloatBarrier
    \subsection{Hochdruckmessung}
      Aus Gleichung \eqref{eqn:clausius-clapeyron} folgt, bei Vernachlässigung von $V_\t{F}$ und mit $V = V_\t{D}$,
      \begin{eqn}
        L = V T \d{T}{p}; \eqlabel{eqn:L_hoch}.
      \end{eqn}
      Für $V$ wird aus der Gleichung 
      \begin{eqn}
        \g(p + \frac{a}{V^2}) V = R T
        \itext{die Näherung}
        V = \frac{1}{2 p} \g(R T + \sqrt{R^2 T^2 - 4 a p})
      \end{eqn}
      hergeleitet.
      Um die Ableitung von $p$ nach $T$ zu bekommen, wird $\f{p}(T)$ durch
      \begin{eqn}
        p = \sum_{i = 0}^4 A_i T^i
      \end{eqn}
      genähert.
      Die Koeffizienten $A_i$ müssen durch eine Ausgleichsrechnung approximiert werden; die Ergebnisse stehen in Tabelle \ref{tab:A}.
      Dort steht ebenfalls der verwendete Wert von $a$.
      In Abbildung \ref{fig:groß} ist der Druck $p$ gegen $T$ aufgetragen, des Weiteren befindet sich dort ebenfalls die berechnete Ausgleichskurve.
      Aus dessen Ableitung
      \begin{eqn}
        \d{T}{p}; = \sum_{i = 1}^4 i A_i T^{i - 1}
      \end{eqn}
      lässt sich anhand von \eqref{eqn:L_hoch} $L$ in Abhängigkeit von $T$ ermitteln, die Ergebnisse sind in Tabelle \ref{tab:groß-L} aufgeführt.
      \begin{table}
        \input{table/A.tex}
        \caption{Ergebnisse der Ausgleichsrechnung für $\f{p}(T)$}
        \label{tab:A}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/groß.pdf}
        \caption{Druck gegen die Temperatur aufgetragen}
        \label{fig:groß}
      \end{figure}
      \begin{table}
        \OverfullCenter{\input{table/groß-L.tex}}
        \caption{Berechnete Werte von $L$ in Abhängigkeit von $T$}
        \label{tab:groß-L}
      \end{table}
      Aus Abbildung \ref{fig:L}, in der die molare Verdampfungswärme $L$ gegen die Temperatur aufgetragen ist, wird ersichtlich, dass $L$ von der Temperatur abhängt.
      Hierbei sind die ersten drei Werte nicht aufgetragen, da sie sehr viel größer als die restlichen sind.
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/L.pdf}
        \caption{Verdampfungswärme $L$ gegen die Temperatur aufgetragen}
        \label{fig:L}
      \end{figure}
  \section{Diskussion}
    \subsection{Niedrigdruckmessung}
      Die Ausgleichsrechnung zur Ermittlung von $L$ ergab Parameter mit kleinem Fehler, was einen kleinen Fehler für $L$ nach sich zog.
      Der Wert für $L_\t{a}$ ist klein, was ein Indiz dafür ist, dass die Volumenänderung vernachlässigbar ist.
      Die Verdampfungswärme pro Molekül übersteigt die äußere Verdampfungswärme deutlich, was bedeutet, dass ein wesentlich größerer Energieaufwand benötigt wird, den Aggregratzustand zu ändern, als die Volumenänderung durchzuführen.
    \subsection{Hochdruckmessung}
      Bei der Bestimmung von $\f{L}(T)$ ergeben sich durch Fehlerfortpflanzung sehr große Fehler von ungefähr \SI{1000}{\percent}.
      Aus diesem Grund sind die Werte von $L$ in Tabelle~\ref{tab:groß-L} ohne Fehler angegeben.
      Wie in der Theorie beschrieben wird $L$ kleiner, wenn die Temperatur sich dem kritischen Punkt nähert.
  \makebibliography
\end{document}
