from labtools import *

p, T = np.loadtxt(lt.file('data/groß'), unpack=True)
p *= sp.constants.bar
T = sp.constants.C2K(T)

def poly(T, A, B, C, D, E):
    return np.polyval([A, B, C, D, E], T)

A = lt.curve_fit(poly, T, p)

R = sp.constants.physical_constants['molar gas constant']
R = unc.ufloat((R[0], R[2]))
a = 0.9

V = (R * T + unc.unumpy.sqrt(R**2 * T**2 - 4 * a * p)) / (2 * p)

L = V * T * (4 * A[0] * T**3 + 3 * A[1] * T**2 + 2 * A[2] * T + A[3])

t = lt.SymbolColumnTable()
t.add_si_column('p', p[:len(p) / 4 + 1] / sp.constants.bar, r'\bar', places=2)
t.add_si_column('T', sp.constants.K2C(T[:len(T) / 4 + 1]), r'\degreeCelsius', places=1)
t.add(lt.VerticalSpace(len(T) / 4, '1em'))
t.add_si_column('p', p[len(p) / 4 + 1:len(p) / 2 + 1] / sp.constants.bar, r'\bar', places=2)
t.add_si_column('T', sp.constants.K2C(T[len(T) / 4 + 1:len(T) / 2 + 1]), r'\degreeCelsius', places=1)
t.add(lt.VerticalSpace(len(T) / 4, '1em'))
t.add_si_column('p', p[len(p) / 2 + 1:len(p) / 4 * 3 + 2] / sp.constants.bar, r'\bar', places=2)
t.add_si_column('T', sp.constants.K2C(T[len(T) / 2 + 1:len(T) / 4 * 3 + 2]), r'\degreeCelsius', places=1)
t.add(lt.VerticalSpace(len(T) / 4, '1em'))
t.add_si_column('p', p[len(p) / 4 * 3 + 2:] / sp.constants.bar, r'\bar', places=2)
t.add_si_column('T', sp.constants.K2C(T[len(T) / 4 * 3 + 2:]), r'\degreeCelsius', places=1)
t.savetable(lt.new('table/groß.tex'))

t = lt.SymbolRowTable()
t.add_si_row('a', a, r'\joule\meter\cubed\per\mole\squared')
t.add_hrule()
t.add_si_row('A_4', A[0] * 1e3, r'\milli\pascal\per\kelvin\tothe{4}')
t.add_si_row('A_3', A[1], r'\pascal\per\kelvin\tothe{3}')
t.add_si_row('A_2', A[2] * 1e-3, r'\kilo\pascal\per\kelvin\tothe{2}')
t.add_si_row('A_1', A[3] * 1e-3, r'\kilo\pascal\per\kelvin')
t.add_si_row('A_0', A[4] * 1e-6, r'\mega\pascal')
t.savetable(lt.new('table/A.tex'))

t = lt.SymbolColumnTable()
t.add_si_column('T', T[:len(T) / 4 + 1], r'\kelvin', places=1)
t.add_si_column('L', lt.vals(L)[:len(L) / 4 + 1], r'\joule\per\mole', places=0)
t.add(lt.VerticalSpace(len(T) / 4, '1em'))
t.add_si_column('T', T[len(T) / 4 + 1:len(T) / 2 + 1], r'\kelvin', places=1)
t.add_si_column('L', lt.vals(L)[len(L) / 4 + 1:len(L) / 2 + 1], r'\joule\per\mole', places=0)
t.add(lt.VerticalSpace(len(T) / 4, '1em'))
t.add_si_column('T', T[len(T) / 2 + 1:len(T) / 4 * 3 + 2], r'\kelvin', places=1)
t.add_si_column('L', lt.vals(L)[len(L) / 2 + 1:len(L) / 4 * 3 + 2], r'\joule\per\mole', places=0)
t.add(lt.VerticalSpace(len(T) / 4, '1em'))
t.add_si_column('T', T[len(T) / 4 * 3 + 2:], r'\kelvin', places=1)
t.add_si_column('L', lt.vals(L)[len(L) / 4 * 3 + 2:], r'\joule\per\mole', places=0)
t.savetable(lt.new('table/groß-L.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(T, p, 'rx', label='Messwerte')
x = np.linspace(300, 480, 10000)
ax.plot(x, poly(x, *lt.vals(A)), 'b-', label='Ausgleichskurve')
ax.yaxis.set_view_interval(-50000, 2000000, ignore=True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter(1, factor=1e-6))
ax.set_xlabel(r'\silabel{T}{\kelvin}')
ax.set_ylabel(r'\silabel{p}{\mega\pascal}')
ax.legend(loc='upper left')
fig.savefig(lt.new('graph/groß.tex'), bbox_inches='tight', pad_inches=0)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(T[3:], lt.vals(L)[3:], 'rx')
x = np.linspace(300, 480, 10000)
ax.yaxis.set_view_interval(9e3, 70e3, ignore=True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e-3))
ax.set_xlabel(r'\silabel{T}{\kelvin}')
ax.set_ylabel(r'\silabel{L}{\kilo\joule\per\mole}')
fig.savefig(lt.new('graph/L.tex'), bbox_inches='tight', pad_inches=0)
