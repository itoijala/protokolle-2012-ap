from labtools import *

p, T_G, T_F = np.loadtxt(lt.file('data/klein'), unpack=True)
p *= 1e-3 * sp.constants.bar
T_G = sp.constants.C2K(T_G)
T_F = sp.constants.C2K(T_F)

A, B = lt.linregress(1 / T_G, np.log(p))

R = sp.constants.physical_constants['molar gas constant']
R = unc.ufloat((R[0], R[2]))
N_A = sp.constants.physical_constants['Avogadro constant']
N_A = unc.ufloat((N_A[0], N_A[2]))

L = - R * A

T0 = 373
La = R * T0 / sp.constants.eV / N_A
Li = L / sp.constants.eV / N_A - La

open(lt.new('result/L.tex'), 'w').write(r'L = \SI{' + lt.round_to(float(lt.vals(L) * 1e-3), float(lt.stds(L) * 1e-3)) +  ' \pm ' + lt.round_figures(float(lt.stds(L) * 1e-3), 1) + r'}{\kilo\joule\per\mole}')
open(lt.new('result/Li.tex'), 'w').write(r'&=& \SI{' + lt.round_to(float(lt.vals(Li)), float(lt.stds(Li))) +  ' \pm ' + lt.round_figures(float(lt.stds(Li)), 1) + r'}{\electronvolt}')
open(lt.new('result/La.tex'), 'w').write(r'L_\t{a} = \SI{' + lt.round_to(float(lt.vals(La)), float(lt.stds(La))) +  ' \pm ' + lt.round_figures(float(lt.stds(La)), 1) + r'}{\electronvolt}')

t = lt.SymbolColumnTable()
t.add_si_column('p', p[:len(p) / 2] / sp.constants.bar * 1e3, r'\milli\bar', places=0)
t.add_si_column(r'T_\t{G}', sp.constants.K2C(T_G[:len(T_G) / 2]), r'\degreeCelsius', places=1)
t.add_si_column(r'T_\t{F}', sp.constants.K2C(T_F[:len(T_F) / 2]), r'\degreeCelsius', places=1)
t.add(lt.VerticalSpace(len(p) / 2, '1em'))
t.add_si_column('p', p[len(p) / 2:] / sp.constants.bar * 1e3, r'\milli\bar', places=0)
t.add_si_column(r'T_\t{G}', sp.constants.K2C(T_G[len(T_G) / 2:]), r'\degreeCelsius', places=1)
t.add_si_column(r'T_\t{F}', sp.constants.K2C(T_F[len(T_F) / 2:]), r'\degreeCelsius', places=1)
t.savetable(lt.new('table/klein.tex'))

t = lt.SymbolRowTable()
t.add_si_row('R', R, r'\joule\per\mole\per\kilogram')
t.add_si_row(r'N_\t{A}', N_A * 1e-23, r'\per\mole', exp='e23')
t.add_hrule()
t.add_si_row('A', A * 1e-3, r'\kilo\kelvin')
t.add_si_row('B', B)
t.add_hrule()
t.add_si_row('L', L * 1e-3, r'\kilo\joule\per\mole')
t.add_hrule()
t.add_si_row(r'L_\t{a}', La, r'\electronvolt')
t.add_si_row(r'L_\t{i}', Li, r'\electronvolt')
t.savetable(lt.new('table/klein-L.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(0.00265, 0.0032, 10000)
ax.plot(x, lt.vals(A * x + B), 'b-', label='Regressionsgerade')
ax.plot(1 / T_G, np.log(p), 'rx', label='Messwerte')
ax.xaxis.set_view_interval(0.00265, 0.0032, ignore=True)
ax.xaxis.set_major_formatter(lt.SiFormatter(factor=1e4))
ax.yaxis.set_major_formatter(lt.SiFormatter(1))
ax.set_xlabel(r'\silabel{\displaystyle \frac{1}{T_\t{G}}}[e-4]{\per\kelvin}')
ax.set_ylabel(r'$\displaystyle \ln(\frac{p}{\si{\pascal}})$')
lt.ax_reverse_legend(ax, 'upper right')
fig.savefig(lt.new('graph/klein.tex'), bbox_inches='tight', pad_inches=0)
