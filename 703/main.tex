\input{header/report.tex}

\SetExperimentNumber{703}

\begin{document}
  \maketitlepage{Das Geiger-Müller-Zählrohr}{17.04.2012}{24.04.2012}
  \section{Ziel}
    In diesem Versuch sollen Charakteristik und Funktionsweise des Geiger-Müller-Zählrohrs, das dazu dient, ionisierende Strahlung nachzuweisen, untersucht werden.
  \section{Theorie}
    \subsection{Aufbau und Funktionsweise des Zählrohrs}
      Das Geiger-Müller-Zählrohr ist ein mit einem Gasgemisch gefüllter Zylinderkondensator, dessen Mantel mit Radius $r_\t{k}$ als Kathode, und ein zentral verlaufender Draht mit dem Radius $r_\t{a}$ als Anode dient.
      Eine am Kondensator anliegende Spannung $U$ mit dem Betrag \SIrange{300}{2000}{\volt} erzeugt ein radialsymmetrisches Feld mit dem Betrag
      \begin{eqn}
        \f{E}(r)= \frac{U}{r \ln(\frac{r_\t{k}}{r_\t{a}})}.
      \end{eqn}
      Eine Ladung wird also innerhalb des Kondensators proportional zu $r^{-1}$ beschleunigt.
      Dabei gibt es seine Energie bei Ionisationsvorgängen ab, dessen Anzahl proportional zur Teilchenenergie ist.
      Diese liegt im Mittel in der Größenordnung $\SI{100}{\kilo\electronvolt}$ und die für die Ionisation benötigte Energie bei $\SI{26}{\electronvolt}$.
      Abhängig von der anliegenden Spannung folgen auf diese so genannte Primärionisation diverse Vorgänge.
      Diese grenzen sich in charakteristische Bereiche ab, die qualitativ in Abbildung \ref{fig:detektorbereich} veranschaulicht sind.
      \begin{figure}
        \includegraphics{graphic/detektorbereich.pdf}
        \caption{Zählrohrbereiche \cite{anleitung703}}
        \label{fig:detektorbereich}
      \end{figure}

      Ist die Spannung klein, mindern häufig auftretende Rekombinationsvorgänge stark die Anzahl der am Draht ankommenden negativen Ladungen.
      Dies ist in Abbildung \ref{fig:detektorbereich} als Bereich I gekennzeichnet.
      Die Wahrscheinlichkeit für diese Rekombinationsvorgänge stagniert schnell, sobald die Spannung größer wird, also sobald Bereich II erreicht wird; deutlich mehr Ladungsträger erreichen nun den Draht, da die Anzahl der Elektronen-Ionen-Paare gestiegen ist.
      Steigt die Spannung weiter, so erreicht das System Bereich III.
      Nun bildet sich eine Proportionalität zwischen fließendem Ionisationsstrom und Energie, beziehungsweise der Intensität einfallender Strahlung aus.
      Die so enstehende Ionisationskammer kann schon zum Nachweis hoher Strahlintensitäten eingesetzt werden.
      Erhöht man die Spannung wieder, so können die geladenen Teilchen zwischen den Stoßvorgängen genug Energie aufnehmen, um wieder zu ionisieren; dieser Vorgang wird als Stoßionisation bezeichnet.
      Die so enstehenden Ladungsträger werden nach einer kurzen Zeit fähig, wiederum zu ionisieren und lösen damit eine so genannte Townsend-Lawine aus, die am Draht als Ladungsimpuls sichtbar ist.
      Dieser Ladungsimpuls repräsentiert die Teilchenenergie, weil die gemessene Ladung $Q$ immer noch proportional zu der ursprünglich an das Gasvolumen abgegebenen Energie ist.
      In diesem Bereich sind folgedessen Energiemessungen möglich; der Detektor wird hier als Proportionalzählrohr bezeichnet; für höhere Spannungen ist die Proportionalität nur noch begrenzt gegeben.
      Übersteigt die anliegende Spanung den Proportionalbereich, so verschwindet die Abhängigkeit der Ladung $Q$ von der Einfallsenergie; dieser Bereich IV wird als Auslösebereich bezeichnet.
      Hier befindet sich die Betriebsspannung des Geiger-Müller-Zählrohrs im engeren Sinn, der Ladungsimpuls ist nun unabhängig vom Ionisationsvermögen der einfallenden Strahlung.
      Die Townsend-Lawinen erzeugen zahlreiche UV-Photonen durch die Anregung von Argon-Atomen nach einem Elektronenstoß, die wiederum freie Elektronen erzeugen.
      Die ungeladenen Photonen können sich auch senkrecht zum elektrischen Feld ausbreiten und erzeugen globale Elektronenlawinen.
      Die Entladung breitet sich bei dieser Spannung nicht nur lokal in Lawinen sondern längs des gesamten Zählrohres aus.
      Die gemessene Ladung nur noch vom Volumen des Zählrohrs und der Höhe der Betriebsspannung und nicht mehr von der Einfallsenergie abhängig.
      Nun sind nur noch Intensitätsmessungen möglich.
      Überschreitet die Spannung den Zählrohrbereich, so kommt es zu spontanen Selbstentladungen, die das Geiger-Müller-Zählrohr zerstören. 
    \subsection{Totzeit und Nachentladungen}
      Im Gegensatz zu den sich schnell zum Draht bewegenden Elektronen, resultiert bei den Ionen aus ihrer Massenträgheit eine längere Aufenhaltsdauer im Gasraum.
      Sie erzeugen vorübergehend einen Ionenschlauch, das heißt eine radialsymmetrische positive Raumladung, die vorübergehend die Feldstärke in Drahtnähe für eine Zeit $T$ soweit absenkt, dass Stoßionisation verhindert wird.
      In dieser Totzeit werden keine einfallenden Teilchen registriert.
      Registriert das Zählrohr $N_\t{r}$ Impulse pro Zeiteinheit, so ist das Zählrohr für den Bruchteil $TN_\t{r}$ unempfindlich und ensprechend für den Rest $1 - TN_\t{r}$ messbereit.
      Die wahre Impulsrate $N_\t{w}$ beträgt daher
      \begin{eqn}
        N_\t{w} = \frac{N_\t{r}}{1 - TN_\t{r}} . \eqlabel{eqn:Totzeit}
      \end{eqn}

      Nach Abwandern der Ionen in Richtung Mantel sind Lawinen zwar wieder möglich, allerdings erreichen die Ladungsinpulse erst wieder die alte Höhe, wenn die Ionen vor einer erneuten Zündung vollständig neutralisiert wurden und das radialsymmetrische Anfangsfeld herrscht. Dieser Zeitraum $T_\t{E}$ wird Erholungszeit genannt.
      In Abbildung \ref{fig:erholungszeit} sind die Zeitintervalle wiederum veranschaulicht.
      Wenn die Ionen auf den Mantel auftreffen, so können sie dort Elektronen aus der Metalloberfläche herauslösen, da bei Neutralisation der Ionen die frei werdende Energie die Austrittsarbeit übersteigt.
      Die so erzeugten Sekundärelektronen können das gesamte Zählrohr durchlaufen, und erneut die Zählrohrentladung hervorrufen, sodass ein einziges Teilchen mehrere zeitlich versetze, zusätzliche Ladungsimpulse hervorruft.
      Hierbei handelt es sich um so genannte Nachentladungen.
      Da der zeitliche Abstand der Ladungsimpulse jedoch der Laufzeit $T_\t{L}$ der Ionen entspricht und weiterhin gilt, dass $T_\t{L} > T$, täuschen sie eintreffende ionisierende Teilchen vor; die Nachentladungen sind daher unerwünscht.
      Ihnen wird durch Zusatz von Alkoholmolekülen im Zählrohrgasgemisch Rechnung getragen.
      Die Ionen stoßen auf ihrem Weg zum Mantel mit den Alkoholmolekülen zusammen und ionisieren diese, da die Ionisierungsenergie der Alkoholmoleküle kleiner als die der Edelgasionen ist; letztere werden bei diesem Vorgang neutralisiert.
      Die Alkoholionen setzen den Weg der Edelgasionen zum Mantel fort und erzeugen bei Neutralisierung Energie, die nun die Moleküle in Schwingung versetzt, anstatt Elektronen herauszuschlagen.
      Die Entladung erlischt damit und wird erst wieder bei einfallenden Teilchen ausgelöst.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/erholungszeit.pdf}
        \caption{Visuelle Definition der Totzeit und Erholungszeit \cite{anleitung703}}
        \label{fig:erholungszeit}
      \end{figure}
    \subsection{Charakteristik eines Zählrohrs}
      Wird die Zählrate $N$ bei konstanter Strahlungsintensität gegen die angelegte Spannung $U$ aufgetragen, so ergibt sich eine so genannte Charakteristik.
      Sie entspricht in etwa Abbildung \ref{fig:charakteristik}.
      Im Bereich der Spannung $U_\t{E}$ setzt der Auslösebereich ein, rechts daneben befindet sich das Plateau; ein linearer Anteil der Kurve.
      Im Idealfall besitzt das Plateau keine Steigung.
      In der Realität tritt immer eine geringe Zunahme von $N$ mit wachsender Spannung $U$ auf, da die Nachentladungen nur minimiert werden können.
      Am Ende des Plateaus steigt die Kurve stark an, da nun der Bereich der selbständigen Gasentladung beginnt.
      Hier treten durch ein einziges ionisierendes Teilchen Dauerentladungen auf, die in Folge der hohen Stromdichten das Zählrohr schnell zerstören.
      \begin{figure}
        \includegraphics{graphic/charakteristik.pdf}
        \caption{Beispielcharakteristik eines Zählrohrs \cite{anleitung703}}
        \label{fig:charakteristik}
      \end{figure}
  \section{Aufbau}
    Eine Spannungsquelle erzeugt ein Potentialgefälle zwischen Anode und Kathode des zu untersuchenden Geiger-Müller-Zählrohrs.
    Eine Schaltung dient zur Zählung der Impulse.
    Sammelt sich nun auf dem Draht des Zählrohrs Ladung an, so fließt sie über einen Widerstand $R$ ab und erzeugt einen Spannungsimpuls, der, nachdem er über einen Kondensator $C$ ausgekoppelt wurde, verstärkt wird und anschließend in einem Zählgerät registriert wird.
  \section{Durchführung}
    \subsection{Aufnahme der Charakteristik}
      Für die Charakteristik wird eine $\particle{\beta}$-Quelle vor das Zählrohr gestellt und die Zählrate in Abhängigkeit von der Betriebsspannung ermittelt.
      Da Totzeit\-/Korrekturen vermieden werden sollen, darf die Impulsrate $\SI{100}{\per\second}$ nicht übersteigen.
      Des Weiteren soll die Messauflösung im Bereich des Plateaus in Folge der geringen Steigung sehr hoch gewählt werden.
      Die Messzeit wird so gewählt, dass der relative statistische Fehler jedes Messpunktes kleiner als $\SI{1}{\percent}$ ist.
      Um das hier verwendete Zählrohr nicht zu zerstören, darf die Betriebsspannung $\SI{700}{\volt}$ nicht übersteigen.
    \subsection{Sichtbarmachung von Nachentladungen}
      Hierbei sollen qualitativ Nachentladungen mit dem Oszilloskop visualisiert werden.
      Dafür wird die $\particle{\beta}$-Quelle soweit abgeschirmt, dass während der Laufzeit des Kathodenstrahls von links nach rechts fast kein weiterer aus einem $\particle{\beta}$-Teilchen stammender Impuls hervorgerufen wird.
      Die Skalierung der Zeitachse am Oszilloskop soll auf $\SI{50}{\micro\second}$ pro Einteilung eingestellt werden.
      Die Rohrspannung befindet sich zunächst in einem für Nachentladungen unwahrscheinlichen Bereich von $\SI{350}{\volt}$ und wird nach allen Vorbereitungen auf einen Bereich von \SIrange{700}{710}{\volt} erhöht.
      Beobachtungen sollen ausformuliert und der zeitliche Abstand zwischen Primär- und Nachentladungsimpuls ausgemessen werden.
    \subsection{Oszillographische Messung der Totzeit}
      Nun soll eine hohe Strahlintensität in das Zählrohr eindringen.
      Des Weiteren soll die Zeitablenkung des Oszillographen durch die Anstiegsflanke des Zählrohrimpulses getriggert werden.
      Aus dem auftretenden Oszillogramm soll die Totzeit, also die Zeit bis wieder größer werdende Ladungsimpulse sichtbar werden, bei bekannter Ablenkzeit abgelesen werden.
      Zusätzlich wird die Erholungszeit grob abgeschätzt.
    \subsection{Messung der Totzeit mit der Zwei-Quellen-Methode}
      Unter Verwendung von Formel \eqref{eqn:Totzeit} soll die Totzeit bestimmt werden.
      Zunächst wird die Zählrate $N_1$ des ersten Präparats im Zählrohr bestimmt.
      Daraufhin wird Präparat 2 hinzugenommen und die Summenzählrate $N_{1+2}$ bestimmt, ohne die Lage des ersten Präparates relativ zum Zählrohr zu verändern.
      Danach wird das erste Präparat entfernt und die Zählrate $N_2$ des zweiten registriert.
      Gäbe es keine Totzeit, so ergäbe die Summe der einzelnen Zählraten die gemeinsame Zählrate.
      In Wirklichkeit beobachtet man aber
      \begin{eqn}
        N_{1+2} < N_1 + N_2.
      \end{eqn}
      Für die wahre Impulsrate laut Formel \eqref{eqn:Totzeit} gilt jedoch die Gleichheit.
      Daraus folgt für die Totzeit
      \begin{eqn}
        T \approx \frac{N_1 + N_2 - N_{1+2}}{2 N_1 N_2} , \eqlabel{eqn:totzeit} \\
      \end{eqn}
      falls $T^2N^2_\t{i} \ll 1$.
    \subsection{Freigesetzte Ladung pro Teilchen}
      Mit einem empfindlichen Zählrohr kann der mittlere Zählrohrstrom
      \begin{eqn}
        \mean{I} \coloneq \frac{1}{\tau} \int_0^\tau \dif{t} \frac{\f{U}(t)}{R} \\
      \end{eqn}
      für $\tau \gg T$ gemessen werden.
      Daraus lässt sich bei bekannter Impulszahl pro Zeiteinheit die pro eindringendes Teilchen freigesetzte Ladung ermitteln.
      Für $Z$ Teilchen ergibt sich also
      \begin{eqn}
        \mean{I} = \frac{\Del{Q}}{\Del{t}} Z.
      \end{eqn}
      $\Del{Q}$ soll hier in Abhängigkeit von der Kondensatorspannung $U$ ermittelt werden.
  \section{Auswertung}
    \input{statistics.tex}
    \input{poisson.tex}
    \input{linregress.tex}
    In Tabelle \ref{tab:zwei-quellen} sind zur Totzeitbestimmung die Messwerte der Zwei-Quellen-Methode aufgeführt.
    Die Indizes stehen intuitiv für die erste, zweite, beziehungsweise beide Quellen.
    Ausgehend von Formel \eqref{eqn:totzeit} errechnet sich die Totzeit $T_\t{2Q}$, die ebenfalls dort aufgeführt ist.
    Für $T_\t{2Q}$ gilt
    \begin{eqns}
      \g(\err{T_\t{2Q}})^2 &=& \g(\frac{1}{2 N_{1} N_{2}} - \frac{N_{1} - N_{1+2} + N_{2}}{2 N_{1}^{2} N_{2}})^{\!\!2} \g(\err{N_{1}})^{2} \\
      &&+ \g(\frac{1}{2 N_{1} N_{2}} - \frac{N_{1} - N_{1+2} + N_{2}}{2 N_{1} N_{2}^{2}})^{\!\!2} \g(\err{N_{2}})^{2} + \frac{\g(\err{N_{1+2}})^{2}}{4 N_{1}^{2} N_{2}^{2}} .
    \end{eqns}
    \begin{table}
      \input{table/T.tex}
      \caption{Messwerte der Zwei-Quellen-Methode, sowie qualitative Bestimmung der Totzeit und der Nachentladungszeit, Literaturwert der Elementarladung \cite{scipy}}
      \label{tab:zwei-quellen}
    \end{table}

    Die Messwerte für die Charakteristik sind in Tabelle \ref{tab:charakteristik-ladung} aufgelistet.
    \begin{table}
      \OverfullCenter{\input{table/charakteristik.tex}}
      \caption{Messwerte zur Ermittlung der Charakteristik, gemessener Strom, sowie registrierte Ladung pro einfallendem Teilchen}
      \label{tab:charakteristik-ladung}
    \end{table}
    Für den Plot (Abbildung \ref{fig:charakteristik_groß}) ist zu beachten, dass die Zählraten auf die Zeit normiert werden müssen, um sie nun als Intensität zu vergleichen.
    Es gilt daher
    \begin{eqn}
      N = \frac{Z}{t} .
    \end{eqn}
    Zur genauen Untersuchung der Abweichung vom idealen Zählrohr sind in Abbildung \ref{fig:charakteristik_klein} die Messpunkte aus dem linearen Plateaubereich vergrößert dargestellt.
    Die Steigung $A$ (und der Achsenabschnitt $B$) werden anhand einer linearen Regression ermittelt, die Regressionsgerade befindet sich ebenfalls in diesem Plot.
    Aus der linearen Regression ergeben sich die in Tabelle \ref{tab:lin_parameter} gelisteten Werte samt Fehlern.
    Die Steigerung des Plateaus $S$ wird durch die Formel
    \begin{eqn}
      S = \frac{N_2 - N_1}{N_1} \frac{\SI{100}{\percent}}{\SI{100}{\volt}} = \frac{A \g(U_2 - U_1)}{N_1} \frac{\SI{100}{\percent}}{\SI{100}{\volt}} = \frac{A \cdot \SI{100}{V}}{A U_1 + B} \frac{\SI{100}{\percent}}{\SI{100}{\volt}}
    \end{eqn}
    gegeben.
    Sie ist für die Stelle $U_1 = \SI{400}{\volt}$ ebenfalls in Tabelle \ref{tab:lin_parameter} angegeben.
    Zur Verdeutlichung der Abhängigkeit der vom Zählrohr ausgehenden Ladung $\Del{Q}$ in Abhängigkeit von der angelegten Spannung $U$ sind die Werte in Abbildung \ref{fig:Q_U} geplottet.
    \begin{table}
      \input{table/linregress.tex}
      \caption{Aus der linearen Regression erhaltene Parameter}
      \label{tab:lin_parameter}
    \end{table}
    \begin{figure}
      \includegraphics[width=\textwidth]{graph/charakteristik.pdf}
      \caption{Gemessene Charakteristik des Geiger-Müller-Zählrohrs}
      \label{fig:charakteristik_groß}
    \end{figure}
    \begin{figure}
      \includegraphics[width=\textwidth]{graph/linear.pdf}
      \caption{Plateaubereich der Charakteristik}
      \label{fig:charakteristik_klein}
    \end{figure}
    \begin{figure}
      \includegraphics[width=\textwidth]{graph/Q_U.pdf}
      \caption{$\Del{Q}$ gegen $U$ aufgetragen}
      \label{fig:Q_U}
    \end{figure}
  \section{Diskussion}
    Die Messzeit zur Aufnahme der Charakteristik wurde so gewählt, dass der relative Fehler der Intensität kleiner als $\SI{1}{\percent}$ ist.
    Das gemessene Plateau erstreckt sich über ein Intervall der Länge $\SI{400}{\volt}$. 
    Wie in Tabelle \ref{tab:charakteristik-ladung} ersichtlich, wurde das Kriterium für die hohen Spannungen größtenteils erfüllt.
    Bei den statistischen Schwankungen im Plateaubereich fällt auf, dass sie sich für den probabilistischen Vorgang des radioaktiven Zefalls in Grenzen halten.
    
    Obwohl sich die mit der Zwei-Quellen-Methode bestimmte Totzeit mitsamt Fehler mit der qualitativen Oszillographenmessung deckt, konnte auf dem Hintergrund der großen statistischen Schwankungen auch mit der Zwei-Quellen-Methode die Totzeit nicht genau bestimmt werden.
    Da die Abweichung $S$ von einem idealen Zählrohr klein ist, handelt es sich hierbei um ein gutes Zählrohr.

    Bei der Betrachtung der Abhängigkeit zwischen den ankommenden Ladungen und der angelegten Spannung fällt auf, dass innerhalb des Messbereichs für kleine Spannungen die Anzahl der erzeugten Ladungen gleich bleibt und erst ab Spannungen über $\SI{400}{\volt}$ ansteigt.
    Das Anstiegsverhalten deutet auf den Charakter einer e-Funktion hin.
  \makebibliography
\end{document}
