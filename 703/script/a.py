from labtools import *

t, U, Z, I = np.loadtxt(lt.file('data/charakteristik'), unpack=True)
I *= 1e-6
Z = unc.unumpy.uarray((Z, np.sqrt(Z)))

N = Z / t

A, B = lt.linregress(U[6:], N[6:])

@numpy.vectorize
def calc_Delta_Q(I, t, Z):
    if Z == 0:
        return unc.ufloat((np.nan, 0))
    return I * t / Z

Delta_Q = calc_Delta_Q(I, t, Z) / sp.constants.elementary_charge

tab = lt.SymbolColumnTable()
tab.add_si_column('t', t, r'\second')
tab.add_si_column('U', U, r'\volt')
tab.add_si_column('Z', lt.vals(Z), places=0)
tab.add_si_column('I', I * 1e6, r'\micro\ampere', places=1)
tab.add_si_column('N', N, r'\per\second')
tab.add_si_column(r'\rerr{N}', lt.stds(N) / lt.vals(N) * 100, r'\percent', places=1)
tab.add_si_column(r'\Del{Q}', Delta_Q * 1e-9, r'$e$', exp="e9")
tab.savetable(lt.new('table/charakteristik.tex'))

tab = lt.SymbolRowTable()
tab.add_si_row('A', A, r'\per\volt\per\second')
tab.add_si_row('B', B, r'\per\second')
tab.add_si_row('S', A * 100 * 100 / (A * 400 + B), r'\percent\per{\SI{100}{\volt}}')
tab.savetable(lt.new('table/linregress.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(U, lt.vals(N), 'rx', label='Messwerte')
ax.xaxis.set_view_interval(280, 720)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
ax.set_xlabel(r'\silabel{U}{\volt}')
ax.set_ylabel(r'\silabel{N}{\per\second}')
ax.legend(loc='lower right')
fig.tight_layout()
fig.savefig(lt.new('graph/charakteristik.tex'), bbox_inches=0, pad_inches=0)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(300, 720, 10000)
ax.plot(x, lt.vals(A * x + B), 'b-', label='Regressionsgerade')
ax.errorbar(U[6:], lt.vals(N[6:]), lt.stds(N[6:]), fmt='rx', label='Messwerte')
ax.xaxis.set_view_interval(300, 720, ignore=True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
ax.set_xlabel(r'\silabel{U}{\volt}')
ax.set_ylabel(r'\silabel{N}{\per\second}')
lt.ax_reverse_legend(ax, loc='lower right')
fig.tight_layout()
fig.savefig(lt.new('graph/linear.tex'), bbox_inches=0, pad_inches=0)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.errorbar(U[11:], lt.vals(Delta_Q[11:]) * 1e-9, lt.stds(Delta_Q[11:]) * 1e-9, fmt='rx', label='Messwerte')
ax.xaxis.set_view_interval(300, 720, ignore=True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
ax.set_xlabel(r'\silabel{U}{\volt}')
ax.set_ylabel(r'\silabel{\Del{Q}}[e9]{e}')
ax.legend(loc='upper left')
fig.tight_layout()
fig.savefig(lt.new('graph/Q_U.tex'), bbox_inches=0, pad_inches=0)
