from labtools import *

t_nachentladung = np.loadtxt(lt.file('data/nachentladungen'))

T_oszi = np.loadtxt(lt.file('data/totzeit_oszi')) * 1e-6

Z_1, Z_2, Z_12, t = np.loadtxt(lt.file('data/zwei_quellen'), unpack=True)
Z_1 = unc.ufloat((Z_1, np.sqrt(Z_1)))
Z_2 = unc.ufloat((Z_2, np.sqrt(Z_2)))
Z_12 = unc.ufloat((Z_12, np.sqrt(Z_12)))

N_1 = Z_1 / t
N_2 = Z_2 / t
N_12 = Z_12 / t

T = (N_1 + N_2 - N_12) / (2 * N_1 * N_2)

tab = lt.SymbolRowTable()
tab.add_si_row('t', t, r'\second')
tab.add_si_row('Z_1', lt.vals(Z_1), places=0)
tab.add_si_row('Z_2', lt.vals(Z_2), places=0)
tab.add_si_row('Z_{1+2}', lt.vals(Z_12), places=0)
tab.add_hrule()
tab.add_si_row(r'T_\t{2Q}', T * 1e6, r"\micro\second")
tab.add_si_row(r'T_\t{Oszi}', T_oszi * 1e6, r"\micro\second")
tab.add_si_row(r't_\t{Nachentladung}', t_nachentladung, r'\milli\second', places=1)
tab.add_hrule()
tab.add_si_row('e', sp.constants.elementary_charge * 1e19, r'\coulomb', exp='e-19', places=2)
tab.savetable(lt.new('table/T.tex'))
