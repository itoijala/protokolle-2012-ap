\input{header/report.tex}

\addbibresource{lit.bib}

\SetExperimentNumber{504}

\begin{document}
  \maketitlepage{Thermische Elektronenemission}{05.06.2012}{12.06.2012}
  \section{Ziel}
    In dem Versuch wird die Austrittsarbeit von Elektronen aus Wolfram und der Exponent des Langmuir-Schottkyschen Raumladungsgesetzes durch das Untersuchen der thermischen Emission von Elektronen bestimmt.
  \section{Theorie}
    Durch Zufuhr von Wärme in ein Metall können Elektronen dazu gebracht werden, das Metall zu verlassen, dies wird thermische Elektronenemission genannt.
    Zur Untersuchung dieses Effektes wird eine Hochvakuumdiode verwendet.
    \subsection{Energieverteilung von Elektronen und Austrittsarbeit}
      Ein Metall ist im kondensierten Zustand in der Regel durch eine kristalline Gitterstruktur gekennzeichnet, die räumlich periodisch angeordnet ist.
      Die Elektronen sind nicht an ein Atom gebunden und bilden ein frei bewegliches Elektronengas.
      Die Potentiale innerhalb und außerhalb des Metalls werden als jeweils konstant genähert.
      Das Potential innerhalb des Metalls ist kleiner als das außerhalb, die Differenz wird als $\phi$ bezeichnet.
      Ein gutes Modell stellt hier ein Potentialtopf mit einem symmetrischen Potential $\phi$ dar; innerhalb dieses Topfs sind die Elektronen frei verschiebbar; aus diesem Grund ist eine hohe Leitfähigkeit für Metalle charakteristisch.
      Die Arbeit, die ein Elektron verrichten muss, um die Potentialschwelle zu überwinden, wird als Austrittsarbeit
      \begin{eqn}
        W_\t{A} = e \phi
      \end{eqn}
      bezeichnet, wobei $e$ die Elementarladung ist.

      Für Elektronen gilt das Pauli-Verbot, also kann jeweils nur ein Elektron eines Systems einen quantenmechanischen Zustand besetzen.
      Aufgrund des Spins folgt, dass nur zwei Elektronen dieselbe Energie haben können.
      Die Wahrscheinlichkeit, dass bei thermischen Gleichgewicht ein Zustand mit der Energie $E$ besetzt ist, wird durch die Fermi-Diracsche-Verteilungsfunktion
      \begin{eqn}
        \f{f}(E) = \frac{1}{1 + \exp.\frac{E - E_\t{F}}{k T}.}
      \end{eqn}
      beschrieben, wobei $k$ die Boltzmann-Konstante und $E_\t{F}$ die Fermi-Energie ist, also die maximal mögliche Energie eines Elektrons bei der Temperatur $T = 0$.
      Der qualitative Verlauf dieser Verteilungsfunktion ist in Abbildung \ref{fig:fermi-dirac} gegeben.
      \begin{figure}
        \includegraphics{graphic/fermi-dirac.pdf}
        \caption{Qualitativer Verlauf der Fermi-Diracschen-Verteilungsfunktion \cite{anleitung504}}
        \label{fig:fermi-dirac}
      \end{figure}

      Bei deren Betrachtung wird deutlich, dass ein Elektron die Energiebeziehung
      \begin{eqn}
        E \ge E_\t{F} + e \phi \eqlabel{eqn:austrittsbedingung}
      \end{eqn}
      erfüllen muss, um das Metall zu verlassen.
      Dieser Wert ist für die austretenden Elektronen selbst beim Schmelzpunkt von Wolfram noch viel größer als $k T$, daher reicht die gute Näherung
      \begin{eqn}
        \f{f}(E) = \exp(\frac{E_\t{F} - E}{k T}) . \eqlabel{eqn:fermi_dirac}
      \end{eqn}
    \subsection{Sättigungsstromdichte}
      Ausgehend von \eqref{eqn:fermi_dirac} wird die Sättigungsstromdichte $\j_\t{S}$ als die Ladung, die pro Zeit- und Flächeneinheit aus einer Metalloberfläche austreten, in Abhängigkeit von der Temperatur definiert.
      Wird ein kartesisches Koordinatensystem verwendet, dessen $z$-Achse senkrecht auf der Metalloberfläche steht, so gilt für die Zahl $\dif{\alpha}$ der Elektronen in einem Impulsraum-Volumenelement $\dif{^3 p}$, die pro Zeit- und Flächeneinheit von innen an die Grenzfläche des Metalls stoßen
      \begin{eqn}
        \dif{\alpha} = v_z \f{n}(E) \dif{^3 p} .
      \end{eqn}
      Dabei ist $\f{n}(E)$ die Phasenraumdichte und $v_z$ die Geschwindigkeitskomponente entlang der Flächennormalen.
      Wegen
      \begin{eqns}
        E &=& \frac{\v{p}^2}{2m} \\
          &=& \frac{1}{2} m \v{v}^2
      \end{eqns}
      folgt
      \begin{eqns}
        \dif{\alpha} &=& \p{p_z}{E}; \f{n}(E) \dif{p_x} \dif{p_y} \dif{p_z} \\
                &=& \f{n}(E) \dif{E} \dif{p_x} \dif{p_y} ,
      \end{eqns}
      wobei $m$ die Elektronenmasse ist.
      Jeder Quantenzustand nimmt im sechsdimensionalen Phasenraum das Volumen $h$ ein, wobei $h$ das Plancksche Wirkungsquantum ist; es gilt daher
      \begin{eqn}
        \f{n}(E) = \frac{2}{h^3} \f{f}(E) ;
      \end{eqn}
      der Faktor $2$ kommt aus den zwei Möglichkeiten des Spins.
      Es gilt also
      \begin{eqn}
        \dif{\alpha} = \frac{2}{h^3} \exp(\frac{E_\t{F} - E}{k T}) \dif{p} \dif{E} .
      \end{eqn}
      Damit die Elektronen aus dem Metall austreten können, muss \eqref{eqn:austrittsbedingung} für $E = E_{\t{kin}, z}$ erfüllt sein.
      Das Produkt aus der Anzahl der Elektronen, die dieses Kriterium erfüllen, und der Elementarladung ist die Sättigungsstromdichte, die sich zu
      \begin{eqn}
        \f{\j_\t{S}}(T) = 4 \PI \frac{e m k}{h^3} T^2 \exp(- \frac{e \phi}{k T}) \eqlabel{eqn:richardson}
      \end{eqn}
      ergibt.
      Das Endergebnis \eqref{eqn:richardson} wird auch als Richardson-Gleichung bezeichnet.
    \subsection{Hochvakuumdiode}
      Die Messung des Sättigungsstroms wird durch eine Hochvakuumdiode realisiert.
      Das gute Vakuum ist notwendig, damit die Elektronen nicht mit Luftmolekülen wechselwirken.
      Die Diode erzeugt ein elektrisches Feld zwischen Anode und Glühkathode, dass die austretenden Elektronen absaugt.
      An der Glühkathode liegt eine Heizspannung an, die einen Strom durch den Wolframdraht fließen lässt, der das Wolfram auf \SIrange{1000}{3000}{\kelvin} erhitzt.
      Die Emission der Anode ist deutlich geringer als die der Kathode.
    \subsection{Die Langmuir-Schottkysche Raumladungsgleichung}
      Bei gegebener Kathodentemperatur ist der Anodenstrom von der Anodenspannung abhängig.
      Ist die Spannung zu niedrig, so erreichen nicht alle Elektronen die Anode.
      Dies geschieht erst bei hinreichend hoher Anodenspannung; der Strom ist nun von der Spannung unabhängig.

      Eine Diode verletzt das Ohmsche Gesetz, da die Elektronen eine Beschleunigung in Richtung der Anode erfahren, insbesondere tritt eine Inhomogenität der Ladungsdichte auf.
      Die Raumladungsdichte nimmt zur Anode hin ab, da sich im Bereich der Glühkathode die meisten Elektronen befinden.
      Auf Grund der Kontinuitätsbedingung ist die Stromdichte an jeder Stelle konstant.
      Wegen
      \begin{eqn}
        \v{\j} = \rho \v{v} \eqlabel{eqn:prop}
      \end{eqn}
      folgt, dass für kleine Geschwindigkeiten $\v{v}$ der Elektronen die Raumladungsdichte $\rho$ groß sein muss, dies ist aber vor allem im Bereich der Glühkathode der Fall.
      Sie ist so groß, dass sie das Feld der angelegten Spannung abschirmt.
      Daraus folgt also, dass der Diodenstrom kleiner als der erwartete Sättigungsstrom \eqref{eqn:richardson} ist.

      Für den quantitativen Zusammenhang wird von der Poisson-Gleichung
      \begin{eqn}
        \d{x}[2]{U}; = \frac{\f{\rho}(x)}{\epsilon_0} , \eqlabel{eqn:poisson}
      \end{eqn}
      wobei $U$ das elektrische Potential und $\epsilon_0$ die elektrische Feldkonstante ist, ausgegangen unter den vereinfachenden Annahmen, dass Anode und Kathode Oberflächen unendlicher Ausdehnung mit dem Abstand $a$ sind; damit hängen $v$ und $\rho$ nur von der Ortskoordinate $x$ ab.
      Mit Hilfe von \eqref{eqn:prop} und der Äquivalenz von elektrischer und kinetischer Energie lässt sich \eqref{eqn:poisson} integrieren.
      Dabei ergibt sich für das Potential
      \begin{eqn}
        \f{U}(x) = \g(3 \sqrt{\frac{\j}{4 \epsilon_0 \sqrt{\frac{2 e}{m}}}} x )^{\!\! \frac{4}{3}} .
      \end{eqn}
      Außerdem gilt für das elektrische Feld und die Ladungsdichte
      \begin{eqns}
        \f{E}(x) &\propto& x^{\frac{1}{3}} \\
        \f{\rho}(x) &\propto& x^{- \frac{2}{3}} .
      \end{eqns}
      Für die Stromdichte gilt schließlich
      \begin{eqn}
        \j = \frac{4}{9} \epsilon_0 \sqrt{\frac{2e}{m}} \frac{1}{a^2} U^{\frac{3}{2}} \eqlabel{eqn:langmuir}
      \end{eqn}
      Dieses Gesetz, das die korrekte Proportionalität zwischen Stromdichte und Potential angibt, wird das Langmuir\-/Schottkysche Raumladungsgesetz genannt.
      Der Gültigkeitsbereich von \eqref{eqn:langmuir} im $\j$-$U$-Diagramm einer Hochvakuumdiode nennt man das Raumladungsgebiet.
    \subsection{Anlaufstromgebiet einer Hochvakuumdiode}
      Nach \eqref{eqn:langmuir} sollte für $U = 0$ die Stromdichte verschwinden, experimentell ergibt sich jedoch ein geringer Anodenstrom.
      Für $T > 0$ gibt es laut \eqref{eqn:fermi_dirac} endlich viele Elektronen, deren Energie größer als die Austrittsarbeit ist.
      Dadurch erhalten die Elektronen eine Restenergie
      \begin{eqn}
        \Del{E} = E - \g(E_\t{F} + e \phi) ,
      \end{eqn}
      sodass sie gegen ein Feld anlaufen können; dieses Ensemble von Ladungsträgern wird daher als Anlaufstrom bezeichnet.
      Das Anodenmaterial besitzt meistens eine höhere Austrittsarbeit als die Kathode; durch die externe leitende Verbindung werden die Fermi-Oberflächen $E = E_\t{F}$ auf das selbe Niveau gebracht.
      Bei einem Potential $U$ entsteht eine Potentialdifferenz von $e U$; um die Anode zu erreichen, muss $E \ge e \phi_\t{A} + e U$ sein, wobei $e \phi_\t{A}$ die Austrittsarbeit der Anode ist.
      Die Zahl der Leitungselektronen zwischen $E$ und $E + \dif{E}$ hängt exponentiell von $E$ ab, somit hängt auch die Anlaufstromstärke vom äußeren Potential ab:
      \begin{eqn}
        \f{\j}(U) = \j_0 \exp(- \frac{e \phi_\t{A} + e U}{k T}) \propto \exp(- \frac{e U}{k T}) .
      \end{eqn}
    \subsection{Kennlinie einer Hochvakuumdiode}
      Als Kennlinie wird der Zusammenhang zwischen Anodenstrom $I$ und der von außen angelegten Spannung $U$ definiert.
      Wie in Abbildung \ref{fig:kennlinie} gliedert sich die Kennlinie in die Bereiche Anlaufstrom-, Raumladungs- und Sättigungsstromgebiet.
      \begin{figure}
        \includegraphics{graphic/kennlinie.pdf}
        \caption{Beispiel einer Kennlinie \cite{anleitung504}}
        \label{fig:kennlinie}
      \end{figure}
      Im ersten Bereich gibt es einen exponentiellen Zusammenhang zwischen $I$ und $U$.
      Im anschließenden Raumladungsgebiet gibt es eine $\sqrt{U^3}$-Abhängigkeit.
      Gleichung \eqref{eqn:langmuir} kann nicht für beliebig hohe Spannungen gültig sein, da die Zahl der pro Zeiteinheit emittierten Elektronen gemäß \eqref{eqn:richardson} nur von der Temperatur, aber nicht von der Anodenspannung abhängt.
      Der Strom strebt einem Sättigungsstrom $I_\t{S}$ zu, was den kontinuierlichen Übergang in das Sättigungsstromgebiet kennzeichnet.
  \section{Aufbau und Durchführung}
    \subsection{Aufnahme der Kennlinie}
      Zur Aufnahme der Kennlinien wird die Schaltung aus Abbildung \ref{fig:exp_kennlinie} verwendet.
      Der Heizstrom im Bereich von \SIrange{2.5}{3.1}{\ampere} wird mit einem Konstantspannungsgerät erzeugt; der Heizstrom $I_\t{F}$ wird an der integrierten Anzeige abgelesen.
      Für die Bestimmung der Heizspannung $U_\t{F}$ wird der ebenfalls integrierte Voltmeter vewendet, das aber nur eine grobe Auflösung bietet.
      Die Anodenspannung $U$, die mit einem zweiten Konstantspannungsgerät erzeugt wird, entnimmt man einem eingebauten Messgerät.
      Der Anodenstrom $I$ kann ebenfalls dort abgelesen werden; er ist sicherheitshalber auf $\SI{2}{\milli\ampere}$ begrenzt, bei Überlast schaltet sich das Netzgerät ab.
      Vor das Netzgerät wird ein Widerstand von $\SI{100}{\ohm}$ geschaltet, sodass durch die abfallende Spannung Rückschlüsse auf den Anodenstrom gezogen werden können.
      Anstatt der Kurve des $x$-$y$-Schreibers werden 15 Messwerte der Anodenspannung und des Anodenstroms abgelesen.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/exp_kennlinie.pdf}
        \caption{Messaufbau zu Bestimmung der Kennlinie \cite{anleitung504}}
        \label{fig:exp_kennlinie}
      \end{figure}
      \FloatBarrier
    \subsection{Ermittlung der Anlaufstromkurve}
      Hierzu wird die Schaltung aus Abbildung \ref{fig:anlaufstrom} benutzt.
      Um den Anlaufstrom $I$ von der Größenordnung \SI{1}{\nano\ampere} zu messen wird ein empfindliches Nanoamperemeter mit eingebautem Verstärker benutzt.
      Wegen der geringen Ströme wird ein möglichst kurzes Kabel zwischen Anode und Eingangssbuchse HI verwendet.
      Durch den starken Heizstrom von \SI{3}{\ampere} gibt es beim Übergangswiderstand zwischen Bananenstecker und Eingangsbuchse einen Spannungsabfall, der schwankt und die Anodenspannung von maximal $\SI{1}{\volt}$ stark beeinflusst.
      Auf Grund der exponentiellen Spannungsabhängigkeit können beträchtliche Schwankungen beim Anodenstrom entstehen; dieser Effekt ist durch mehrmaliges Drehen des Bananensteckers zu minimieren.
      Durch den Innenwiderstand $R_\t{i}$ des Amperemeters von \SI{1}{\mega\ohm} liegt zwischen Anode und Kathode eine andere Spannung $V$ an, als die vom Konstantspannungsgerät angezeigte Spannung $U$.
      Die Heizspannung muss auf den Maximalwert gedreht werden, damit ein Strom gemessen werden kann; der Strom wird in Abhängigkeit von der angelegten Diodenspannung abgelesen.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/anlaufstrom.pdf}
        \caption{Schaltung zu Ermittlung des Anlaufstroms \cite{anleitung504}}
        \label{fig:anlaufstrom}
      \end{figure}
  \section{Messwerte}
    In Tabelle \ref{tab:constants} stehen die verwendeten Literaturwerte von Konstanten und bekannte Werte der Apparatur.
    Die Messwerte der fünf aufgenommenen Kennlinien stehen in den Tabellen \ref{tab:I_F} und \ref{tab:I}.
    Dabei ist $I_\t{F}$ der eingestellte Heizstrom und $U_\t{F}$ die gemessene Heizspannung.
    In Tabelle \ref{tab:I} ist $U$ für die ersten vier Kennlinien und $U_5$ für die fünfte.
    Die Messwerte der Messung des Anlaufstroms stehen in Tabelle \ref{tab:d}.
    \begin{table}
      \input{table/constants.tex}
      \caption{Verwendete Konstanten \cite{scipy} und Apparaturangaben}
      \label{tab:constants}
    \end{table}
    \begin{table}
      \input{table/I_F.tex}
      \caption{Heizströme und -Spannungen}
      \label{tab:I_F}
    \end{table}
    \begin{table}
      \input{table/I.tex}
      \caption{Messwerte für die Kennlinien}
      \label{tab:I}
    \end{table}
    \begin{table}
      \input{table/d.tex}
      \caption{Messwerte des Anlaufstroms und der angelegten Diodenspannung}
      \label{tab:d}
    \end{table}
  \section{Auswertung}
    \input{statistics.tex}
    \input{linregress.tex}
    \subsection{Kennlinienschar}
      In Abbildung \ref{fig:I_S} sind die Kennlinien für verschiedene Heizströme aufgetragen, die abgelesenen Sättigungsströme $I_\t{S}$ für die ersten vier Kennlinen stehen in Tabelle \ref{tab:I_S}.
      Eine Angabe über den Sättigungsstrom der fünften Kennlinie ist nicht möglich, da der Sättigungsbereich nicht im Messbereich enthalten ist.
      \begin{table}
        \input{table/I_S.tex}
        \caption{Abgelesene Sättigungsströme aus Abbildung \ref{fig:I_S}}
        \label{tab:I_S}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/I_S.pdf}
        \caption{Kennlinienschar der Hochvakuumdiode für verschiedene Heizströme}
        \label{fig:I_S}
      \end{figure}
      \FloatBarrier
    \subsection{Exponent des Langmuir\texorpdfstring{\-/}{-}Schottkyschen Gesetzes}
      Laut dem Langmuir-Schottkyschen Gesetz \eqref{eqn:langmuir} gilt für den Strom
      \begin{eqn}
        I_5 \propto U_5^{\frac{3}{2}} .
      \end{eqn}
      Um die Aussage der Theorie über den Exponenten zu überprüfen, wird die lineare Regression
      \begin{eqn}
        \ln.\frac{I_5}{\si{\ampere}}. = A \ln.\frac{U_5}{\si{\volt}}. + B
      \end{eqn}
      durchgeführt.
      Dabei ist $A$ der gesuchte Exponent.
      Die Ergebnisse der linearen Regression stehen in Tabelle \ref{tab:I_5} und die Messwerte sowie die Regressionsgerade sind in Abbildung \ref{fig:I_5} aufgetragen.
      \begin{table}
        \input{table/I_5.tex}
        \caption{Ergebnisse der linearen Regression zur Bestimmung des Exponenten des Langmuir-Schottkyschen Gesetzes}
        \label{tab:I_5}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/I_5.pdf}
        \caption{Messwerte und Regressiongerade zur Bestimmung des Exponenten}
        \label{fig:I_5}
      \end{figure}
      \FloatBarrier
    \subsection{Anlaufstrom}
      Der Innenwiderstand $R_\t{i}$ des Nanoamperemeters muss berücksichtigt werden, indem anstelle von $U$ mit
      \begin{eqn}
        V = U + R_\t{i} I
      \end{eqn}
      gerechnet wird.
      Für den Strom im Anlaufgebiet gilt
      \begin{eqn}
        I \propto \exp(- \frac{e V}{k T}) .
      \end{eqn}
      Um die Temperatur $T$ der Kathode zu bestimmen wird die lineare Regression
      \begin{eqn}
        \ln.\frac{I}{\si{\ampere}}. = C V + D
      \end{eqn}
      durchgeführt.
      Die Temperatur ergibt sich dann als
      \begin{eqn}
        T = - \frac{e}{k C} .
      \end{eqn}
      Die Ergebnisse der linearen Regression und der berechnete Wert von $T$ stehen in Tabelle \ref{tab:T_5}.
      Die Meswerte und die Regressionsgerade sind in Abbildung \ref{fig:T_5} aufgetragen.
      \begin{table}
        \input{table/T_5.tex}
        \caption{Ergebnisse der linearen Regression und der berechnete Wert von $T$}
        \label{tab:T_5}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/T_5.pdf}
        \caption{Messwerte und Regressionsgerade zur Bestimung von $T$}
        \label{fig:T_5}
      \end{figure}
      \FloatBarrier
    \subsection{Leistungsbilanz des Heizstromkreises}
      Aus dem Stefan-Boltzmannschen Gesetz für die Strahlungsleistung eines schwarzen Körpers der Temperatur $T$
      \begin{eqn}
        P_\t{Str} = f \eta \sigma T^4 ,
      \end{eqn}
      wobei $f$ Oberfläche des Körpers, $\eta$ der Emissionsgrad der Oberfläche und $\sigma$ die Stefan-Boltzmannsche Strahlungskonstante ist, folgt
      \begin{eqn}
        I_\t{F} U_\t{F} = f \eta \sigma T^4 + P_\t{WL} ,
      \end{eqn}
      wobei $P_\t{WL}$ die Wärmeleitung der Apparatur ist.
      Dies lässt sich zu
      \begin{eqn}
        \sqrt[4]{\frac{I_\t{F} U_\t{F} - P_\t{WL}}{f \eta \sigma}}
      \end{eqn}
      umformen, um die Kathodentemperatur zu berechnen.
      Aus der Richardson\-/Gleichung \eqref{eqn:richardson} folgt für die Austrittsarbeit
      \begin{eqn}
        W_\t{A} = e \phi = - k T \ln(\frac{h^3}{4 \PI e m k^2} \frac{I_\t{S}}{f} \frac{1}{T^2}) .
      \end{eqn}
      \begin{table}
        \input{table/T.tex}
        \caption{Berechnete Werte der Kathodentemperatur und der Austrittsarbeit}
        \label{tab:T}
      \end{table}
  \section{Diskussion}
    Die Analyse der Kennlinienschar deckt sich mit den Erwartungen, dass die Anzahl der emittierten Elektronen von der Heizspannung abhängt und mit steigender Temperatur mehr Elektronen emittiert werden.

    Der Exponent zur Bestätigung der Langmuir-Schottkyschen Gesetzes liegt etwas unter dem Theoriewert.
    Der statistische Fehler ist klein, der Fehler kommt also daher, dass Elektronen für kleine Spannungen verloren gehen und zum anderen aus der Näherung, dass die Platten der Kathode und Anode unendlich groß sind.
    In der linearen Regression wurden zwei ausreißende Messwerte nicht berücksichtigt.

    Die lineare Regression wies einen geringen statistischen Fehler auf, nachdem einige Messwerte herausgeholt wurden, da die Messung mit dem Nanoamperemeter störanfällig war.
    Die mit den zwei Verfahren bestimmten Temperaturen stimmen ungefähr überein, wobei der Fehler des Sättigungsstromverfahrens größer als der des Anlaufstroms einzuschätzen ist.
    Die berechnete Austrittsarbeit stimmt ungefähr mit dem Literaturwert von \SI{4.55}{\electronvolt} \cite{npl} überein.
    Die Abweichung beträgt mehr als zwei Standardabweichungen, allerdings ist der Fehler des Mittelwerts eher eine Unterschätzung der wirklichen Messungenauigkeit.
    Es ist anzunehmen, dass ebenso ein unidentifizierter systematischer Fehler zum großen Fehler beiträgt. 
  \makebibliography
\end{document}
