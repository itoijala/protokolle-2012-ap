from labtools import *

U, I = np.loadtxt(lt.file('result/U_5'))

mask = np.array([1] * 2 + [0] * (len(U) - 2))

U = np.ma.masked_array(data=U, mask=mask)
I = np.ma.masked_array(data=I, mask=mask)

A, B = lt.linregress(np.log(U[~U.mask]), np.log(I[~I.mask]))

t = lt.SymbolRowTable()
t.add_si_row('A', A)
t.add_si_row('B', B)
t.add_hrule()
t.add_si_row(r'A_\t{theor}', 1.5, places=2)
t.savetable(lt.new('table/I_5.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(4, 100, 100)
ax.plot(x, lt.vals(unc.unumpy.exp(A * np.log(x) + B)), 'b-', label='Regressionsgerade')
ax.plot(U[U.mask].data, I[I.mask].data, 'gx', label='Messwerte nicht in Regression')
ax.plot(U[~U.mask], I[~I.mask], 'rx', label='Messwerte')
ax.set_xscale('log')
ax.set_yscale('log')
ax.xaxis.set_view_interval(4.5, 85, True)
ax.yaxis.set_view_interval(5e-5, 1.3e-3, True)
ax.xaxis.set_ticks([10.0, 80.0])
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e6))
ax.set_xlabel(r'\silabel{U_5}{\volt}')
ax.set_ylabel(r'\silabel{I_5}{\micro\ampere}')
lt.ax_reverse_legend(ax, 'upper left')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/I_5.tex'))
