from labtools import *

U, I = np.loadtxt(lt.file('data/d'), unpack=True)
I *= 1e-9

e, _, _, k, _, _, _, _, R_i = np.load(lt.file('result/constants.npy'))

t = lt.SymbolColumnTable()
t.add_si_column('U', U, r'\volt', places=2)
t.add_si_column('I', I * 1e9, r'\nano\ampere', places=([0] * (len(I) - 4) + [1] * 4))
t.savetable(lt.new('table/d.tex'))

V = U + R_i * I

mask = np.array([0] * 10 + [1] + [0] * 7 + [1] * 2)
V = np.ma.masked_array(data=V, mask=mask)
I = np.ma.masked_array(data=I, mask=mask)

C, D = lt.linregress(V[~V.mask], np.log(I[~I.mask]))

T = - e / k / C

t = lt.SymbolRowTable()
t.add_si_row('C', C, r'\per\volt')
t.add_si_row('D', D)
t.add_hrule()
t.add_si_row('T', T, r'\kelvin')
t.savetable(lt.new('table/T_5.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.array([0.2, 1])
ax.plot(x, lt.vals(unc.unumpy.exp(C * x + D)), 'b-', label='Regressionsgerade')
ax.plot(V[V.mask].data, I[I.mask].data, 'gx', label='Messwerte nicht in Regression')
ax.plot(V[~V.mask], I[~I.mask], 'rx', label='Messwerte')
ax.set_yscale('log')
ax.xaxis.set_view_interval(0.2, 1, True)
ax.yaxis.set_view_interval(2e-9, 3e-7, True)
ax.xaxis.set_major_formatter(lt.SiFormatter(1))
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e9))
ax.set_xlabel(r'\silabel{V}{\volt}')
ax.set_ylabel(r'\silabel{I}{\nano\ampere}')
lt.ax_reverse_legend(ax, 'lower left')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/T_5.tex'))
