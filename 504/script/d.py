from labtools import *

I_S, U_F, I_F = np.load(lt.file('result/I_S.npy'))

e, m, h, k, sigma, P_WL, eta, f, R_i = np.load(lt.file('result/constants.npy'))

T = ((I_F * U_F - P_WL) / sigma / eta / f)**0.25

W_A = - k * T[:-1] * unc.unumpy.log(I_S[:-1] / f * h**3 / 4 / np.pi / e / m / k**2 / T[:-1]**2)

W_A_mean = lt.mean(W_A)

t1 = lt.SymbolColumnTable()
t1.add_si_column('', [1, 2, 3, 4, 5], places=0)
t1.add_si_column('T', lt.vals(T), r'\kelvin', places=0)
t1.add_si_column(r'W_\t{A}', lt.vals(W_A) / sp.constants.eV, r'\electronvolt', places=1)

t2 = lt.SymbolRowTable()
t2.add_si_row(r'\mean{W_\t{A}}', W_A_mean / sp.constants.eV, r'\electronvolt')

t3 = lt.Table()
t3.add(lt.Row())
t3.add_hrule()
t3.add(lt.Row())
t3.add(lt.Column([t1, t2], '@{} c @{}'))
t3.savetable(lt.new('table/T.tex'))
