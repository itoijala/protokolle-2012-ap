from labtools import *

U_F, I_F = np.loadtxt(lt.file('data/a'), unpack=True)
U, I_1, I_2, I_3, I_4 = np.loadtxt(lt.file('data/b'), unpack=True)
U_5, I_5 = np.loadtxt(lt.file('data/c'), unpack=True)
U = np.array([list(U)] * 4 + [U_5])
I = np.array([I_1, I_2, I_3, I_4, I_5])
I *= 1e-6

I_S = np.amax(I, axis=1)[:-1]
I_S = unc.unumpy.uarray((I_S, [10e-6] * 3 + [20e-6]))

np.savetxt(lt.new('result/U_5'), (U[4], I[4]))

np.save(lt.new('result/I_S.npy'), (list(I_S) + [np.inf], U_F, I_F))

t = lt.SymbolColumnTable()
t.add_si_column('', [1, 2, 3, 4, 5])
t.add_si_column(r'I_\t{F}', I_F, r'\ampere', places=1)
t.add_si_column(r'U_\t{F}', U_F, r'\volt', places=1)
t.savetable(lt.new('table/I_F.tex'))

t = lt.SymbolColumnTable()
t.add_si_column('U', U[0], r'\volt', places=0)
for i in range(len(U) - 1):
    t.add_si_column('I_' + str(i + 1), I[i] * 1e6, r'\micro\ampere', places=0)
t.add(lt.VerticalSpace(1, '1em'))
t.add_si_column('U_5', U[4], r'\volt', places=0)
t.add_si_column('I_5', I[4] * 1e6, r'\micro\ampere', places=0)
t.savetable(lt.new('table/I.tex'))

t = lt.SymbolColumnTable()
t.add_si_column('', [1, 2, 3, 4], places=0)
t.add_si_column(r'I_\t{S}', I_S * 1e6, r'\micro\ampere')
t.savetable(lt.new('table/I_S.tex'))

color = ['b', 'g', 'r', 'c']

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
for i in range(len(U) - 1):
    ax.plot(U[i], I[i], color[i] + 'x', label=str(i + 1))
    ax.axhline(y=lt.vals(I_S[i]), ls='--', c=color[i])
ax.plot(U[4], I[4], 'mx', label='5')
ax.plot([-10], [0], 'k--', label='$I_\t{S}$') # dummy
ax.xaxis.set_view_interval(0, 230, True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e6))
ax.set_xlabel(r'\silabel{U_i}{\volt}')
ax.set_ylabel(r'\silabel{I_i}{\micro\ampere}')
ax.legend(loc='upper left')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/I_S.tex'))
