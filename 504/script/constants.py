from labtools import *

e = lt.constant('elementary charge')
m = lt.constant('electron mass')
h = lt.constant('Planck constant')
k = lt.constant('Boltzmann constant')
sigma = lt.constant('Stefan-Boltzmann constant')

P_WL = 1
eta = 0.28
f = 0.35 * 1e-4
R_i = 1e6

np.save(lt.new('result/constants.npy'), (e, m, h, k, sigma, P_WL, eta, f, R_i))

t = lt.SymbolRowTable()
t.add_si_row('e', e * 1e19, r'\coulomb', exp='e-19')
t.add_si_row('m', m * 1e31, r'\kilogram', exp='e-31')
t.add_si_row('h', h * 1e34, r'\joule\second', exp='e-34')
t.add_si_row('k', k * 1e23, r'\joule\per\kelvin', exp='e-23')
t.add_si_row(r'\sigma', sigma * 1e8, r'\watt\per\square\meter\per\kelvin\tothe{4}', exp='e-8')
t.add_hrule()
t.add_si_row(r'P_\t{WL}', P_WL, r'\watt', places=0)
t.add_si_row(r'\eta', eta, places=2)
t.add_si_row(r'f', f * 1e4, r'\square\centi\meter', places=2)
t.add_si_row(r'R_\t{i}', R_i * 1e-6, r'\mega\ohm', places=0)
t.savetable(lt.new('table/constants.tex'))
