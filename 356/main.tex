\input{header/report.tex}

\SetExperimentNumber{356}

\begin{document}
  \maketitlepage{Kettenschaltungen mit \texorpdfstring{$LC$}{LC}-Gliedern}{22.05.2012}{29.05.2012}
  \section{Ziel}
    An einer gekoppelten $LC$-Kette sollen verschiedene wellencharakteristische Größen ausgemessen werden.
  \section{Theorie}
    Eine Abfolge von Tief- oder Hochpässen, wie in Abbildung \ref{fig:htpass}, kann unter verschiedenen Gesichtspunkten betrachtet werden:
    \begin{itemize}
      \item als eine Schaltung zur Frequenztrennung eines polyfrequenten Signals;
      \item als ein Ensemble von gekoppelten Oszillatoren, deren Kopplung durch einen gemeinsam verwendeten Kondensator zu Stande kommt;
      \item als eine Transportvorrichtung für elektrische Energie, die im Limes vom Diskreten ins Kontinuierliche, also für unendlich kleine Kapazitäten und Induktivitäten als Ersatzschaltbild für einen verlustlosen Leiter angesehen werden kann.
    \end{itemize}
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/htpass.pdf}
      \caption{Beispiel für eine Tiefpass-, beziehungsweise Hochpasskette \cite{anleitung356}}
      \label{fig:htpass}
    \end{figure}

    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/node_n.pdf}
      \caption{Ströme und Spannungen in einem Abschnitt der $LC$-Kette \cite{anleitung356}}
      \label{fig:node_n}
    \end{figure}
    Zu Verdeutlichung der Indizes ist Abbildung \ref{fig:node_n} gegeben.
    Mit Hilfe der Kirchhoffschen Knotenregel
    \begin{eqn}
      \sum_i I_i = 0 ,
    \end{eqn}
    wonach die Summe aller Ströme in einem Knoten null ist, ergibt sich für die Strombilanz im Knotenpunkt $n$
    \begin{eqn}
      I_n - I_{n + 1} - I_{n, \t{quer}} = 0 . \eqlabel{eqn:node_rule}
    \end{eqn}
    Für den eingeschwungenen Zustand gilt
    \begin{eqns}
      I_n &=& \frac{U_n - U_{n - 1}}{\I \omega L} \\
      I_{n, \t{quer}} &=& U_n \I \omega C ,
    \end{eqns}
    wobei $\omega$ die Kreisfrequenz ist.
    Daraus folgt für Gleichung \eqref{eqn:node_rule}
    \begin{eqn}
      - \omega^2 C U_n + \frac{1}{L} \g(- U_{n - 1} + 2 U_n - U_{n + 1}) = 0 . \eqlabel{eqn:discrete_dgl_C}
    \end{eqn}
    Der Lösungsansatz
    \begin{eqn}
      U_n = U_0 \exp(\I \omega t) \exp(- \I n \theta t)
    \end{eqn}
    löst die diskrete Differentialgleichung \eqref{eqn:discrete_dgl_C} unter der Bedingung
    \begin{eqn}
      \omega^2 = \frac{4}{L C} \g(1 - \cos.\theta.) . \eqlabel{eqn:dispersion_1}
    \end{eqn}
    Die Beziehung \eqref{eqn:dispersion_1} wird als Dispersionsrelation bezeichnet und liefert einen Zusammenhang zwischen der Phasenänderung $\theta$ pro Kettenglied und der Kreisfrequenz $\omega$.
    Da die Kreisfrequenz als messbare Größe reell sein muss, gilt
    \begin{eqn}
      0 < \omega < \frac{2}{\sqrt{L C}} .
    \end{eqn}

    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/alternierend.pdf}
      \caption{Schaltung mit abwechselnder Größe der Kapazitäten \cite{anleitung356}}
      \label{fig:alternierend}
    \end{figure}
    Wird die Schaltung aus Abbildung \ref{fig:htpass} um eine Stufe abstrahiert, indem zwei sich periodisch abwechselnde Kapazitäten in die Schaltung eingebaut werden, so ergibt sich eine Schaltung wie in Abbildung \ref{fig:alternierend}; es gelten für die Kapazitäten die diskreten Differentialgleichungen
    \begin{eqns}
      - \omega^2 C_1 U_{2 n + 1} + \frac{1}{L} \g(- U_{2 n} + 2 U_{2 n + 1} - U_{2 n + 2}) &=& 0 \\
      - \omega^2 C_2 U_{2 n} + \frac{1}{L} \g(- U_{2 n - 1} + 2 U_{2 n} - U_{2 n + 1}) &=& 0 .
    \end{eqns}
    Mit dem Lösungsansatz, also zwei Wellen mit jeweils verschiedener Amplitude für die geraden und ungeraden Kettenelemente,
    \begin{eqns}
      U_{2 n + 1} &=& U_{0, 1} \exp(\I \omega t) \exp(- \I \g(2 n + 1) \theta t) \\
      U_{2 n} &=& U_{0, 2} \exp(\I \omega t) \exp(- \I \g(2 n) \theta t)
    \end{eqns}
    ergibt sich das Gleichungssystem
    \begin{eqn}
      \m{A} \v{U_0} = \v{0}
    \end{eqn}
    mit
    \begin{eqn}
      \m{A} =
      \begin{pmatrix}
        - \omega^2 C_1 + \frac{2}{L} & - \frac{2}{L} \cos.\theta. \\
        - \frac{2}{L} \cos.\theta. & - \omega^2 C_2 + \frac{2}{L}
      \end{pmatrix} . \eqlabel{eqn:2ndlgs}
    \end{eqn}
    Aus $\det.\m{A}. = 0$ folgt für die Dispersionsrelation
    \begin{eqn}
      \f{\omega_{1,2}}(\theta)^2 = \frac{1}{L} \g(\frac{1}{C_1} + \frac{1}{C_2}) \pm \frac{1}{L} \sqrt{\g(\frac{1}{C_1} + \frac{1}{C_2})^{\!\! 2} - \frac{4 \sin(\theta)^2}{C_1 C_2}} . \eqlabel{eqn:dispersion_12}
    \end{eqn}
    Durch Kleinwinkelnäherungen ergibt sich
    \begin{eqn}
      \f{\omega_2}(\theta) \approx \sqrt{\frac{2}{L} \frac{1}{C_1 + C_2}} \theta
    \end{eqn}
    für die Dispersionskurve mit negativen Vorzeichen.
    Beginnend im Ursprung erreicht sie ihr Maximum bei
    \begin{eqn}
      \f{\omega_2}(\frac{\PI}{2}) = \sqrt{\frac{2}{L C_1}} , \eqlabel{eqn:omega_gr_1}
    \end{eqn}
    das heißt bei zwei benachbarten gleichen Kapazitäten ist die Spannung um $\PI$ verschoben, und ihr Minimum für $\theta = 0$.
    Für die Dispersionskurve mit positivem Vorzeichen ergibt sich genähert
    \begin{eqn}
      \f{\omega_1}(0) \approx \sqrt{\frac{2}{L} \frac{C_1 + C_2}{C_1 C_2}} , \eqlabel{eqn:omega_gr_3}
    \end{eqn}
    bei $\theta = 0$. Die Frequenz $\omega_1$ nimmt also gemäß
    \begin{eqn}
      \f{\omega_1}(\theta) \approx \f{\omega_1}(0) \g(1 - \frac{C_1 C_2}{\g(C_1 + C_2)^2} \theta^2)
    \end{eqn}
    bis zum Minimalwert
    \begin{eqn}
      \f{\omega_1}(\frac{\PI}{2}) = \sqrt{\frac{2}{L C_2}} \eqlabel{eqn:omega_gr_2}
    \end{eqn}
    ab.
    Da der Minimalwert von $\omega_1$ ungleich dem Maximalwert von $\omega_2$ ist, entsteht ein Frequenzbereich, in dem keine Schwingung angeregt werden kann.
    \subsection{Phasen- und Gruppengeschwindigkeit}
      Breitet sich eine Welle in der Zeit $\Del{t}$ um $\Del{n}$ aus, so folgt für die Phasengeschwindigkeit
      \begin{eqn}
        v_\t{ph} = \frac{\Del{n}}{\Del{t}} . \eqlabel{eqn:gen_phase_velocity}
      \end{eqn}
      Da die Welle zum Zeitpunkt $t$ die Phase
      \begin{eqn}
        \Phi = \omega t - n \theta
      \end{eqn}
      besitzt und diese sich mit der Phasengeschwindigkeit \eqref{eqn:gen_phase_velocity} ausbreitet, gilt zum Zeitpunkt $t + \Del{t}$
      \begin{eqn}
        \Phi = \omega \g(t + \Del{t}) - \g(n + \Del{n}) \theta .
      \end{eqn}
      Folgedessen ergibt sich für die Phasengeschwindigkeit ebenfalls die Beziehung
      \begin{eqn}
        v_\t{ph} = \frac{\omega}{\theta} . \eqlabel{eqn:v_ph}
      \end{eqn}
      Gleichung \eqref{eqn:v_ph} beschreibt die Ausbreitungsgeschwindigkeit einer harmonischen ebenen Welle, wobei $\omega$ für alle $t$ konstant ist.
      Die Unbegrenztheit einer solchen Welle in Raum und Zeit verletzt bei einem Informationstransport Einsteins Axiome der speziellen Relativitätstheorie; eine Information kann daher nur mit Wellenpaketen transportiert werden, die im Frequenzraum ein Frequenzspektrum besitzen, im Gegensatz zum $\Diracdelta;$-Spike der ebenen Welle; sie werden also durch Fourierzerlegung aus mehreren ebenen Wellen zusammengesetzt.
      Die Geschwindigkeit der Einhüllenden Gauß-Kurve bezeichnet man als Gruppengeschwindigkeit $v_\t{gr}$.
      Wird die Information durch ein Medium mit linearer Dispersion geschickt, so ist $v_\t{ph} = v_\t{gr}$; im hier vorliegenden Fall einer nicht-linearen Dispersion (Gleichung \eqref{eqn:dispersion_12}) verschmiert das Wellenpaket, das heißt die Breite der Gauß-Einhüllenden wächst mit der Zeit, da die Phasengeschwindigkeiten der einzelnen Fourierkomponenten unterschiedlich sind.
      Um einen Ausdruck für die Gruppengeschwindigkeit zu gewinnen, reduziert man ohne Beschränkung der Allgemeinheit das Frequenzspektrum des Wellenpaketes auf zwei Frequenzen $\omega$ und $\omega'$, wobei $\abs{\omega - \omega'} \ll \omega$.
      Für die Amplitude des Wellenpaketes gilt
      \begin{eqn}
        \f{A}(t) = A_0 \cos(\omega t - \theta) + \cos(\omega' t - \theta') .
      \end{eqn}
      Mit Hilfe eines Additionstheorems und der Bedingung, dass beide Frequenzen sehr nah beieinander liegen, ist
      \begin{eqn}
        \f{A}(t) = 2 A_0 \cos(\omega t - \theta) \cos.\frac{1}{2} \g[\g(\omega - \omega') t - \g(\omega - \omega')]. .
      \end{eqn}
      Die Zeitabhängigkeit ist in Abbildung \ref{fig:schwebung} veranschaulicht, sie wird als Schwebung bezeichnet.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/schwebung.pdf}
        \caption{Schwebung \cite{anleitung356}}
        \label{fig:schwebung}
      \end{figure}
      Dabei bewegt sich ein Maximum mit der Geschwindigkeit \eqref{eqn:v_ph}, also für zwei Frequenzen
      \begin{eqn}
        v_\t{gr} = \frac{\omega - \omega'}{\theta - \theta'} ,
      \end{eqn}
      was im Grenzfall $n \to \infty$ zum Differentialquotienten
      \begin{eqn}
        v_\t{gr} = \d{\theta}{\omega};
      \end{eqn}
      wird.
      Somit ergibt sich hier
      \begin{eqns}
        v_\t{ph} &=& \frac{\omega}{\arccos(1 - \frac{1}{2} \omega^2 L C)} \eqlabel{eqn:v_ph_theory} \\
        v_\t{gr} &=& \sqrt{\frac{1}{L C} - \frac{1}{4} \omega^2} .
      \end{eqns}
      Wenn $\omega \to 0$, dann gehen Phasen- und Gruppengeschwindigkeit gegen den Wert
      \begin{eqn}
        \lim_{\omega \to 0} v_\t{ph} = \lim_{\omega \to 0} v_\t{gr} = \frac{1}{\sqrt{L C}} .
      \end{eqn}
      Bei kleinen Frequenzen $\omega^2 \ll \g(L C)^{-1}$ verschwindet die Dispersion; für die Grenzfrequenz
      \begin{eqn}
        \omega_\t{gr} = \frac{2}{\sqrt{L C}} \eqlabel{eqn:omega_gr}
      \end{eqn}
      verschwindet die Gruppengeschwindigkeit; ab hier kann keine Information durch die $LC$-Kette gesendet werden.
      Die Phasengeschwindigkeit bleibt endlich und erreicht dort ihr Minimum
      \begin{eqn}
        v_\t{ph} = \frac{2}{\PI} \frac{1}{\sqrt{L C}} .
      \end{eqn}
    \subsection{Widerstand einer \texorpdfstring{$LC$}{LC}-Kette}
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/eingangsimpedanz.pdf}
        \caption{Abkürzungen für die Eingangsimpedanz \cite{anleitung356}}
        \label{fig:eingangsimpedanz}
      \end{figure}
      Die Eingangsimpedanz $Z$ ist der Quotient aus der Spannung $U_0$, die an der Eingangskapazität $C / 2$ anliegt, und dem einfließenden Strom $I_0$.
      Ausgehend von Abbildung \ref{fig:eingangsimpedanz} ergibt sich mit der Kirchhoffschen Knotenregel
      \begin{eqn}
        I_0 - \I \omega \frac{C}{2} U_0 + \frac{U_1 - U_0}{\I \omega L} = 0
      \end{eqn}
      für den Strom im Punkt $P_0$.
      Mit Hilfe des Lösungsansatzes
      \begin{eqn}
        \f{U_n}(\theta) = U_0 \exp(- \I n \theta)
      \end{eqn}
      und der ausgerechneten Dispersion ergibt sich die Eingangsimpedanz
      \begin{eqn}
        Z = \frac{\omega L}{\sin.\theta.} .
      \end{eqn}
      Mit Hilfe von Gleichung \eqref{eqn:dispersion_1} lässt sich der Eingangswiderstand umformen in
      \begin{eqn}
        \f{Z}(\omega) = \sqrt{\frac{L}{C}} \frac{1}{\sqrt{1 - \frac{1}{4} \omega^2 L C}} . \eqlabel{eqn:in_impedance}
      \end{eqn}
      Diese Größe ist reell, trotz der verbauten imaginären Impedanzen.
      Dies impliziert, dass Strom und Spannung an jedem Ort der Kette in Phase sind, und weiterhin, dass der Energiefluss nur in die Schaltung hinein, nicht aber durch Reflektion aus der Schaltung heraus erfolgt.
      Wird in den letzten Schwingkreis ein Widerstand des Betrages $Z$, der sich aus Gleichung \eqref{eqn:in_impedance} ergibt, eingebaut, so erscheint die endliche Kette der $LC$-Schaltungen unendlich.
      Nun ergibt sich nach Anschließen eines Impedanzmeters an die Anschlussklemmen (Abbildung \ref{fig:infty_chain}) für eine beliebige Zahl von $LC$-Schaltungen immer der selbe Eingangswiderstand.
      \begin{figure}
        \includegraphics{graphic/infty_chain.pdf}
        \caption{Unendliche $LC$-Kette \cite{anleitung356}}
        \label{fig:infty_chain}
      \end{figure}
      Dieser Widerstand wird als Wellenwiderstand oder charakteristische Impedanz der Schaltung bezeichnet.
      Das Wachstumsverhalten von $\f{Z}(\omega)$ ist dadurch charakterisiert, dass für $\omega \ll \omega_\t{gr}$ der Funktionswert im Wesentlichen $\sqrt{L / C}$ ist; nähert sich die Frequenz der Grenzfrequenz, so divergiert Gleichung \eqref{eqn:in_impedance} gegen unendlich.
    \subsection{Eigenschaften einer endlich langen \texorpdfstring{$LC$}{LC}-Kette} \label{efrq}
      Im Folgenden wird angenommen, dass am Ende der $LC$-Kette eine Reflektion stattfindet, sodass sich ein- und auslaufende Welle zu einer stehenden Welle überlagern können.
      Der Quotient aus den Amplituden der hin- und rücklaufenden Wellen, der Reflektionsfaktor, hängt stark von der Gestalt des Kettenendes ab.
      Dabei gilt für die Spannung die am komplexen Endwiderstand $r$ anliegt
      \begin{eqn}
        U_\t{r} = U_\t{e} + U_\t{ref} ,
      \end{eqn}
      wobei $U_\t{e}$ die einfallende und $U_\t{ref}$ die reflektierte Spannung ist.
      Für den Strom gilt genauso
      \begin{eqn}
        I_\t{r} = I_\t{e} + I_\t{ref} .
      \end{eqn}
      Mit dem Ohmschen Gesetz
      \begin{eqn}
        U = RI ,
      \end{eqn}
      wobei $R$ der Widerstand ist, ergibt sich
      \begin{eqn}
        \frac{U_\t{ref}}{U_\t{E}} = \frac{r - Z}{r + Z} .
      \end{eqn}
      Damit ergeben sich folgende Spezialfälle:
      \begin{description}
        \item[offenes Ende] $U_\t{r}$ fällt weg, sodass die einfallende Spannung gleich der reflektierten Spannung ist;
        \item[Kurzschluss] $U_\t{r}$ fällt weg, die Welle erfährt bei Reflektion eine Phasenverschiebung um $\PI$;
        \item[Abschluss mit Wellenwiderstand] Es wird keine Energie reflektiert; vielmehr fließt sie vom Eingangswiderstand zum Ausgangswiderstand;
        \item[sonstige Fälle] Die Wellen werden teilweise verschoben; ist $r$ komplex, findet eine zusätzliche Phasenverschiebung statt.
      \end{description}

      Die hinlaufende Welle wird also beschrieben durch
      \begin{eqns}
        A_\t{hin} &=& A_0 \cos(\omega t - n \theta)
        \itext{und die reflektierte durch}
        A_\t{rü} &=& A_0 \cos(\omega t + n \theta) .
      \end{eqns}
      Durch Superposition ergibt sich
      \begin{eqn}
        A_\t{hin} + A_\t{rü} = 2 A_0 \cdot
        \begin{cases}
          \cos.\omega t. \cos.n \theta. , & r = \infty \\
          \sin.\omega t. \sin.n \theta. , & r = 0
        \end{cases} .
      \end{eqn}
      Somit verschwindet die Schwingungsamplitude für
      \begin{eqn}
        n_k \theta = \PI \cdot
        \g{.}{\begin{cases}
          k + \frac{1}{2} , & r = \infty \\
          k , & r = 0
        \end{cases}}{\}} , \quad k \in \setN_0 .
      \end{eqn}
      Ist der Endwiderstand unendlich, bildet sich nur dann eine stehende Welle aus, wenn für das Kettenende $n_\t{max}$ gilt
      \begin{eqn}
        n_\t{max} = \theta_k = k \PI , \eqlabel{eqn:randbdg}
      \end{eqn}
      am Kettenende muss also ein Spannungsbauch auftreten.
      Verschwindet der Endwiderstand, so muss ebenfalls \eqref{eqn:randbdg} erfüllt sein, damit sich ein Knoten am Rand ausbildet.
      Bei einer Kette von $n_\t{max}$ Gliedern, kommt es somit maximal zu $n_\t{max}$ stehenden Wellen.
      Durch Gleichung \eqref{eqn:randbdg} und der Dispersionsrelation \eqref{eqn:dispersion_1} errechnen sich die zugehörigen Frequenzen.

      Weitere Wellen treten für die Bedingung
      \begin{eqn}
        n_\t{max} \theta_l = l \frac{\PI}{2} , \quad l \in \g\{0\} \cup \set{l = 2 \lambda - 1}{\lambda \in \setN , \lambda < n_\t{max}}
      \end{eqn}
      auf.
      Hier liegen also am Anfang ein Bauch und am Ende ein Knoten.
      Damit sind insgesamt $2 n_\t{max}$ Eigenschwingungen möglich.
  \section{Aufbau und Durchführung}
    \subsection{Aufnahme der Durchlasskurve}
      Es soll die Ausgangsspannung am Ende der Kette in Abhängigkeit von der Frequenz der Speisespannung bei konstantem Eingangsstrom ermittelt werden.
      Hierbei wird die Schaltung in Abbildung \ref{fig:durchlasskurve} benutzt.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/durchlasskurve.pdf}
        \caption{Schaltung zur Ermittlung der Durchlasskurve \cite{anleitung356}}
        \label{fig:durchlasskurve}
      \end{figure}
      An dem $x$- und $y$-Eingang des Schreibers gibt es äquivalente Einstellungsmöglichkeiten, so lässt sich einstellen, ob der Schreiber für den Kanal das Signal oder eine konstante Spannung verwendet.
      Des Weiteren gibt es einen Knopf mit dem der Offset des Schlittens eingestellt werden kann.
      Zwei weitere Knöpfe dienen dazu die vom Schlitten zurückgelegte Strecke in Abhängigkeit von der eingehenden Spannung einzustellen, dies ist sowas wie ein Maßstab mit dem sich die Empfindlichkeit des Schreibers regeln lässt, ein Knopf ist für die grobe Einstellung, der andere für Feinjustierung.
      Der $x$-$y$-Schreiber besitzt weiterhin Knöpfe zur elektrostatischen Fixierung des Millimeterpapiers und um den Stift, der die Kurve zeichnet, vom Blatt hochzuheben oder abzusenken.
      Für die Justage muss am Spannungsmessgerät die Messempfindlichkeit derart eingestellt werden, dass der Zeigerausschlag während der kontinuierlichen Änderung der Frequenz durch den NF-Generator innerhalb des eingestellten Messbereichs des Spannungsmessgerätes liegt; in diesem Fall kommt das zu untersuchende Spannungsintervall aus dem Gleichspannungsausgang des Millivoltmeters.

      Der NF-Generator fährt ein einstellbares Frequenzintervall logarithmisch ab, dabei kann die Mittelpunktsfrequenz des Intervalls, sowie der Abstand der Grenzen vom Mittelpunkt eingestellt werden.
      Bei der $LC$-Kette sollte um die Grenzfrequenz \eqref{eqn:omega_gr} gemessen werden.
      Bei der $LC_1C_2$-Kette sollte ein Bereich um die Grenzfrequenzen \eqref{eqn:omega_gr_1}, \eqref{eqn:omega_gr_2} und \eqref{eqn:omega_gr_3} gemessen werden.
      Um den oberen Ast zu messen, muss der Messbereich des Millivoltmeters während der Messung geändert werden, um das Signal zu verstärken.
      Nach erfolgreicher Justage ohne Stift, senkt man diesen ab und nimmt für das eingestellte Frequenzintervall eine Kurve auf.
    \subsection{Ermittlung der Dispersionsrelation} \label{eigenfreq}
      Bei dieser Messung werden mit Hilfe von Lissajous-Figuren die Eigenfrequenzen der $LC$- und der $LC_1C_2$-Kette gemessen.
      Dies sind die Frequenzen, bei denen die Phasenverschiebung am Ende der Kette ein Vielfaches von $\PI$ ist, also bei denen die Lissajous-Figur zu einer Geraden wird.
      Die für diesen Versuch bezogen auf Abbildung \ref{fig:durchlasskurve} leicht abgeänderte Schaltung befindet sich in Abbildung \ref{fig:dispersionsrel}.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/dispersionsrel.pdf}
        \caption{Schaltung zur Aufnahme der Dispersionsrelation \cite{anleitung356}}
        \label{fig:dispersionsrel}
      \end{figure}
    \subsection{Nachweis stehender Wellen}
      Hier genügt es, die Schaltung aus Abbildung \ref{fig:durchlasskurve} zu benutzen, ohne den Schreiber, die Wobbeleinrichtung und die Abschlusswiderstände $Z$.
      Stehende Wellen ergeben sich für die in \ref{eigenfreq} ermittelten Eigenfrequenzen und zeichnen sich dadurch aus, dass am Anfang und Ende der Kette maximale Spannung auftritt.
      Mit Hilfe eines Stufenschalters wird ein Millivoltmeter der Reihe nach an jedes Kettenglied angeschlossen und die dort anliegende Spannung ermittelt; so zeichnet sich der Verlauf der Spannung entlang der Kettengleider ab.
      Gemessen wird bei der 1. und 2. Eigenschwingung einer offenen $LC$-Kette und einer durch einen Widerstand abgeschlossenen $LC$-Kette für eine Frequenz.
    \subsection{Messung der Gruppengeschwindigkeit}
      Hierbei wird die Schaltung aus Abbildung \ref{fig:grupgeschw} benutzt.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/grupgeschw.pdf}
        \caption{Schaltung zu Ermittlung der Gruppengeschwindigkeit \cite{anleitung356}}
        \label{fig:grupgeschw}
      \end{figure}
      Die Signalfrequenz, unter der $v_\t{gr}$ gemessen werden soll, kann an einem NF-Generator eingestellt werden, es werden die in \ref{eigenfreq} ermittelten Eigenfrequenzen eingestellt.
      Am Wellengruppengenerator lassen sich Wellengruppen mit einer Erzeugungsdauer von \SIrange{200}{800}{\micro\second} erzeugen.
      Für die Messung leitet man die Wellengruppe vor und nach der Kette auf einen Zwei-Kanal-Oszillographen.
      Nach richtiger Wellengruppenlängenwahl, sowie korrekter Justierung der Zeitablenkung im Oszilloskop, erhält man ein Bild, wie in Abbildung \ref{fig:oszibild}, worin die Dispersion, sowie die Schwerpunktsverschiebung $T$ des rautenförmigen Pakets deutlich wird.
      Die entstehenden Bilder werden für die jeweiligen Eigenfrequenzen abgespeichert.
      \begin{figure}
        \includegraphics[scale=0.7]{graphic/oszibild.pdf}
        \caption{Beispiel für das Oszillographenbild des Zwei\-/Kanal\-/Oszillographen bei der Ermittlung der Gruppengeschwindigkeit \cite{anleitung356}}
        \label{fig:oszibild}
      \end{figure}
  \section{Messwerte}
    \subsection{Durchlasskurven}
      Die Daten der im Versuch verwendeten Induktivitäten $L$ und Kapazitäten $C_1$ und $C_2$ stehen in Tabelle \ref{tab:L}.
      In Versuchsteilen mit nur einer Kapazität wurde $C_1$ verwendet, also gilt in allen Gleichungen $C = C_1$.
      Die Mittels des $x$-$y$-Schreibers bestimmten Durchlasskurven sind in den Abbildungen \ref{fig:xy-1} und \ref{fig:xy-12} aufgeführt.
      Die abgelesenen Grenzfrequenzen $\nu_\t{gr}$, $\nu_{\t{gr}, 1}$, $\nu_{\t{gr}, 2}$ und $\nu_{\t{gr}, 3}$ im jeweiligen Wendepunkt der Graphen, stehen in Tabelle \ref{tab:nu_gr}.
      \begin{table}
        \input{table/L.tex}
        \caption{Daten der verwendeten Induktivitäten und Kapazitäten}
        \label{tab:L}
      \end{table}
      \begin{table}
        \input{table/nu_gr.tex}
        \caption{Aus den Durchlasskurven abgelesene Grenzfrequenzen}
        \label{tab:nu_gr}
      \end{table}
      \FloatBarrier
    \subsection{Dispersionskurven}
      Die Messwerte der Eigenfrequenzen, bei den die Phasenverschiebung ein Vielfaches von $\PI$ ist, stehen für die $LC$-Kette in Tabelle \ref{tab:nu_1} und für die $LC_1C_2$-Kette in Tabelle \ref{tab:nu_12}.
      \begin{table}
        \input{table/nu_1.tex}
        \caption{Gemessene Eigenfrequenzen der $LC$-Kette}
        \label{tab:nu_1}
      \end{table}
      \begin{table}
        \input{table/nu_12.tex}
        \caption{Gemessene Eigenfrequenzen der $LC_1C_2$-Kette}
        \label{tab:nu_12}
      \end{table}
      \FloatBarrier
    \subsection{Stehende Wellen}
      Die Messwerte der Spannung am Kettenglied $n$ bei der ersten $A_1$ und zweiten $A_2$ Eigenfrequenz der offenen $LC$-Kette und der ersten Eigenfrequenz der abgeschlossenen $LC$-Kette $A_Z$ stehen in Tabelle \ref{tab:A}.
      \begin{table}
        \input{table/A.tex}
        \caption{Spannungen an den einzelnen Kettengliedern}
        \label{tab:A}
      \end{table}
  \section{Auswertung}
    \input{statistics.tex}
    \input{linregress.tex}
    \subsection{Durchlasskurven, Grenzfrequenzen}
      Die mit dem $x$-$y$-Schreiber aufgenommenen Durchlasskurven sind in den Abbildungen \ref{fig:xy-1} und \ref{fig:xy-12} aufgeführt.
      Der Abstand der blauen Punkte auf der logarithmischen Skala unten beträgt \SI{5}{\kilo\hertz}.
      Die Messwerte, mit $2 \PI$ multipliziert, stehen mit den berechneten Theoriewerten in Tabelle \ref{tab:omega_gr}.
      Für $\omega_{\t{gr}, \t{theor}}$ gilt laut Formel \eqref{eqn:omega_gr}
      \begin{eqn}
        \omega_{\t{gr}, \t{theor}} = \frac{2}{\sqrt{L C_1}} .
      \end{eqn}
      Aus den Formeln \eqref{eqn:omega_gr_1}, \eqref{eqn:omega_gr_2} und \eqref{eqn:omega_gr_3} folgt
      \begin{eqns}
        \omega_{\t{gr}, 1, \t{theor}} &=& \sqrt{\frac{2}{L C_1}} \\
        \omega_{\t{gr}, 2, \t{theor}} &=& \sqrt{\frac{2}{L C_2}} \\
        \omega_{\t{gr}, 3, \t{theor}} &=& \sqrt{\frac{2}{L} \frac{C_1 + C_2}{C_1 C_2}} .
      \end{eqns}
      \begin{figure}
        \input{graphic/xy-1.tex}
        \caption{Durchlasskurve der $LC$-Kette}
        \label{fig:xy-1}
      \end{figure}
      \begin{figure}
        \input{graphic/xy-12.tex}
        \caption{Durchlasskurve der $LC_1C_2$-Kette}
        \label{fig:xy-12}
      \end{figure}
      \begin{table}
        \input{table/omega_gr.tex}
        \caption{Mess- und Theoriewerte der Grenzfrequenzen}
        \label{tab:omega_gr}
      \end{table}
      \FloatBarrier
    \subsection{Dispersionskurven}
      Die Dispersionskurven, also $\omega$ gegen $\theta$ aufgetragen, sind in den Abbildungen \ref{fig:dispersion_1} für die $LC$-Kette und \ref{fig:dispersion_12} für die $LC_1C_2$-Kette aufgeführt.
      Bei den Messwerten in den Tabellen \ref{tab:nu_1} und \ref{tab:nu_12} gilt für die Phasenverschiebung pro Kettenglied bei der $m$-ten Eigenfrequenz
      \begin{eqn}
        \theta_{1, 12} = \frac{m \PI}{14} ,
      \end{eqn}
      da die Gesamtphasenverschiebung $m \PI$ beträgt und die Kette aus $14$ Gliedern besteht.
      Für die Theoriekurven folgt aus den Formeln \eqref{eqn:dispersion_1} und \eqref{eqn:dispersion_12}
      \begin{eqns}
        \omega_1 &=& \sqrt{\frac{2}{L C_1} \g(1 - \cos.\theta_1.)} \\
        \omega_{12} &=& \sqrt{\frac{1}{L} \g(\frac{1}{C_1} + \frac{1}{C_2}) - \frac{1}{L} \sqrt{\g(\frac{1}{C_1} + \frac{1}{C_2})^{\!\! 2} - \frac{4 \sin(\theta_{12})^2}{C_1 C_2}}} .
      \end{eqns}
      Das Minuszeichen bei $\omega_{12}$ folgt daraus, dass bei der Messung nur die Eigenfrequenzen unterhalb der ersten Grenzfrequenz messbar waren.
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/dispersion_1.pdf}
        \caption{Messwerte aus der Dispersionsmessung und Theoriekurve für die $LC$-Kette}
        \label{fig:dispersion_1}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/dispersion_12.pdf}
        \caption{Messwerte aus der Dispersionsmessung und Theoriekurve für die $LC_1C_2$-Kette}
        \label{fig:dispersion_12}
      \end{figure}
      \FloatBarrier
    \subsection{Phasengeschwindigkeit}
      Mit den Messwerten aus Tabelle \ref{tab:nu_1} gilt nach \eqref{eqn:v_ph} für die Phasengeschwindigkeit
      \begin{eqn}
        v_\t{ph} = \frac{\omega_1}{\theta_1} .
      \end{eqn}
      Für die Theoriekurve folgt aus \eqref{eqn:v_ph_theory}
      \begin{eqn}
        v_{\t{ph}, \t{theor}} = \frac{\omega_1}{\arccos(1 - \frac{1}{2} L C_1 \omega_1^2)} .
      \end{eqn}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/v_ph.pdf}
        \caption{Die Phasengeschwindigkeit gegen die Frequenz aufgetragen}
        \label{fig:v_ph}
      \end{figure}
      \FloatBarrier
    \subsection{Stehende Wellen}
      Die Messwerte aus Tabelle \ref{tab:A} sind in den Abbildungen \ref{fig:A_1} bis \ref{fig:A_Z} aufgetragen.
      In den Tabellen \ref{tab:A_1} bis \ref{tab:A_Z} befinden sich durch Ausgleichsrechnung mit der Funktion
      \begin{eqn}
        A = E \abs{\cos(F n + G)} + H
      \end{eqn}
      ermittelten Werte der Konstanten $E$, $F$, $G$ und $H$.
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/A_1.pdf}
        \caption{Messwerte der Spannungen an den Kettengliedern bei der ersten Eigenfrequenz}
        \label{fig:A_1}
      \end{figure}
      \begin{table}
        \input{table/A_1.tex}
        \caption{Ergebnisse der Ausgleichsrechnung für die erste Eigenfrequenz}
        \label{tab:A_1}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/A_2.pdf}
        \caption{Messwerte der Spannungen an den Kettengliedern bei der zweiten Eigenfrequenz}
        \label{fig:A_2}
      \end{figure}
      \begin{table}
        \input{table/A_2.tex}
        \caption{Ergebnisse der Ausgleichsrechnung für die zweiten Eigenfrequenz}
        \label{tab:A_2}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/A_Z.pdf}
        \caption{Messwerte der Spannungen an den Kettengliedern bei der zweiten Eigenfrequenz und abgeschlossener Kette}
        \label{fig:A_Z}
      \end{figure}
      \begin{table}
        \input{table/A_Z.tex}
        \caption{Ergebnisse der Ausgleichsrechnung für die erste Eigenfrequenz und abgeschlossene Kette}
        \label{tab:A_Z}
      \end{table}
      \FloatBarrier
    \subsection{Gruppengeschwindigkeit}
      Die Beträge der Messwerte der Eingangsspannung $U_\t{E}$ und der Ausgangsspannung $U_\t{A}$ in Abhängigkeit von der Zeit $t$ sind in den Abbildungen \ref{fig:1} bis \ref{fig:11} für die ersten elf Eigenfrequenzen aufgetragen.
      Die Maxima der Spannungen werden bestimmt und durch zwei lineare Regressionen zu zwei Geraden zusammengefasst.
      Dabei werden jeweils die Maxima links und rechts des höchsten Maximums verwendet, wobei das mittlere Maximum in beiden Intervallen enthalten ist.
      Der zeitliche Abstand $T$ der Schnittpunkte der Geraden ist die zeitliche Verschiebung des Schwerpunkts des Wellenpakets beim Durchlaufen der $LC$-Kette.
      Da die Kette $14$ Gleider enthält, kann die Gruppengeschwindigkeit durch
      \begin{eqn}
        v_\t{gr} = \frac{14}{T}
      \end{eqn}
      bestimmt werden.
      Die angegebenen Fehler von $T$ und $v_\t{gr}$ ergeben sich aus den Fehlern der linearen Regression.
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/1.pdf}
        \caption{Messwerte für die erste Eigenfrequenz}
        \label{fig:1}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/2.pdf}
        \caption{Messwerte für die zweite Eigenfrequenz}
        \label{fig:2}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/3.pdf}
        \caption{Messwerte für die dritte Eigenfrequenz}
        \label{fig:3}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/4.pdf}
        \caption{Messwerte für die vierte Eigenfrequenz}
        \label{fig:4}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/5.pdf}
        \caption{Messwerte für die fünfte Eigenfrequenz}
        \label{fig:5}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/6.pdf}
        \caption{Messwerte für die sechste Eigenfrequenz}
        \label{fig:6}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/7.pdf}
        \caption{Messwerte für die siebte Eigenfrequenz}
        \label{fig:7}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/8.pdf}
        \caption{Messwerte für die achte Eigenfrequenz}
        \label{fig:8}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/9.pdf}
        \caption{Messwerte für die neunte Eigenfrequenz}
        \label{fig:9}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/10.pdf}
        \caption{Messwerte für die zehnte Eigenfrequenz}
        \label{fig:10}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/11.pdf}
        \caption{Messwerte für die elfte Eigenfrequenz}
        \label{fig:11}
      \end{figure}
      \begin{table}
        \input{table/T.tex}
        \caption{Berechnete Werte für $T$ und $v_\t{gr}$}
        \label{tab:T}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/v_gr.pdf}
        \caption{Berechnete Werte von $v_\t{gr}$}
        \label{fig:v_gr}
      \end{figure}
  \section{Diskussion}
    \subsection{Durchlasskurven, Grenzfrequenzen}
      Die aus den aufgenommenen Durchlasskurven abgelesenen Werte für die Grenzfrequenzen passen gut mit den Theoriewerten überein.
      In Tabelle \ref{tab:omega_gr} werden der theoretische und der gemessene Wert verglichen; die Theoriewerte liegen innerhalb einer Standardabweichung.
      Dies bedeutet, dass die Messung ein Erfolg war, obwohl es erhebliche Schwierigkeiten gab, die richtigen Einstellungen des $x$-$y$-Schreibers zu finden und die logarithmische Skala aufzunehmen.
      Dies lag unter anderem daran, dass sich die Geschwindigkeit der Frequenzänderung bei großen Frequenzen wegen der logarithmischen Skala größer wird, also eine verbesserte Reaktionsgeschwindigkeit erfordert.
      Ein anderes Problem war, dass der obere Ast bei der zweiten Messung zunächst nicht sichtbar war und verstärkt werden musste.
    \subsection{Dispersionskurven}
      Die Messwerte und Theoriekurven in den Abbildungen \ref{fig:dispersion_1} und \ref{fig:dispersion_12} stimmen sehr gut überein.
      Bei der Messung war es nicht möglich, Eigenfrequenzen im oberen Ast zu bestimmen, da die Lissajous-Figur oberhalb der letzten gemessenen Eigenfrequenz zu einem vertikalen Strich mutierte und sich nicht durch weiteres Erhöhen der Frequenz ändern ließ.
      Ein Problem lag darin, dass sich die Frequenz besonders bei höheren Frequenzen wegen der logarithmischen Skala des Generators nicht mehr präzise einstellen ließ.
    \subsection{Phasengeschwindigkeit}
      Die Messwerte in Abbildung \ref{fig:v_ph} passen gut mit der Theoriekurve überein.
      Da die Messwerte des letzten Abschnitts verwendet werden, trifft auch hier das im letzten Abschnitt angesprochene Problem mit der Genauigkeit der Frequenzeinstellung zu.
    \subsection{Stehende Wellen}
      Wie nach der Ausgleichsrechnung ersichtlich, ist der Schwingungszustand der ersten Eigenfrequenz eine stehende Welle mit einer Periode auf der Länge der Kette.
      Die Spannungsmaxima befinden sich an den Kettenenden, wie von der Theorie vorhergesagt.
      Bei der zweiten Eingenfrequenz pasen zwei gesamte Perioden auf die Kette, wobei auch hier Maxima an den Enden auftreten.
      Bei der angeschlossenen Kette ergibt sich bei der ersten Eigenfrequenz eine ganze Periode auf der Kette, jedoch sind hier Minima an den Enden der Kette.
      Bei allen drei Messungen ist die Übereinstimmung von Messwerten und Ausgleichskurve akzeptabel.
    \subsection{Gruppengeschwindigkeit}
      Bei den niedrigen Eigenfrequenzen gibt es nur wenige Maxima für die lineare Regression.
      Trotzdem kann man schon hier ein Verschieben des Schwerpunktes erkennen.
      Aus diesem Grund wurde der Betrag der Spannung verwendet, da dies eine Verdopplung der Anzahl der Maxima bedeutet, ohne das Ergebnis zu ändern.
      Hinzu kam, dass die Eigenfrequenzen nicht immer exakt getroffen werden konnten, da für größere Frequenzen die Einstellung auf Grund der logarithmischen Skala des Frequenzgenerators ungenau wurde.
      Ein Zerfließen der Kurve tritt jedoch erst in Abbildung \ref{fig:10} und Abbildung \ref{fig:11} auf. 
      Bei der Bestimmung von $T$ tritt ein sehr großer Fehler auf, der sich entsprechend für die Gruppengeschwindigkeit fortpflanzt.
      Der angegebene Fehler bezieht sich nur auf die Fehler aus der linearen Regression.
      Hinzu kommen noch andere Fehlerquellen wie die ungenaue Bestimmung der Maxima und die Maxima, die wegen der nicht genauen Frequenzeinstellung neben dem Wellenpaket auftreten.
  \makebibliography
\end{document}
