from labtools import *

L, C1, C2 = np.loadtxt(lt.file('result/L'))

nu_gr = unc.ufloat((64000, 2000))
nu_gr_1 = unc.ufloat((45000, 2000))
nu_gr_2 = unc.ufloat((65000, 2000))
nu_gr_3 = unc.ufloat((80000, 1000))

omega_gr = nu_gr * 2 * np.pi
omega_gr_1 = nu_gr_1 * 2 * np.pi
omega_gr_2 = nu_gr_2 * 2 * np.pi
omega_gr_3 = nu_gr_3 * 2 * np.pi

omega_gr_theory = 2 / np.sqrt(L * C1)
omega_gr_1_theory = np.sqrt(2 / L / C1)
omega_gr_2_theory = np.sqrt(2 / L / C2)
omega_gr_3_theory = np.sqrt(2 / L * (C1 + C2) / C1 / C2)

t = lt.SymbolRowTable()
t.add_si_row(r'\nu_\t{gr}', nu_gr * 1e-3, r'\kilo\hertz')
t.add_si_row(r'\nu_{\t{gr}, 1}', nu_gr_1 * 1e-3, r'\kilo\hertz')
t.add_si_row(r'\nu_{\t{gr}, 2}', nu_gr_2 * 1e-3, r'\kilo\hertz')
t.add_si_row(r'\nu_{\t{gr}, 3}', nu_gr_3 * 1e-3, r'\kilo\hertz')
t.savetable(lt.new('table/nu_gr.tex'))

t = lt.SymbolRowTable()
t.add_si_row(r'\omega_\t{gr}', omega_gr * 1e-3, r'\kilo\hertz')
t.add_si_row(r'\omega_{\t{gr}, \t{theor}}', omega_gr_theory * 1e-3, r'\kilo\hertz', places=0)
t.add_hrule()
t.add_si_row(r'\omega_{\t{gr}, 1}', omega_gr_1 * 1e-3, r'\kilo\hertz')
t.add_si_row(r'\omega_{\t{gr}, 1, \t{theor}}', omega_gr_1_theory * 1e-3, r'\kilo\hertz', places=0)
t.add_hrule()
t.add_si_row(r'\omega_{\t{gr}, 2}', omega_gr_2 * 1e-3, r'\kilo\hertz')
t.add_si_row(r'\omega_{\t{gr}, 2, \t{theor}}', omega_gr_2_theory * 1e-3, r'\kilo\hertz', places=0)
t.add_hrule()
t.add_si_row(r'\omega_{\t{gr}, 3}', omega_gr_3 * 1e-3, r'\kilo\hertz')
t.add_si_row(r'\omega_{\t{gr}, 3, \t{theor}}', omega_gr_3_theory * 1e-3, r'\kilo\hertz', places=0)
t.savetable(lt.new('table/omega_gr.tex'))
