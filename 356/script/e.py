from labtools import *

settings = [[],
            [0, 0, 0,  0, 50, 50, 0.35, 8],  # savefig(lt.new('graph/1.tex'))
            [0, 0, 0,  0, 50, 50, 0.7,  18], # savefig(lt.new('graph/2.tex'))
            [0, 0, 0,  1, 50, 50, 0.9,  22], # savefig(lt.new('graph/3.tex'))
            [0, 0, 0,  3, 50, 50, 1,    22], # savefig(lt.new('graph/4.tex'))
            [0, 0, 1,  5, 30, 50, 1,    25], # savefig(lt.new('graph/5.tex'))
            [0, 0, 4,  6, 30, 30, 1,    25], # savefig(lt.new('graph/6.tex'))
            [0, 0, 4,  8, 30, 30, 1,    27], # savefig(lt.new('graph/7.tex'))
            [0, 0, 9,  8, 30, 30, 1,    28], # savefig(lt.new('graph/8.tex'))
            [0, 0, 13, 3, 30, 30, 1.1,  27], # savefig(lt.new('graph/9.tex'))
            [0, 0, 20, 0, 30, 30, 1.1,  27], # savefig(lt.new('graph/10.tex'))
            [0, 0, 23, 0, 30, 30, 1,    22]] # savefig(lt.new('graph/11.tex'))

T = []

for i in range(1, 12):
    s = settings[i]
    t, E, A = np.loadtxt(lt.file('data/' + str(i)), unpack=True)

    fig = plt.figure()

    M = lt.peakdetect(np.abs(E), t, s[4])[0]
    M = M[s[0]:]
    if s[1] > 0:
        M = M[:-[1]]
    M = np.array(M).T
    Mmax = np.argmax(M[1])
    M1 = M.T[:Mmax + 1].T
    M2 = M.T[Mmax:].T

    N = lt.peakdetect(np.abs(A), t, s[5])[0]
    N = N[s[2]:]
    if s[3] > 0:
        N = N[:-s[3]]
    N = np.array(N).T
    Nmax = np.argmax(N[1])
    N1 = N.T[:Nmax + 1].T
    N2 = N.T[Nmax:].T

    if i > 1:
        B1 = lt.linregress(M1[0], M1[1])
        B2 = lt.linregress(M2[0], M2[1])

        C1 = lt.linregress(N1[0], N1[1])
        C2 = lt.linregress(N2[0], N2[1])
    else:
        B1 = (M1[1][1] - M1[1][0]) / (M1[0][1] - M1[0][0])
        B1 = (B1, M1[1][0] - B1 * M1[0][0])
        B2 = (M2[1][1] - M2[1][0]) / (M2[0][1] - M2[0][0])
        B2 = (B2, M2[1][0] - B2 * M2[0][0])

        C1 = (N1[1][1] - N1[1][0]) / (N1[0][1] - N1[0][0])
        C1 = (C1, N1[1][0] - C1 * N1[0][0])
        C2 = (N2[1][1] - N2[1][0]) / (N2[0][1] - N2[0][0])
        C2 = (C2, N2[1][0] - C2 * N2[0][0])

    y = (B2[1] - B1[1]) / (B1[0] - B2[0])
    z = (C2[1] - C1[1]) / (C1[0] - C2[0])
    T.append(z - y)

    ax = fig.add_axes([0, 0.5, 1, 0.5])
    ax.plot(t, np.abs(E), 'r-', label='Messwerte')
    x = np.linspace(M1[0][0], lt.vals(y), 10000)
    ax.plot(x, lt.vals(B1[0] * x + B1[1]), 'b-', label='Regressionsgeraden')
    x = np.linspace(lt.vals(y), M2[0][-1], 10000)
    ax.plot(x, lt.vals(B2[0] * x + B2[1]), 'b-')
    ax.axvline(x=lt.vals(y), c='k', ls='--', label='Schnittpunkt')
    ax.plot(M[0], M[1], 'gx', label='Maxima')
    ax.xaxis.set_view_interval(t[0], t[-1], True)
    ax.yaxis.set_view_interval(0, s[6], True)
    ax.xaxis.set_major_formatter(plt.NullFormatter())
    ax.yaxis.set_major_formatter(lt.SiFormatter(2))
    ax.set_ylabel(r'\silabel{\abs{U_\t{E}}}{\volt}')
    handles, labels = ax.get_legend_handles_labels()
    ax.legend((handles[0], handles[3], handles[1], handles[2]), (labels[0], labels[3], labels[1], labels[2]), loc='upper right')

    ax2 = fig.add_axes([0, 0, 1, 0.5])
    ax3 = ax2.twinx()
    ax3.plot(t, np.abs(A), 'r-')
    x = np.linspace(N1[0][0], lt.vals(z), 10000)
    ax3.plot(x, lt.vals(C1[0] * x + C1[1]), 'b-')
    x = np.linspace(lt.vals(z), N2[0][-1], 10000)
    ax3.plot(x, lt.vals(C2[0] * x + C2[1]), 'b-')
    ax3.axvline(x=lt.vals(z), c='k', ls='--')
    ax3.plot(N[0], N[1], 'gx')
    ax2.xaxis.set_view_interval(t[0], t[-1], True)
    ax3.xaxis.set_view_interval(t[0], t[-1], True)
    ax2.yaxis.set_view_interval(0, s[7] * 1e-3, True)
    ax3.yaxis.set_view_interval(0, s[7] * 1e-3, True)
    ax2.yaxis.set_major_formatter(plt.NullFormatter())
    ax2.xaxis.set_major_formatter(lt.SiFormatter(1, factor=1e3))
    ax3.yaxis.set_major_formatter(lt.SiFormatter(0, factor=1e3))
    ax2.set_xlabel(r'\silabel{t}{\milli\second}')
    ax3.set_ylabel(r'\silabel{\abs{U_\t{A}}}{\milli\volt}')

    lt.savefig(fig, lt.new('graph/' + str(i) + '.tex'))

T[0] = unc.ufloat((T[0], np.inf))
T = np.array(T)
v_gr = 14 / T
t = lt.SymbolColumnTable()
t.add_si_column(r'\#', list(range(1, 12)), places=0)
t.add_si_column('T', T * 1e3, r'\milli\second')
t.add_si_column(r'v_\t{gr}', v_gr * 1e-6, r'\per\micro\second')
t.savetable(lt.new('table/T.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.errorbar(list(range(1, 12)), lt.vals(v_gr), lt.stds(v_gr), fmt='rx')
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter(1, factor=1e-6))
ax.xaxis.set_minor_locator(plt.MultipleLocator())
ax.set_xlabel('$n$')
ax.set_ylabel(r'\silabel{v_\t{gr}}{\per\micro\second}')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/v_gr.tex'))
