from labtools import *

L = np.loadtxt(lt.file('data/L'))
L *= 1e-3
C1 = np.loadtxt(lt.file('data/C1'))
C1 *= 1e-9
C2 = np.loadtxt(lt.file('data/C2'))
C2 *= 1e-9

np.savetxt(lt.new('result/L'), (L, C1, C2))

t = lt.SymbolRowTable()
t.add_si_row('L', L * 1e3, r'\milli\henry', places=3)
t.add_si_row('C_1', C1 * 1e9, r'\nano\farad', places=2)
t.add_si_row('C_2', C2 * 1e9, r'\nano\farad', places=2)
t.savetable(lt.new('table/L.tex'))
