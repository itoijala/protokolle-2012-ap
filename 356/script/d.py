from labtools import *

n, A_1, A_2, A_3, A_Z = np.loadtxt(lt.file('data/A'), unpack=True)
A_Z *= 1e-3

t = lt.SymbolColumnTable()
t.add_si_column('n', n, places=0)
t.add_si_column('A_1', A_1, r'\volt', places=3)
t.add_si_column('A_2', A_2, r'\volt', places=2)
t.add_si_column('A_Z', A_Z * 1e3, r'\milli\volt', places=0)
t.savetable(lt.new('table/A.tex'))

def cos(x, E, F, G, H):
    return E * np.abs(np.cos(F * x + G)) + H

B_1 = lt.curve_fit(cos, n, A_1, p0=(0.7, np.pi / 20, 0, 1))                 # lt.new('table/A_1.tex')
B_2 = lt.curve_fit(cos, n, A_2, p0=(30.7, np.pi / 7, 0, 1))               # lt.new('table/A_2.tex')
B_Z = lt.curve_fit(cos, n, A_Z, p0=(3.5e-3, np.pi / 14, np.pi / 2, 53e-3)) # lt.new('table/A_Z.tex')

for B, tab in zip((B_1, B_2, B_Z), ('1', '2', 'Z')):
    t = lt.SymbolRowTable()
    t.add_si_row('E_' + tab, B[0], r'\volt')
    t.add_si_row('F_' + tab, B[1])
    t.add_si_row('G_' + tab, B[2])
    t.add_si_row('H_' + tab, B[3], r'\volt')
    t.savetable(lt.new('table/A_' + tab + '.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(-0.2, 14.2, 10000)
ax.plot(x, cos(x, *lt.vals(B_1)), 'b-', label='Ausgleichskurve')
ax.plot(n, A_1, 'rx', label='Messwerte')
ax.xaxis.set_view_interval(-0.2, 14.2, True)
ax.yaxis.set_view_interval(-0.1, 1.9, True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter(1))
ax.xaxis.set_minor_locator(plt.MultipleLocator())
ax.set_xlabel(r'$n$')
ax.set_ylabel(r'\silabel{A_1}{\volt}')
lt.ax_reverse_legend(ax, 'lower left')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/A_1.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(-0.2, 14.2, 10000)
ax.plot(x, cos(x, *lt.vals(B_2)), 'b-', label='Ausgleichskurve')
ax.plot(n, A_2, 'rx', label='Messwerte')
ax.xaxis.set_view_interval(-0.2, 14.2, True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter(1))
ax.xaxis.set_minor_locator(plt.MultipleLocator())
ax.set_xlabel(r'$n$')
ax.set_ylabel(r'\silabel{A_2}{\volt}')
lt.ax_reverse_legend(ax, 'lower left')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/A_2.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(-0.2, 14.2, 10000)
ax.plot(x, cos(x, *lt.vals(B_Z)), 'b-', label='Ausgleichskurve')
ax.plot(n, A_Z, 'rx', label='Messwerte')
ax.xaxis.set_view_interval(-0.2, 14.2, True)
ax.yaxis.set_view_interval(48.5e-3, 57.5e-3, True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e3))
ax.xaxis.set_minor_locator(plt.MultipleLocator())
ax.set_xlabel(r'$n$')
ax.set_ylabel(r'\silabel{A_Z}{\milli\volt}')
lt.ax_reverse_legend(ax, 'upper right')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/A_Z.tex'))
