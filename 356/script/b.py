from labtools import *

L, C1, C2 = np.loadtxt(lt.file('result/L'))

nu_1 = np.loadtxt(lt.file('data/nu_1'), unpack=True)
nu_12 = np.loadtxt(lt.file('data/nu_12'), unpack=True)

def omega_1_theory(theta):
    return np.sqrt(2 / (L * C1) * (1 - np.cos(theta)))

def omega_12_theory(theta):
    return np.sqrt(1 / L * (1 / C1 + 1 / C2) - 1 / L * np.sqrt((1 / C1 + 1 / C2)**2 - 4 * np.sin(theta)**2 / C1 / C2))

theta_1 = np.arange(1, len(nu_1) + 1) * np.pi / 14
theta_12 = np.arange(1, len(nu_12) + 1) * np.pi / 14

omega_1 = nu_1 * 2 * np.pi
omega_12 = nu_12 * 2 * np.pi

np.savetxt(lt.new('result/omega_1'), (theta_1, omega_1))

t = lt.SymbolColumnTable()
t.add_si_column(r'\#', list(range(1, len(nu_1) + 1)), places=0)
t.add_si_column(r'\theta_1', np.arange(1, len(nu_1) + 1), r'$\dfrac{\PI}{14} \,$\radian', places=0)
t.add_si_column(r'\nu_1', nu_1, r'\hertz', places=0)
t.savetable(lt.new('table/nu_1.tex'))

t = lt.SymbolColumnTable()
t.add_si_column(r'\theta_{12}', np.arange(1, len(nu_12) + 1), r'$\dfrac{\PI}{14} \,$\radian', places=0)
t.add_si_column(r'\nu_{12}', nu_12, r'\hertz', places=0)
t.savetable(lt.new('table/nu_12.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(0, 2.8, 10000)
ax.plot(x, omega_1_theory(x), 'b-', label='Theoriekurve')
ax.plot(theta_1, omega_1, 'rx', label='Messwerte')
ax.xaxis.set_view_interval(0, 2.8, True)
ax.xaxis.set_major_formatter(lt.SiFormatter(1))
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e-3))
ax.set_xlabel(r'\silabel{\theta_1}{\radian}')
ax.set_ylabel(r'\silabel{\omega_1}{\kilo\hertz}')
lt.ax_reverse_legend(ax, 'lower right')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/dispersion_1.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(0, 1.6, 10000)
ax.plot(x, omega_12_theory(x), 'b-', label='Theoriekurve')
ax.plot(theta_12, omega_12, 'rx', label='Messwerte')
ax.xaxis.set_view_interval(0, 1.6, True)
ax.xaxis.set_major_formatter(lt.SiFormatter(1))
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e-3))
ax.set_xlabel(r'\silabel{\theta_{12}}{\radian}')
ax.set_ylabel(r'\silabel{\omega_{12}}{\kilo\hertz}')
lt.ax_reverse_legend(ax, 'lower right')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/dispersion_12.tex'))
