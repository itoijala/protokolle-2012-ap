from labtools import *

L, C, _ = np.loadtxt(lt.file('result/L'))

theta, omega = np.loadtxt(lt.file('result/omega_1'))

def v_ph_theory(omega):
    return omega / np.arccos(1 - 1 / 2 * L * C * omega**2)

v_ph = omega / theta

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(0, 4e5, 10000)
ax.plot(x, v_ph_theory(x), 'b-', label='Theoriekurve')
ax.plot(omega, v_ph, 'rx', label='Messwerte')
ax.yaxis.set_view_interval(14e4, 21e4, True)
ax.xaxis.set_major_formatter(lt.SiFormatter(factor=1e-3))
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e-3))
ax.set_xlabel(r'\silabel{\omega_1}{\kilo\hertz}')
ax.set_ylabel(r'\silabel{v_\t{ph}}{\per\milli\second}')
lt.ax_reverse_legend(ax, 'lower left')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/v_ph.tex'))
