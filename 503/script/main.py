from labtools import *

g, rho_L, rho_Öl, p, B, d = np.loadtxt(lt.file('data/constants'))

p *= 1e2
B *= 1e-5 * sp.constants.torr
d *= 1e-3

U, R, s_ab, t_ab, s_auf, t_auf, s_0, t_0 = np.loadtxt(lt.file('data/v'), unpack=True)

s_ab *= 0.5 * 1e-3
s_auf *= 0.5 * 1e-3
s_0 *= 0.5 * 1e-3

U_e = U
R_e = R

@np.vectorize
def R_T(R):
    if R <= 1.815:
        return 29
    elif R <= 1.857:
        return 28
    elif R <= 1-902:
        return 27
    else:
        return 26

T = R_T(R)

T_eta_L_dict = {26: 1.8525 * 1e-5,
                27: 1.8575 * 1e-5,
                28: 1.8625 * 1e-5,
                29: 1.8675 * 1e-5}

@np.vectorize
def T_eta_L(T):
    return T_eta_L_dict[T]

eta_L = T_eta_L(T)

v_ab = s_ab / t_ab
v_auf = s_auf / t_auf
v_0 = s_0 / t_0

#epsilon = 1
#tmp_U, tmp_R, tmp_ab, tmp_auf, tmp_0 = [], [], [], [], []
#for i in range(len(v_ab)):
#    if np.abs(v_ab[i] - v_auf[i] - 2 * v_0[i]) < epsilon:
#        tmp_U.append(U[i])
#        tmp_R.append(R[i])
#        tmp_ab.append(v_ab[i])
#        tmp_auf.append(v_auf[i])
#        tmp_0.append(v_0[i])
#
#U = np.array(tmp_U)
#R = np.array(tmp_R)
#v_ab = np.array(tmp_ab)
#v_auf = np.array(tmp_auf)
#v_0 = np.array(tmp_0)

r = 0.5 * (np.sqrt((18 * eta_L * (v_ab - v_auf)) / (g * (rho_Öl - rho_L)) + B**2 / p**2) - B / p)
q = 9 / 2 * np.pi * d * (v_ab + v_auf) / U * np.sqrt((v_ab - v_auf) / (g * (rho_Öl - rho_L))) * (eta_L / (1 + B / (p * r)))**1.5

t = lt.SymbolRowTable()
t.add_si_row("g", g, r"\meter\per\square\second", 3)
t.add_si_row(r"\rho_\t{L}", rho_L, r"\kilogram\per\meter\cubed", 2)
t.add_si_row(r"\rho_\t{Öl}", rho_Öl * 1e-2, r"\kilogram\per\meter\cubed", 3, exp="e2")
t.add_si_row("p", p * 1e-5, r"\hecto\pascal", 4, exp="e3")
t.add_si_row("B", B * 1e3, r"\milli\pascal\meter", 3)
t.add_si_row("d", d * 1e3, r"\milli\meter", 4)
open(lt.new('table/const.tex'), "w").write(t.render())

t = lt.SymbolColumnTable()
t.add(lt.SymbolColumn(r"R \le", [1.815, 1.857, 1.902, 1.950], r"\mega\ohm", 4))
t.add(lt.SymbolColumn("T", [29, 28, 27, 26], r"\degreeCelsius", 2))
t.add(lt.SymbolColumn(r"\eta_L", [1.8525, 1.8575, 1.8625, 1.8675], r"\newton\second\per\square\meter", 5, exp="e-5"))
open(lt.new('table/R.tex'), "w").write(t.render())

t = lt.SymbolColumnTable()
t.add(lt.SiColumn([[ str(v) for v in range(len(U_e))]], r"\#"))
t.add(lt.SymbolColumn("U", U_e, r"\volt", places=0))
t.add(lt.SymbolColumn("R", R_e, r"\mega\ohm", 3))
t.add(lt.SymbolColumn(r"s_\t{ab}", s_ab * 1e3, r"\milli\meter", 2))
t.add(lt.SymbolColumn(r"t_\t{ab}", t_ab, r"\second", places=3))
t.add(lt.SymbolColumn(r"s_\t{auf}", s_auf * 1e3, r"\milli\meter", 2))
t.add(lt.SymbolColumn(r"t_\t{auf}", t_auf, r"\second", places=3))
t.add(lt.SymbolColumn(r"s_0", s_0 * 1e3, r"\milli\meter", 2))
t.add(lt.SymbolColumn(r"t_0", t_0, r"\second", places=3))
open(lt.new('table/v.tex'), "w").write(t.render())

t = lt.SymbolColumnTable()
t.add(lt.SiColumn([[ str(v) for v in range(len(U_e))]], r"\#"))
t.add(lt.SymbolColumn("r", r * 1e6, r"\micro\meter", places=3))
t.add(lt.SymbolColumn("q", q * 1e18, r"\coulomb", exp="e-18", places=2))
open(lt.new('table/q.tex'), "w").write(t.render())

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(range(len(q)), sorted(list(lt.vals(q))), 'xb')
ax.xaxis.set_view_interval(-1, 27, ignore=True)
ax.xaxis.set_major_locator(plt.NullLocator())
ax.yaxis.set_major_formatter(lt.SiFormatter(1, factor=1e17))
ax.set_ylabel(r'\silabel{q}[e-17]{\coulomb}')
fig.savefig(lt.new('graph/q.tex'), bbox_inches='tight', pad_inches=0)

F = sp.constants.physical_constants['Faraday constant']
F = unc.ufloat((F[0], F[2]))
N_A = sp.constants.physical_constants['Avogadro constant']
N_A = unc.ufloat((N_A[0], N_A[2]))
e = sp.constants.physical_constants['elementary charge']
e = unc.ufloat((e[0], e[2]))

t = lt.SymbolRowTable()
t.add_si_row(r'F_\t{lit}', F * 1e-4, r'\coulomb\per\mole', exp="e4")
t.add_si_row(r'e_\t{lit}', e * 1e19, r'\coulomb', exp="e-19")
t.add_si_row(r'N_{\t{A},\t{lit}}', N_A * 1e-23, r'\per\mole', exp="e23")
t.savetable(lt.new('table/lit.tex'))
