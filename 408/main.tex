\input{header/report.tex}

\SetExperimentNumber{408}

\begin{document}
  \maketitlepage{Geometrische Optik}{29.05.2012}{05.06.2012}
  \section{Ziel}
    Ziel des Versuchs ist es, die Brennweiten verschiedener Linsen mittels verschiedener Methoden zu bestimmen.
  \section{Theorie}
    Linsen bestehen aus optisch dichterem Material als die Umgebung.
    Ein einfallender Lichtstrahl wird beim Ein- und Austritt in das Material gebrochen.
    Es gibt zwei verschiedene Arten von Linsen: Sammellinsen und Zerstreuungslinsen.
    Eine Sammellinse bündelt das einfallende Licht, die Bildweite $b$ und Brennweite $f$ sind positiv, siehe Abbildung \ref{fig:sammel}.
    Bei einer Zerstreuungslinse sind Bildweite und Brennweite negativ, siehe Abbildung \ref{fig:zerstreu}.
    \begin{figure}
      \includegraphics{graphic/sammel.pdf}
      \caption{Eine Sammellinse \cite{anleitung408}}
      \label{fig:sammel}
    \end{figure}
    \begin{figure}
      \includegraphics{graphic/zerstreu.pdf}
      \caption{Eine Zerstreuungslinse \cite{anleitung408}}
      \label{fig:zerstreu}
    \end{figure}

    Dünne Linsen könne dadurch genähert werden, dass nur eine Brechung in der Mitte der Linse betrachtet wird.
    Durch Betrachtung eines Parallelstrahls $P$, eines Mittelpunktstrahls $M$ und eines Brennpunktstrahls $B$ kann die Brennweite berechnet werden.
    Dabei ist $P$ vor der Linse parallel zur optischen Achse, $M$ geht durch den Mittelpunktder Linse und wird nicht gebrochen und $B$ geht durch den Brennpunkt vor der Linse und wird zu einem parallelen Strahl nach der Linse.
    Es ergibt sich das Abbildungsgesetz
    \begin{eqn}
      V = \frac{B}{G} = \frac{b}{g} ,
    \end{eqn}
    wobei $V$ der Abbildungsmaßstab und $B$ und $G$ die Größen des Bildes und des Gegenstands sind.
    Außerdem ergibt sich die Linsengleichung
    \begin{eqn}
      \frac{1}{f} = \frac{1}{b} + \frac{1}{g} . \eqlabel{eqn:linsengleichung}
    \end{eqn}

    Dicke Linsen können durch Einführung zweier Hauptebenen $H$ und $H'$ genähert werden, wobei $b$ und $g$ bis zu den Hauptebenen gemessen werden, siehe Abbildung \ref{fig:dick}.
    Mit diesen Definitionen gilt \eqref{eqn:linsengleichung} auch für dicke Linsen.
    \begin{figure}
      \includegraphics{graphic/dick.pdf}
      \caption{Eine dicke Linse \cite{anleitung408}}
      \label{fig:dick}
    \end{figure}

    Die obigen Näherungen gelten nur für achsennahe Strahlen.
    Achsenferne Strahlen werden von einer Linse stärker gebrochen, der Brennpunkt dieser Strahlen ist näher an der Linse.
    Dieser Effekt wird spärische Aberration genannt.
    Außerdem ist die Brennweite von der Wellenlänge des Lichts abhängig.
    Bei dieser chromatischen Aberration wird blaues Licht stärker gebrochen als rotes, der Brennpunkt des blauen Lichts liegt näher an der Linse.
  \section{Aufbau und Durchführung}
    Bei der Messung wird eine optische Bank verwendet, die alle optischen Elemente trägt und eine Skala zur Positionsbestimmung der Elemente bietet.
    Das Licht wird von einer Halogenlampe erzeugt.
    Als Gegenstand wird eine Blende mit einer L-förmigen Ansammlung von Löchern verwendet.
    \subsection{Bestimmung der Brennweite nach herkömmlicher Methode}
      Es werden die Brennweiten einer bekannten harten und einer unbekannten weichen, mit Wasser gefüllten, Linse bestimmt.
      Dazu wird die Position $G$ des Gegenstands gemessen und bei zehn Positionen $L$ der Linse die Position $s$ des Schirms bestimmt, bei der das Bild scharf ist.
      Die Messung wird für beide Linsen durchgeführt.
    \subsection{Bestimmung der Brennweite mit der Methode nach Bessel}
      Bei der Brennweitenbstimmung nach Bessel werden bei zehn Positionen $s$ des Schirms die Positionen $L_1$ und $L_2$ der Linse gemessen, an den das Bild scharf ist, siehe Abbildung \ref{fig:bessel}.
      Hierbei muss darauf geachtet werden, dass der Abstand $e$ des Gegenstands und des Schirms mindestens viermal so groß ist wie die Brennweite der Linse.
      Die Messung wird für fünf Positionen des Schirms mit rotem und blauem Licht wiederholt, wobei Farbfilter zum Erzeugen des farbigen Lichts verwendet werden.
      \begin{figure}
        \begin{tikzpicture}
          \node [outer sep=0pt,inner sep=0pt,anchor=south west] at (0,0) (xy) {\includegraphics{graphic/bessel.pdf}};
          \draw [white,fill] (0,2.6) rectangle (0.5,3.1);
        \end{tikzpicture}
        \caption{Aufbau nach Bessel \cite{anleitung408}}
        \label{fig:bessel}
      \end{figure}
    \subsection{Bestimmung der Brennweite mit der Methode nach Abbe}
      Bei der Methode nach Abbe wird die  Brennweite eines Linsensystems bestehend aus einer Zerstreuungs- und einer Sammellinse mit dem Abstand $a$ bestimmt.
      Die Abmessungen $c_\t{g}$ der langen Seite und $d_\t{g}$ der kurzen Seite des Gegenstands werden gemessen.
      Bei zehn Positionen $L$ des Linsensystems wird die Position $s$ des Schirms gemessen, bei der das Bild scharf ist, siehe Abbildung \ref{fig:abbe}.
      Außerdem werden jeweils die Abmessungen $c$ der langen Seite und $d$ der kurzen Seite des Bilds mit einem Maßband gemessen.
      \begin{figure}
        \begin{tikzpicture}
          \node [outer sep=0pt,inner sep=0pt,anchor=south west] at (0,0) (xy) {\includegraphics{graphic/abbe.pdf}};
          \draw [white,fill] (0.1,2.6) rectangle (0.6,3.1);
        \end{tikzpicture}
        \caption{Aufbau nach Abbe \cite{anleitung408}}
        \label{fig:abbe}
      \end{figure}
  \section{Messwerte}
    \subsection{Bestimmung der Brennweite nach herkömmlicher Methode}
      In Tabelle \ref{tab:s_k} sind die Positionen $L$ der Linse und $s$ des Schirms aufgeführt.
      Der Index $\t{k}$ steht für die Linse mit bekannter Brennweite, der Index $\t{w}$ für die unbekannte, mit Wasser gefüllte, Linse.
      Der Gegenstand befand sich für alle Methoden an der gleichen Stelle
      \begin{eqn}
        G = \input{result/G.tex} .
      \end{eqn}
      \begin{table}
        \input{table/s_k.tex}
        \caption{Messwerte der Positionen der Linse und des Schirms}
        \label{tab:s_k}
      \end{table}
      \FloatBarrier
    \subsection{Bestimmung der Brennweite mit der Methode nach Bessel}
      In Tabelle \ref{tab:bessel-data} stehen die nach der Bessel-Methode ermittelten Messwerte für weißes, rotes und blaues Licht, wobei $s$ die Position des Schirms und $L_i$ die $i$-te Position der Linse ist.
      Der Index $\t{r}$ steht für rotes, $\t{b}$ für blaues Licht.
      \begin{table}
        \input{table/bessel-data.tex}
        \caption{Messwerte der Bessel-Methode}
        \label{tab:bessel-data}
      \end{table}
      \FloatBarrier
    \subsection{Bestimmung der Brennweite mit der Methode nach Abbe}
      In Tabelle \ref{tab:abbe-data} sind die Position $L$ der Zerstreuungslinse, die Position $s$ des Schirms und die Abmessungen $c$ und $d$ des Bildes auf dem Schirm aufgeführt.
      In Tabelle \ref{tab:abbe-data-2} stehen weitere Messwerte für die lange Seite $c_\t{g}$ und die kurze Seite $d_\t{g}$ des Gegenstandes und der Abstand $a$ zwischen den Linsen.
      \begin{table}
        \input{table/abbe-data.tex}
        \caption{Messwerte der Abbe-Methode}
        \label{tab:abbe-data}
      \end{table}
      \begin{table}
        \input{table/abbe-data-2.tex}
        \caption{Weitere Messwerte der Abbe-Methode}
        \label{tab:abbe-data-2}
      \end{table}
  \section{Auswertung}
    \input{statistics.tex}
    \input{linregress.tex}
    \subsection{Bestimmung der Brennweite nach herkömmlicher Methode}
      Für die Gegenstandsweite gilt
      \begin{eqn}
        g = L - G
      \end{eqn}
      und für die Bildweite
      \begin{eqn}
        b = s - L .
      \end{eqn}
      Die Brennweite errechnet sich über
      \begin{eqns}
        \frac{1}{f_\t{r}} &=& \frac{1}{b} + \frac{1}{g}
        \itext{zu}
        f_\t{r} &=& \frac{1}{\cfrac{1}{b} + \cfrac{1}{g}} .
      \end{eqns}

      Alternativ kann die Brennweite über eine graphische Konstruktion berechnet werden.
      Hierbei wird für jedes $\g(b, g)$-Paar die Gerade
      \begin{eqn}
        y = - \frac{b}{g} x + b
      \end{eqn}
      in ein Diagramm gezeichnet.
      Alle Geraden sollten sich laut der Theorie in dem Punkt $\g(f, f)$ schneiden.
      Durch Berechnung des Mittelwertes der $x$- und $y$-Komponenten aller Schnittpunkte der Geraden kann $f$ berechnet werden.
      Für den $x$- und $y$-Wert des Schnittpunkts der $i$-ten und $j$-ten Geraden ergibt sich
      \begin{eqns}
        x &=& \frac{b_j - b_i}{\cfrac{b_j}{g_j} - \cfrac{b_i}{g_i}} \\
        y &=& - \frac{b_i}{g_i} x + b_i .
      \end{eqns}

      Die Mittels beider Verfahren berechneten Mittelwerte der Brennweite sind, zusammen mit der Herstellerangabe $f_\t{Herst}$ für die bekannte Linse, in Tabelle \ref{tab:f_k} aufgeführt.
      Die Geraden und der Mittelwert von $f$ sind in den Abbildungen \ref{fig:f_k} für die bekannte und \ref{fig:f_w} für die unbekannte Linse aufgetragen.
      Dabei wurden die Messwerte für $L = \SI{75}{cm}$ bei der bekannten Linse nicht mit eingerechnet, da sie große Abweichungen aufweisen (gelbe Geraden in Abbildung \ref{fig:f_k}).
      \begin{table}
        \input{table/f_k.tex}
        \caption{Berechnete Brennweiten, sowie Herstellerangabe zur Brennweite der Sammellinse}
        \label{tab:f_k}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/f_k.pdf}
        \caption{Ermittlung der Brennweite für die Linse mit bekannter Brennweite}
        \label{fig:f_k}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/f_w.pdf}
        \caption{Ermittlung der Brennweite für die Linse mit unbekannter Brennweite}
        \label{fig:f_w}
      \end{figure}
      \FloatBarrier
    \subsection{Bestimmung der Brennweite mit der Methode nach Bessel}
      Für die Abstände des Gegenstands und des Schirms $e$ und der beiden Linsenpositionen $d$ gilt
      \begin{eqns}
        e &=& s - G \\
        d &=& L_2 - L_1 .
      \end{eqns}
      Laut Theorie gilt für die Brennweite
      \begin{eqn}
        f = \frac{e^2 - d^2}{4 e} .
      \end{eqn}
      Die berechneten $e$- und $d$-Werte stehen in Tabelle \ref{tab:e_d}.
      Die berechneten Mittelwerte der Brennweite für weißes, rotes und blaues Licht stehen in Tabelle \ref{tab:bessel-f}.
      \begin{table}
        \input{table/e_d.tex}
        \caption{Berechnete $e$- und $d$-Werte für weißes, rotes und blaues Licht}
        \label{tab:e_d}
      \end{table}
      \begin{table}
        \input{table/bessel-f.tex}
        \caption{Brennweiten aus der Methode nach Bessel für weißes, rotes und blaues Licht}
        \label{tab:bessel-f}
      \end{table}
      \FloatBarrier
    \subsection{Bestimmung der Brennweite mit der Methode nach Abbe}
      Für die Abstände $g'$ zwischen dem Gegenstand und der linken Seite $A$ des Linsenreiters und $b'$ zwischen dem Schirm und $A$ gilt
      \begin{eqns}
        A &=& L - \frac{a}{2} \\
        g' &=& A - G \\
        b' &=& s - A .
      \end{eqns}
      Für den Abbildungsmaßstab gilt
      \begin{eqn}
        V = \frac{c}{c_\t{g}} = \frac{d}{d_\t{g}} ,
      \end{eqn}
      wobei mit dem Mittelwert der Ergebnisse der langen und kurzen Seite gerechnet wird.
      Die Brennweite und die Positionen $h$ und $h'$ der Halbebenen können durch lineare Regressionen aus
      \begin{eqns}
        g' &=& f_g \g(1 + V^{\, -1}) + h \eqlabel{eqn:regress-g} \\
        b' &=& f_b \g(1 + V) + h' \eqlabel{eqn:regress-b}
      \end{eqns}
      berechnet werden.
      Die Messwerte für die lineare Regression sowie die Regressionsgeraden sind in den Abbildungen \ref{fig:g} und \ref{fig:b} aufgetragen.
      Die Ergebnisse der linearen Regression stehen, zusammen mit den Herstellerangaben über die verwendeten Linsen, in Tabelle \ref{tab:abbe-f}.
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/g.pdf}
        \caption{Lineare Regression nach \eqref{eqn:regress-g}}
        \label{fig:g}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/b.pdf}
        \caption{Lineare Regression nach \eqref{eqn:regress-b}}
        \label{fig:b}
      \end{figure}
      \begin{table}
        \input{table/abbe-f.tex}
        \caption{Berechnete Brennweiten und Halbebenen, Herstellerangaben}
        \label{tab:abbe-f}
      \end{table}
  \section{Diskussion}
    \subsection{Bestimmung der Brennweite nach herkömmlicher Methode}
      Der Fehler der graphisch berechneten Brennweite ist größer als der des mit der Linsengleichung \eqref{eqn:linsengleichung} berechneten, die dadurch bestätigt wird.
      Die Angabe über die Brennweite der Sammellinse des Herstellers konnte im Rahmen der Messgenauigkeit verifiziert werden.
      Es war schwierig, genau zu beurteilen, wo das Bild scharf war.
      Dies führt zu einer gewissen Ungeneuigkeit der Messwerte in einem Rahmen von \SI{0.5}{\centi\meter}.
    \subsection{Bestimmung der Brennweite mit der Methode nach Bessel}
      Die nach der Bessel-Methode bestimmte Brennweite stimmt mit der Herstellerangabe überein, allerdings mit einem relativen Fehler von $\SI{10}{\percent}$.
      Für rotes und blaues Licht wird die chromatische Aberration sichtbar, dabei liegt der Brenpunkt vom blauen Licht näher an der Linse als der vom roten Licht; dies ist auf Dispersionseffekte zurückzuführen, die Messwerte bestätigen also die Vorhersage der Theorie.
    \subsection{Bestimmung der Brennweite mit der Methode nach Abbe}
      Die linearen Regressionsgeraden stimmen gut mit den Messwerten überein.
      Bei den zwei Ausgleichsrechnungen kamen allerdings unterschiedliche Werte für die Brennweite des Gesamtlinsensystems raus.
      Das Problem mit der Beurteilung, ob das Bild scharf ist, bestand auch hier.
      Die Halbebenen liegen ungefähr symmetrisch um den Referenzpunkt $A$, wobei die rechte Halbebene weiter entfernt ist.
      Die statistischen Fehler sind klein und ausgehend von den Größenordnungen, kommt für die Gesamtbrennweite ein sinnvoller Wert raus.
  \makebibliography
\end{document}
