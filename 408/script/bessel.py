from labtools import *

G = np.loadtxt(lt.file('result/G'))

s, L_1, L_2 = np.loadtxt(lt.file('data/bessel'), unpack=True)
s *= 1e-2
L_1 *= 1e-2
L_2 *= 1e-2
s_ca, L_r_1, L_r_2, L_b_1, L_b_2 = np.loadtxt(lt.file('data/chrom_aberration'), unpack=True)
s_ca *= 1e-2
L_r_1 *= 1e-2
L_r_2 *= 1e-2
L_b_1 *= 1e-2
L_b_2 *= 1e-2

e = s - G
e_ca = s_ca - G
d = L_2 - L_1
d_r = L_r_2 - L_r_1
d_b = L_b_2 - L_b_1

f = lt.mean((e**2 - d**2) / 4 / e)
f_r = lt.mean((e_ca**2 - d_r**2) / 4 / e_ca)
f_b = lt.mean((e_ca**2 - d_b**2) / 4 / e_ca)

t = lt.SymbolColumnTable()
t.add_si_column('s', s * 1e2,r'\centi\meter', places=1)
t.add_si_column('L_1', L_1 * 1e2, r'\centi\meter', places=1)
t.add_si_column('L_2', L_2 * 1e2, r'\centi\meter', places=1)
t.add_si_column(r'L_{\t{r}, 1}', L_r_1 * 1e2, r'\centi\meter', places=1)
t.add_si_column(r'L_{\t{r}, 2}', L_r_2 * 1e2, r'\centi\meter', places=1)
t.add_si_column(r'L_{\t{b}, 1}', L_b_1 * 1e2, r'\centi\meter', places=1)
t.add_si_column(r'L_{\t{b}, 2}', L_b_2 * 1e2, r'\centi\meter', places=1)
t.savetable(lt.new('table/bessel-data.tex'))

t = lt.SymbolColumnTable()
t.add_si_column('e', e * 1e2, r'\centi\meter', places=1)
t.add_si_column('d', d * 1e2, r'\centi\meter', places=1)
t.add_si_column(r'd_\t{r}', d_r * 1e2, r'\centi\meter', places=1)
t.add_si_column(r'd_\t{b}', d_b * 1e2, r'\centi\meter', places=1)
t.savetable(lt.new('table/e_d.tex'))

t = lt.SymbolRowTable()
t.add_si_row(r'f', f * 1e3, r'\milli\meter')
t.add_si_row(r'f_\t{r}', f_r * 1e3, r'\milli\meter')
t.add_si_row(r'f_\t{b}', f_b * 1e3, r'\milli\meter')
t.savetable(lt.new('table/bessel-f.tex'))
