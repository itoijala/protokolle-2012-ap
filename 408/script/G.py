from labtools import *

G = np.loadtxt(lt.file('data/G'), unpack=True)
G *= 1e-2

np.savetxt(lt.new('result/G'), (G,))
open(lt.new('result/G.tex'), 'w').write(r'\SI{' + lt.round_places(float(G * 1e2), 1) + r'}{\centi\meter}')
