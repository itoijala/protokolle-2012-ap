from labtools import *

G = np.loadtxt(lt.file('result/G'))

L, s_k, s_w = np.loadtxt(lt.file('data/s_k'), unpack=True)
L *= 1e-2
s_k *= 1e-2
s_w *= 1e-2
f_herst = np.loadtxt(lt.file('data/f_sammel'))
f_herst *= 1e-3

g = L - G
b_k = s_k - L
b_w = s_w - L

mask = 5

f_k = []
f_w = []
for i in range(len(g) - 1):
    for j in range(i + 1, len(g)):
        if i != 5 and j != 5:
            x = (b_k[j] - b_k[i]) / ((-b_k[i] / g[i]) + (b_k[j] / g[j]))
            f_k.append(x)
            f_k.append(-b_k[i] / g[i] * x + b_k[i])
        x = (b_w[j] - b_w[i]) / ((-b_w[i] / g[i]) + (b_w[j] / g[j]))
        f_w.append(x)
        f_w.append(-b_w[i] / g[i] * x + b_w[i])
f_k = np.array(f_k)
f_w = np.array(f_w)

f_k_mean = lt.mean(f_k)
f_w_mean = lt.mean(f_w)

f_k_r = []
f_w_r = []
for i in range(len(g) - 1):
    if i != 5:
        f_k_r.append((b_k[i]**-1 + g[i]**-1)**-1)
    f_w_r.append((b_w[i]**-1 + g[i]**-1)**-1)
f_k_r = np.array(f_k_r)
f_w_r = np.array(f_w_r)

f_k_r_mean = lt.mean(f_k_r)
f_w_r_mean = lt.mean(f_w_r)

t = lt.SymbolColumnTable()
t.add_si_column('L', L * 1e2, r'\centi\meter', places=1)
t.add_si_column(r's_\t{k}', s_k * 1e2, r'\centi\meter', places=1)
t.add_si_column(r's_\t{w}', s_w * 1e2, r'\centi\meter', places=1)
t.savetable(lt.new('table/s_k.tex'))

t = lt.SymbolRowTable()
t.add_si_row(r'f_\t{k}', f_k_mean * 1e3, r'\milli\meter', places=0)
t.add_si_row(r'f_{\t{k}, \t{r}}', f_k_r_mean * 1e3, r'\milli\meter', places=0)
t.add_si_row(r'f_\t{Herst}', f_herst * 1e3, r'\milli\meter', places=0)
t.add_hrule()
t.add_si_row(r'f_\t{w}', f_w_mean * 1e3, r'\milli\meter', places=0)
t.add_si_row(r'f_{\t{w}, \t{theor}}', f_w_r_mean * 1e3, r'\milli\meter', places=0)
t.savetable(lt.new('table/f_k.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
for i in range(len(g)):
    x = np.linspace(0, g[i], 1000)
    ax.plot(x, -b_k[i] / g[i] * x + b_k[i])
ax.errorbar(lt.vals(f_k_mean), lt.vals(f_k_mean), xerr=lt.stds(f_k_mean), yerr=lt.stds(f_k_mean), fmt=None)
ax.yaxis.set_view_interval(0, 0.3, True)
ax.xaxis.set_view_interval(0, 0.6, True)
ax.xaxis.set_major_formatter(lt.SiFormatter(factor=1e2))
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e2))
ax.set_xlabel(r'\silabel{b_\t{k}}{\centi\meter}')
ax.set_ylabel(r'\silabel{g}{\centi\meter}')

ax2 = fig.add_axes([0.4, 0.4, 0.5, 0.5])
for i in range(len(g)):
    x = np.linspace(0, g[i], 1000)
    ax2.plot(x, - b_k[i] / g[i] * x + b_k[i])
ax2.errorbar(lt.vals(f_k_mean), lt.vals(f_k_mean), xerr=lt.stds(f_k_mean), yerr=lt.stds(f_k_mean), fmt=None)
ax2.yaxis.set_view_interval(0.07, 0.115, True)
ax2.xaxis.set_view_interval(0.03, 0.155, True)
ax2.xaxis.set_major_formatter(lt.SiFormatter(factor=1e2))
ax2.yaxis.set_major_formatter(lt.SiFormatter(1, factor=1e2))
fig.tight_layout()
lt.savefig(fig, lt.new('graph/f_k.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
for i in range(len(g)):
    x = np.linspace(0, g[i], 1000)
    ax.plot(x, -b_w[i] / g[i] * x + b_w[i])
ax.errorbar(lt.vals(f_w_mean), lt.vals(f_w_mean), xerr=lt.stds(f_w_mean),yerr=lt.stds(f_w_mean), fmt=None)
ax.yaxis.set_view_interval(0, 0.15, True)
ax.xaxis.set_major_formatter(lt.SiFormatter(factor=1e2))
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e2))
ax.set_xlabel(r'\silabel{b_w}{\centi\meter}')
ax.set_ylabel(r'\silabel{g}{\centi\meter}')

ax2 = fig.add_axes([0.45, 0.4, 0.45, 0.5])
for i in range(len(g)):
    x = np.linspace(0, g[i], 1000)
    ax2.plot(x, - b_w[i] / g[i] * x + b_w[i])
ax2.errorbar(lt.vals(f_w_mean), lt.vals(f_w_mean), xerr=lt.stds(f_w_mean), yerr=lt.stds(f_w_mean), fmt=None)
ax2.yaxis.set_view_interval(0.05, 0.08, True)
ax2.xaxis.set_view_interval(0, 0.085, True)
ax2.xaxis.set_major_formatter(lt.SiFormatter(factor=1e2))
ax2.yaxis.set_major_formatter(lt.SiFormatter(1, factor=1e2))
fig.tight_layout()
lt.savefig(fig, lt.new('graph/f_w.tex'))
