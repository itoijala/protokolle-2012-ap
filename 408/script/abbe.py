from labtools import *

G = np.loadtxt(lt.file('result/G'))

a = np.loadtxt(lt.file('data/a'))
a *= 1e-2
c_g, d_g = np.loadtxt(lt.file('data/c_g'))
c_g *= 1e-2
d_g *= 1e-2
L, s, c, d = np.loadtxt(lt.file('data/abbe'), unpack=True)
L *= 1e-2
s *= 1e-2
c *= 1e-2
d *= 1e-2
fs_herst = np.loadtxt(lt.file('data/f_sammel'))
fz_herst = np.loadtxt(lt.file('data/f_zerstreu'))

V = lt.mean(np.array([c / c_g, d / d_g]))

A = L - a / 2
b_tick = s - A
g_tick = A - G

f_g, h = lt.linregress(1 + V**-1, g_tick)
f_b, h_tick = lt.linregress(1 + V, b_tick)

t = lt.SymbolColumnTable()
t.add_si_column('L', L * 1e2, r'\centi\meter', places=1)
t.add_si_column('s', s * 1e2, r'\centi\meter', places=1)
t.add_si_column('c', c * 1e2, r'\centi\meter', places=1)
t.add_si_column('d', d * 1e2, r'\centi\meter', places=1)
t.savetable(lt.new('table/abbe-data.tex'))

t = lt.SymbolRowTable()
t.add_si_row(r'c_\t{g}', c_g * 1e2, r'\centi\meter', places=1)
t.add_si_row(r'd_\t{g}', d_g * 1e2, r'\centi\meter', places=1)
t.add_si_row(r'a', a * 1e2, r'\centi\meter', places=1)
t.savetable(lt.new('table/abbe-data-2.tex'))

t = lt.SymbolRowTable()
t.add_si_row('h', h * 1e2, r'\centi\meter')
t.add_si_row(r'f_g', f_g * 1e3, r'\milli\meter')
t.add_hrule()
t.add_si_row("h'", h_tick * 1e2, r'\centi\meter')
t.add_si_row(r'f_b', f_b * 1e3, r'\milli\meter')
t.add_hrule()
t.add_si_row(r'f_{\t{Sammel}, \t{Herst}}', fs_herst, r'\milli\meter')
t.add_si_row(r'f_{\t{Zerstreu}, \t{Herst}}', fz_herst, r'\milli\meter')
t.savetable(lt.new('table/abbe-f.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(1.5, 5, 10000)
ax.plot(x, lt.vals(f_g * x + h), 'b-', label='Regressionsgerade')
ax.plot(lt.vals(1 + V**-1), g_tick, 'rx', label='Messwerte')
ax.yaxis.set_view_interval(0.1, 0.6, True)
ax.xaxis.set_major_formatter(lt.SiFormatter(2))
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e2))
ax.set_xlabel(r'\silabel{1 + V^{\, -1}}{}')
ax.set_ylabel(r"\silabel{g'}{\centi\meter}")
lt.ax_reverse_legend(ax, 'lower right')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/g.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(1.2, 2.8, 10000)
ax.plot(x, lt.vals(f_b * x + h_tick), 'b-', label='Regressionsgerade')
ax.plot(lt.vals(1 + V), b_tick, 'rx', label='Messwerte')
ax.xaxis.set_view_interval(1.2, 2.8, True)
ax.yaxis.set_view_interval(0.35, 0.7, True)
ax.xaxis.set_major_formatter(lt.SiFormatter(2))
ax.yaxis.set_major_formatter(lt.SiFormatter(factor=1e2))
ax.set_xlabel(r'\silabel{1 + V}{}')
ax.set_ylabel(r"\silabel{b'}{\centi\meter}")
lt.ax_reverse_legend(ax, 'lower right')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/b.tex'))
