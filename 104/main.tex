\input{header/report.tex}

\SetExperimentNumber{104}

\begin{document}
  \maketitlepage{Der Doppler-Effekt}{24.04.2012}{08.05.2012}
  \section{Ziel}
    Ziel des Versuchs ist es, die Theorie des Doppler-Effekts experimentell zu bestätigen, sowie die Schallgeschwindigkeit in Luft bei Raumtemperatur zu ermitteln.
  \section{Theorie}
    Der Doppler-Effekt tritt als Frequenzänderung eines wellenartigen Signals dann auf, wenn sich Sender und Empfänger relativ zueinander bewegen.
    In diesem Versuch wird der Doppler-Effekt bei Schallwellen untersucht, die an die Luft als Medium gebunden sind.
    Aus diesem Grund muss zwischen Bewegungen des Senders und des Empfängers unterschieden werden.

    Bewegt sich der Empfänger mit der Geschwindigkeit $v$ auf den ruhenden Sender zu, misst er eine höhere Frequenz $\nu_\t{E}$, als die vom Sender ausgestrahlte Ruhefrequenz $\nu_0$.
    Der bewegte Beobachter beobachtet mehr Schwingungen pro Zeiteinheit $\Del{t}$ als ein ruhender Beobachter.
    Hieraus folgt für die beobachtete Frequenz
    \begin{eqn}
      \nu_\t{E} = \nu_0 + \frac{v}{\lambda} ,
    \end{eqn}
    wobei $\lambda$ die ausgestrahlte Wellenlänge ist.
    Mit
    \begin{eqn}
      c = \nu_0 \lambda
    \end{eqn}
    ergibt sich die Formel
    \begin{eqn}
      \nu_\t{E} = \nu_0 \g(1 + \frac{v}{c}) ,\eqlabel{eqn:nu_e}
    \end{eqn}
    die auch für einen Beobachter gilt, der sich vom Sender entfernt.

    Bei bewegtem Sender und ruhendem Empfänger misst der Beobachter die Frequenz
    \begin{eqn}
      \nu_\t{Q} = \nu_0 \frac{1}{1 - \frac{v}{c}} . \eqlabel{eqn:nu_q}
    \end{eqn}
    Aus der Reihendarstellung
    \begin{eqn}
      \nu_\t{Q} = \nu_0 \sum_{i = 0}^\infty \g(\frac{v}{c})^{\! i}
    \end{eqn}
    folgt $\nu_\t{Q} > \nu_\t{E} > \nu_0$ für $v > 0$.
  \section{Aufbau und Durchführung}
    \subsection{Messung der Relativgeschwindigkeit}
      Der Sender der Schallwelle ist auf einem per Synchrotronmotor betriebenen Wagen montiert.
      Um seine Geschwindigkeit zu messen, werden zwei Lichtschranken mit der Entfernung $s$ verwendet.
      Eine Uhr, die von den Lichtschranken über eine elektronische Schaltung gesteuert wird, misst die Zeit $t$, die der Wagen für den Weg $s$ benötigt.
      Die Strecke $s$ wird mit einem Maßband gemessen.
      Für die Geschwindigkeit gilt
      \begin{eqn}
        v = \frac{s}{t} . \eqlabel{eqn:speed}
      \end{eqn}
      Da der Motor ein Getriebe mit mehreren Gänge besitzt, wird die Messung für alle später verwendeten Gänge wiederholt.

      Die Schaltung (Abbildung \ref{fig:schaltung1}) schaltet die Uhr ein, wenn der Wagen die erste Lichtschranke passiert, und nach der zweiten Lichtschranke wieder aus.
      Die Lichtschranken sind durch Schmitt-Trigger an den Rest der Schaltung angebunden, um das analoge Signal des Lichtsensors in ein digitales zu verwandeln.
      Die Zeitbasis produziert jede Mikrosekunde genaue Impulse, die vom Untersetzer in Millisekunden verwandelt werden.
      Der Zustand des Flip-Flops entspricht dem der Uhr; wird der Flip-Flop durch die erste Lichtschranke aktiviert, zählt der Zähler die Millisekundenimpulse des Untersetzers, da das Und-Element die Signale des Untersetzers zum Ausgang und damit zur Uhr durchlässt.
      Das Signal der zweiten Lichtschranke setzt den Flip-Flop zurück und hält damit den Zähler an.
      \begin{figure}
        \input{graphic/schaltung1.tex}
        \caption{Schaltung zur Steuerung einer Uhr zur Bestimmung der Wagengeschwindigkeit}
        \label{fig:schaltung1}
      \end{figure}
    \subsection{Messung der Schallgeschwindigkeit}
      Der Aufbau zur Bestimmung der Schallgeschwindigkeit (Abbildung \ref{fig:aufbau2}) besteht aus einem Lautsprecher, der durch eine Mikrometerschraube positioniert werden kann, und einem gegenüberstehenden Mikrophon.
      Lautsprecher und Mikrophon sind an den $x$- und $y$-Kanälen eines Oszilloskops angeschlossen.
      Die Schallgeschwindigkeit $c$ wird aus der Wellelänge über die Beziehung
      \begin{eqn}
        c = \lambda \nu_0
      \end{eqn}
      ermittelt.
      Um die Wellenlänge $\lambda$ zu ermitteln, wird der Lautsprecher von einem Ende der Skala zum anderen verschoben und dabei die Orte markiert, an denen die Lissajousfigur auf dem Oszilloskop zu einer Gerade wird.
      Der Abstand $d$ dieser Orte ist jeweils die halbe Wellenlänge der Schallwelle.
      Die Frequenz $\nu_0$ wird im nächsten Versuchsteil gemessen.
      \begin{figure}
        \includegraphics[width=\textwidth]{graphic/aufbau2.pdf}
        \caption{Aufbau zur Bestimmung der Schallgeschwindigkeit \cite{anleitung104}}
        \label{fig:aufbau2}
      \end{figure}
    \subsection{Messung der Frequenz}
      Zur Messung der Frequenz wird die Schaltung aus Abbildung \ref{fig:schaltung2} in Verbindung mit dem motorbetriebenen Wagen und einer Lichtschranke verwendet.
      Ein Impuls der Lichtschranke, also das Passieren des Wagens, startet die Uhr, die eine Sekunde lang die vom Mikrophon registrieten Schwingungen zählt.
      Dazu wird ein Flip-Flop durch die Lichtschranke aktiviert, die wiederum den Untersetzer aktiviert.
      Dieser erhält von der Zeitbasis eine Million Impulse und produziert dann einen Impuls, der den Flip-Flop wieder deaktiviert.
      Ist der Flip-Flop aktiv, so werden die Impulse des Mikrophons vom Zähler registriert.
      \begin{figure}
        \input{graphic/schaltung2.tex}
        \caption{Schaltung zur Messung der Frequenz}
        \label{fig:schaltung2}
      \end{figure}

      Der Lautsprecher wird auf den Wagen montiert.
      Als erstes wird die Frequenz $\nu_0$ bei stehendem Wagen zur Bestimmung der Schallgeschwindigkeit gemessen.
      Dazu kann die Lichtschranke manuell ausgelöst werden.
      Die vom Mikrophon empfangene Frequenz $\nu$ wird bei jedem Gang und vorwärts sowie rückwärtsfahrendem Wagen ermittelt.
      Für die letzte Messung mittels einer Schwebung wird der Lautsprecher neben dem Mikrophon platziert und eine Reflektorplatte auf den Wagen montiert.
      Zwischen dem vom Lautsprecher gesendetem und dem reflektierten Signal kommt es zu einer Schwebung, die vom Mikrophon registriert wird.
      Die Anzeige des Zählers ist der Frequenzunterschied $\Del{\nu}$.
  \section{Messwerte und Auswertung}
    \input{statistics.tex}
    \input{linregress.tex}
    \subsection{Messung der Relativgeschwindigkeit}
      Die Werte der mehrmals gemessenen Strecke $s$ zwischen den Lichtschranken stehen in Tabelle \ref{tab:s}; dort steht ebenfalls der errechnete Mittelwert.
      \begin{table}
        \input{table/s.tex}
        \caption{Messwerte der Fahrstrecke}
        \label{tab:s}
      \end{table}
      Die mit der digitalen Schaltung ermittelten Zeiten sind in Tabelle \ref{tab:t} aufgeführt.
      Bei $t_\t{v}$ handelt es sich um die Zeit bei vorwärtsbewegtem Wagen, bei $t_\t{z}$ um die beim rückwärtsbewegten.
      Bewegt sich der Wagen vorwärts, nähert er sich der Gegenstelle.
      \begin{table}
        \input{table/t.tex}
        \caption{Messwerte für die Zeiten, abhängig vom eingestellten Gang}
        \label{tab:t}
      \end{table}
      Aus Tabelle \ref{tab:s} und \ref{tab:t} errechnen sich nach Formel \eqref{eqn:speed} die in Tabelle \ref{tab:v} stehenden Geschwindigkeiten des Wagens bei den verschiedenen Gängen.
      Außerdem stehen in Tabelle \ref{tab:v} die Mittelwerte der gemessenen Zeiten.
      Die Indizes verhalten sich analog zu Tabelle \ref{tab:t}.
      Für die Messungen, bei denen der Wagen zurückfährt, $s$ also negativ ist, ändert sich das Vorzeichen der Geschwindigkeit. 
      \begin{table}
        \OverfullCenter{\input{table/v.tex}}
        \caption{Mittelwerte der Zeiten und sich ergebende Geschwindigkeiten}
        \label{tab:v}
      \end{table}
      \FloatBarrier
    \subsection{Messung der Schallgeschwindigkeit}
      Die mit dem Präzissionsschlitten gemessenen Abstände $d$ sind in Tabelle \ref{tab:lambda} aufgeführt; dort stehen ebenfalls die errechneten Wellenlängen $\lambda$.
      \begin{table}
        \input{table/lambda.tex}
        \caption{Gemessene Strecke mit Präzisionsschlitten, sowie sich ergebende Wellenlänge}
        \label{tab:lambda}
      \end{table}
      In Tabelle \ref{tab:nu_0} stehen die Messwerte der Grundfrequenz $\nu_0$.
      \begin{table}
        \input{table/nu_0.tex}
        \caption{Messwerte der Frequenz bei ruhendem Wagen}
        \label{tab:nu_0}
      \end{table}
      Tabelle \ref{tab:c} enthält die errechnete Schallgeschwindigkeit, sowie den Mittelwert der reziproken Wellenlänge. 
      \begin{table}
        \input{table/c.tex}
        \caption{Errechnete Schallgeschwindigkeit, Literaturwert \cite{scipy}, sowie reziproke Wellenlänge}
        \label{tab:c}
      \end{table}
      
      In Tabelle \ref{tab:nu_qe} steht der Mittelwert der Maximalgeschwindigkeiten \mdash{} es wurde zu dessen Ermittlung jeweils der Betrag der jeweiligen Maximalgeschwindigkeiten genommen \mdash{} und die nach den Formeln \eqref{eqn:nu_e} und \eqref{eqn:nu_q} errechneten Größen $\nu_\t{Q}$ und $\nu_\t{E}$.
      Aus Tabelle \ref{tab:nu_qe} geht hervor, dass kein Unterschied zwischen $\nu_\t{Q}$ und $\nu_\t{E}$ gemacht werden kann, da die erste sich unterscheidende Nachkommastelle im Fehlerbereich liegt.
      \begin{table}
        \input{table/nu_qe.tex}
        \caption{Berechnete Werte für $v_\t{max}$, $\nu_\t{Q}$ und $\nu_\t{E}$}
        \label{tab:nu_qe}
      \end{table}
    \subsection{Messung der Frequenz nach herkömmlicher Methode}
      Die nach der ersten Methode gemessenen Frequenzen stehen in Tabelle \ref{tab:nu}.
      \begin{table}
        \input{table/nu.tex}
        \caption{Gemessene Frequenzen mit der herkömmlichen Methode für einzelne Gänge}
        \label{tab:nu}
      \end{table}
      Damit diese Messung weiter ausgewertet werden kann, muss die Grundfrequenz $\nu_0$ von den gemessenen Werten abgezogen werden; die sich ergebenen Werte stehen mit den Mittelwerten der Messwerte in Tabelle \ref{tab:Delta_nu_norm}.
      \begin{table}
        \OverfullCenter{\input{table/Delta_nu_norm.tex}}
        \caption{Mittelwerte der Frequenzen, sowie sich ergebende Frequenzdifferenzen}
        \label{tab:Delta_nu_norm}
      \end{table}
      Die geplotteten Messwerte aus dieser Tabelle und die anhand linearer Regression ermittelte Regressionsgerade ist in Abbildung \ref{fig:reg_norm_pos} für positive Geschwindigkeiten und in Abbildung \ref{fig:reg_norm_neg} für negative Geschwindigkeiten geplottet. 
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/norm_pos.pdf}
        \caption{Messwerte und Regressionsgerade für die positiven Geschwindigkeiten bei der herkömmlichen Methode}
        \label{fig:reg_norm_pos}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/norm_neg.pdf}
        \caption{Messwerte und Regressionsgerade für die negativen Geschwindigkeiten bei der herkömmlichen Methode}
        \label{fig:reg_norm_neg}
      \end{figure}
      Die ermittelten Fitparameter ($A$ ist die Steigung, $B$ der $y$-Achsenabschnitt) stehen in Tabelle \ref{tab:param_reg_norm}.
      \begin{table}
        \input{table/norm_param.tex}
        \caption{Parameter aus der linearen Regressionsrechnung der herkömmlich bestimmten Frequenzdifferenzen}
        \label{tab:param_reg_norm}
      \end{table}
      \FloatBarrier
    \subsection{Messung von Frequenzdifferenzen anhand der Schwebungsmethode}
      Die Frequenzdifferenzen der Schwebungsmethode stehen in Tabelle \ref{tab:deltanu_schweb}.
      Die Differenz bei zurückfahrendem Wagen ist negativ, da die Geschwindigkeit negativ ist.
      \begin{table}
        \input{table/Delta_nu.tex}
        \caption{Frequenzdifferenzen der Schwebungsmethode, sowie deren Mittelwerte in Abhängigkeit gewählten Gang}
        \label{tab:deltanu_schweb}
      \end{table}
      Die Messwerte für die Schwebungsmethode sind für positive und negative Geschwindigkeiten in Abbildung \ref{fig:reg_schweb_pos} und \ref{fig:reg_schweb_neg} geplottet; dort befinden sich ebenfalls die Regressionsgeraden.
      Hier wird die doppelte Geschwindigkeit verwendet, da die Reflektorplatte als Sender und Empfänger gleichzeitig arbeitet.
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/schwebe_pos.pdf}
        \caption{Messwerte und Regressionsgerade für die positiven Geschwindigkeiten bei der Schwebungsmethode}
        \label{fig:reg_schweb_pos}
      \end{figure}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/schwebe_neg.pdf}
        \caption{Messwerte und Regressionsgerade für die negativen Geschwindigkeiten bei der Schwebungsmethode}
        \label{fig:reg_schweb_neg}
      \end{figure}
      Die durch die lineare Regression ermittelten Fitparameter stehen in Tabelle \ref{tab:param_reg_schweb}.
      \begin{table}
        \input{table/schweb_param.tex}
        \caption{Parameter aus der linearen Regressionsrechnung der mit der Schwebungsmethode bestimmten Frequenzdifferenzen}
        \label{tab:param_reg_schweb}
      \end{table}
  \section{Diskussion}
    Die Strecke $s$ wurde abwechselnd auf beiden Seiten (Lichtschrankensender und -empfänger) gemessen.
    Der Fehler des Mittelwerts übersteigt dabei den anzunehmenden Ablesefehler und wird als Gesamtfehler angenommen.
    Ursachen dafür liegen darin, dass Anfangs- und Endpunkt der Messung nicht genau definiert gewesen sind und zudem von der individuellen Blickrichtung abgehangen haben.

    Die Messung der Zeit war durch die digitale Schaltung sehr genau, was sich im verhältnismäßig geringen Fehler des Mittelwerts äußert.
    Die geringe Abweichung der Messwerte ist darin zu suchen, dass der Wagen zwischen zwei Millisekundenpulsen das erste Tor passiert hat, oder die Lichtschranke nicht immer exakt gleich aktiviert hat, da Infrarotsender und -sensor leicht gegeneinander verschoben waren.
    Die errechnete Relativgeschwindigkeit besitzt für höhere Geschwindigkeiten einen zunehmenden Fehler, da die Genauigkeit der Zeitmessung durch die Lichtschranke hier absinkt.
    Trotzdem bleibt der Fehler in einem akzeptablen Bereich im Verhältnis zum Mittelwert.
    Der Unterschied der beiden Relativgeschwindigkeiten $v_\t{v}$ und $v_\t{z}$ ist auf im Betrag und Richtung unterschiedliche Reibungskräfte des Unterlagenfilzes und das teilweise verspätete Ansprechen des nur locker befestigten Wagens auf das Antriebsseil zurückzuführen.
    Der Wagen hatte somit unterschiedliche Beschleunigungsstrecken und erreichte nicht immer rechtzeitig eine konstante Geschwindigkeit.

    Bei der Bestimmung der Wellenlänge hat sich das Problem ergeben, dass die Lissajous-Figuren nie richtig eine scharfe Linie ergaben, sondern lediglich die Fläche minimal geworden ist.
    Die Bestimmung des Messpunktes war daher einer bestimmten Willkür unterlegen; trotzdem befindet sich der Fehler im akzeptablen Bereich.
    Die Messung der Grundfrequenz $\nu_0$ war aufgrund der Schaltung sehr exakt; Fehler sind, wenn überhaupt, nur in der minimalen Unschärfe des Frequenzgenerators und darin dass der Untersetzer zwischen zwei Mikrosekundenpulsen aktiviert worden ist.
    Für die Schallgeschwindigkeit ergibt sich ein Wert, der sich etwas von dem Literaturwert unterscheidet; dies ist auf Störsignale durch den Umgebungslärm sowie die benachbarte Apparatur zurückzuführen.

    Die Frequenzdifferenzen weisen vor allem im niedrigen Bereich bei der herkömmlichen Methode einen großen relativen Fehler auf, da die Messgenauigkeit nur in einem bestimmten Bereich liegt.
    Bei der Schwebungsmethode, ergibt sich keine Abweichung vom Mittelwert, die Messung ist also genauer und besser.
    Die linearen Fits passen in allen Fällen gut zu den Messwerten, was an den geringen Fehlern der Fitparameter ersichtlich ist.
    Vergleicht man die Fitparameter $A_\t{n,v}$, $A_\t{n,z}$, $A_\t{s,v}$ und $A_\t{s,z}$ mit der reziproken gemittelten Wellenlänge $1 / \lambda$ aus der Messung in Tabelle \ref{tab:c}, so fällt auf, dass sie sehr gut zusammen passen.
    Die reziproke Wellenlänge wird damit durch die vier Fits nochmal bestätigt.
  \makebibliography
\end{document}
