from labtools import *

gang, nu_v, nu_z = np.loadtxt(lt.file('data/nu'), unpack=True)

v_v = np.loadtxt(lt.file('result/v_v'))
v_z = np.loadtxt(lt.file('result/v_z'))
nu_0 = np.loadtxt(lt.file('result/nu_0'))
v_v = unc.unumpy.uarray((v_v[0], v_v[1]))
v_z = unc.unumpy.uarray((v_z[0], v_z[1]))
nu_0 = unc.ufloat((nu_0[0], nu_0[1]))

def meanofj(values, j):
    return lt.mean(np.split(values, len(values) // j), axis=-1)

nu_v_mean = meanofj(nu_v, 2)
nu_z_mean = meanofj(nu_z, 2)

deltanu_v = nu_v_mean - nu_0
deltanu_z = nu_z_mean - nu_0

def create_column(symbol, values, j=1, unit="", exp="", places=0, squeeze_unit=False):
    col = lt.SiColumn([], labtools.table.si.build_header(symbol, unit, exp, squeeze_unit))
    values = np.split(values, len(values) // j)
    for vals in values:
        cell = []
        for v in vals:
            cell.append(lt.round_places(str(v), places))
        col.add_cell(cell)
    return col

t = lt.Table("S[table-format=2.0]", headerRow=lt.Row(r'\multicolumn{1}{c}{Gang}'))
for g in np.split(gang, len(gang) // 2):
    t.add(lt.Row(int(g[0])))
t.add(create_column(r'\nu_\t{v}', nu_v * 1e-3, 2, r'\kilo\hertz', places=3))
t.add(create_column(r'\nu_\t{z}', nu_z * 1e-3, 2, r'\kilo\hertz', places=3))
t.savetable(lt.new("table/nu.tex"))

t = lt.SymbolColumnTable()
t.add_si_column(r'\t{Gang}', np.arange(1, 11) * 6, places=0)
t.add_si_column(r'\mean{\nu_\t{v}}', nu_v_mean * 1e-3, r'\kilo\hertz', squeeze_unit=True, zero_error_places=3)
t.add_si_column(r'\Del{\nu_\t{n,v}}', deltanu_v, r'\hertz', squeeze_unit=True)
t.add_si_column(r'\mean{\nu_\t{z}}', nu_z_mean * 1e-3, r'\kilo\hertz', squeeze_unit=True, zero_error_places=3)
t.add_si_column(r'\Del{\nu_\t{n,z}}', deltanu_z, r'\hertz', squeeze_unit=True)
t.savetable(lt.new('table/Delta_nu_norm.tex'))

A, B = lt.linregress(lt.vals(v_v), deltanu_v)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(0, 55, 10000) * 1e-2
ax.plot(x * 1e2, lt.vals(A * x + B), 'b-', label='Regressionsgerade')
ax.errorbar(lt.vals(v_v) * 1e2, lt.vals(deltanu_v), lt.stds(deltanu_v), fmt='rx', label='Messwerte')
ax.xaxis.set_view_interval(0, 55, ignore=True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
ax.set_xlabel(r'\silabel{v_\t{v}}{\centi\meter\per\second}')
ax.set_ylabel(r'\silabel{\Del{\nu_\t{n, v}}}{\hertz}')
lt.ax_reverse_legend(ax, loc='lower right')
fig.tight_layout()
fig.savefig(lt.new('graph/norm_pos.tex'), bbox_inches=0, pad_inches=0)

C, D = lt.linregress(lt.vals(v_z), deltanu_z)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(-55, 0, 10000) * 1e-2
ax.plot(x * 1e2, lt.vals(C * x + D), 'b-', label='Regressionsgerade')
ax.errorbar(lt.vals(v_z) * 1e2, lt.vals(deltanu_z), lt.stds(deltanu_z), fmt='rx', label='Messwerte')
ax.xaxis.set_view_interval(-55, 0, ignore=True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
ax.set_xlabel(r'\silabel{v_\t{z}}{\centi\meter\per\second}')
ax.set_ylabel(r'\silabel{\Del{\nu_\t{n, z}}}{\hertz}')
lt.ax_reverse_legend(ax, loc='lower right')
fig.tight_layout()
fig.savefig(lt.new('graph/norm_neg.tex'), bbox_inches=0, pad_inches=0)

t = lt.SymbolRowTable()
t.add_si_row(r'A_\t{n,v}', A * 1e-2, '\per\centi\meter')
t.add_si_row(r'B_\t{n,v}', B, '\hertz')
t.add_hrule()
t.add_si_row(r'A_\t{n,z}', C * 1e-2, '\per\centi\meter')
t.add_si_row(r'B_\t{n,z}', D, '\hertz')
t.savetable(lt.new('table/norm_param.tex'))
