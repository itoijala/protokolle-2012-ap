from labtools import *

gang, Delta_nu_v, Delta_nu_z = np.loadtxt(lt.file('data/Delta_nu'), unpack=True)
Delta_nu_z *= -1

v_v = np.loadtxt(lt.file('result/v_v'))
v_z = np.loadtxt(lt.file('result/v_z'))
v_v = unc.unumpy.uarray((v_v[0], v_v[1]))
v_z = unc.unumpy.uarray((v_z[0], v_z[1]))

def meanofj(values, j):
    return lt.mean(np.split(values, len(values) // j), axis=-1)

Delta_nu_v_mean = meanofj(Delta_nu_v, 2)
Delta_nu_z_mean = meanofj(Delta_nu_z, 2)

def create_column(symbol, values, j=1, unit="", exp="", places=0):
    col = lt.SiColumn([], labtools.table.si.build_header(symbol, unit, exp, False))
    values = np.split(values, len(values) // j)
    for vals in values:
        cell = []
        for v in vals:
            cell.append(lt.round_places(str(v), places))
        col.add_cell(cell)
    return col

t = lt.Table("S[table-format=2.0]", headerRow=lt.Row(r'\multicolumn{1}{c}{Gang}'))
for g in np.split(gang, len(gang) // 2):
    t.add(lt.Row(int(g[0])))
t.add(create_column(r'\Del{\nu_\t{s,v}}', Delta_nu_v, 2, r'\hertz'))
t.add(create_column(r'\mean{\Del{\nu_\t{s,v}}}', lt.vals(Delta_nu_v_mean), 1, r'\hertz'))
t.add(create_column(r'\Del{\nu_\t{s,z}}', Delta_nu_z, 2, r'\hertz'))
t.add(create_column(r'\mean{\Del{\nu_\t{s,z}}}', lt.vals(Delta_nu_z_mean), 1, r'\hertz'))
t.savetable(lt.new("table/Delta_nu.tex"))

A, B = lt.linregress(2 * lt.vals(v_v), Delta_nu_v_mean)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(0, 110, 10000) * 1e-2
ax.plot(x * 1e2, lt.vals(A * x + B), 'b-', label='Regressionsgerade')
ax.plot(2 * lt.vals(v_v) * 1e2, lt.vals(Delta_nu_v_mean), 'rx', label='Messwerte')
ax.xaxis.set_view_interval(0, 110, ignore=True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
ax.set_xlabel(r'\silabel{2 v_\t{v}}{\centi\meter\per\second}')
ax.set_ylabel(r'\silabel{\mean{\Del{\nu_\t{s,v}}}}{\hertz}')
lt.ax_reverse_legend(ax, loc='lower right')
fig.tight_layout()
fig.savefig(lt.new('graph/schwebe_pos.tex'), bbox_inches=0, pad_inches=0)

C, D = lt.linregress(2 * lt.vals(v_z), Delta_nu_z_mean)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(-110, 0, 10000) * 1e-2
ax.plot(x * 1e2, lt.vals(C * x + D), 'b-', label='Regressionsgerade')
ax.plot(2 * lt.vals(v_z) * 1e2, lt.vals(Delta_nu_z_mean), 'rx', label='Messwerte')
ax.xaxis.set_view_interval(-110, 0, ignore=True)
ax.xaxis.set_major_formatter(lt.SiFormatter())
ax.yaxis.set_major_formatter(lt.SiFormatter())
ax.set_xlabel(r'\silabel{2 v_\t{z}}{\centi\meter\per\second}')
ax.set_ylabel(r'\silabel{\mean{\Del{\nu_\t{s, z}}}}{\hertz}')
lt.ax_reverse_legend(ax, loc='lower right')
fig.tight_layout()
fig.savefig(lt.new('graph/schwebe_neg.tex'), bbox_inches=0, pad_inches=0)

t = lt.SymbolRowTable()
t.add_si_row(r'A_\t{s,v}', A * 1e-2, '\per\centi\meter')
t.add_si_row(r'B_\t{s,v}', B, '\hertz')
t.add_hrule()
t.add_si_row(r'A_\t{s,z}', C * 1e-2, '\per\centi\meter')
t.add_si_row(r'B_\t{s,z}', D, '\hertz')
t.savetable(lt.new('table/schweb_param.tex'))
