from labtools import *

s = np.loadtxt(lt.file('data/s'), unpack=True)
s *= 1e-2

s_mean = lt.mean(s)

np.savetxt(lt.new('result/s'), (lt.vals(s_mean), lt.stds(s_mean)))

t = lt.SymbolRowTable()
t.add_si_row('s', s * 1e2, r'\centi\meter', places=1)
t.add_hrule()
t.add_si_row(r'\mean{s}', s_mean * 1e2, r'\centi\meter')
t.savetable(lt.new('table/s.tex'))
