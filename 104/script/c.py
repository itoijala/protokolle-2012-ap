from labtools import *

nu_0 = np.loadtxt(lt.file('data/nu_0'), unpack=True)
d = np.loadtxt(lt.file('data/d'), unpack=True)
d *= 1e-3

v_max = np.loadtxt(lt.file('result/v_max'), unpack=True)
v_max = unc.ufloat((v_max[0], v_max[1]))

lambda_ = 2 * np.ediff1d(d)
nu_0_mean = lt.mean(nu_0)
lambda_mean = lt.mean(lambda_)
c = lambda_mean * nu_0_mean

nu_q = nu_0_mean * (1 - v_max / c)**-1
nu_e = nu_0_mean * (1 + v_max / c)

np.savetxt(lt.new('result/nu_0'), (lt.vals(nu_0_mean), lt.stds(nu_0_mean)))
np.savetxt(lt.new('result/c'), (lt.vals(c), lt.stds(c)))

t = lt.SymbolRowTable()
t.add_si_row(r'\nu_0', nu_0 * 1e-3, r'\kilo\hertz', places=3)
t.add_hrule()
t.add_si_row(r'\mean{\nu_0}', nu_0_mean * 1e-3, r'\kilo\hertz')
t.savetable(lt.new('table/nu_0.tex'))

t = lt.SymbolRowTable()
t.add_si_row(r'd', d * 1e3, r'\milli\meter', places=2)
t.add_hrule()
t.add_si_row(r'\lambda', lambda_ * 1e3, r'\milli\meter', places=2)
t.add_hrule()
t.add_si_row(r'\mean{\lambda}', lambda_mean * 1e3, r'\milli\meter')
t.savetable(lt.new('table/lambda.tex'))

t = lt.SymbolRowTable()
t.add_si_row('c', c, r'\meter\per\second')
t.add_si_row(r'c_\t{lit}', sp.constants.speed_of_sound, r'\meter\per\second', figures=3)
t.add_si_row('\lambda^{-1}', 1 / lambda_mean * 1e-2, '\per\centi\meter')
t.savetable(lt.new('table/c.tex'))

t = lt.SymbolRowTable()
t.add_si_row(r'\mean{\abs{v_\t{max}}}', v_max * 1e2, r'\centi\meter\per\second')
t.add_si_row(r'\nu_\t{E}', nu_e * 1e-3, r'\kilo\hertz')
t.add_si_row(r'\nu_\t{Q}', nu_q * 1e-3, r'\kilo\hertz')
t.savetable(lt.new('table/nu_qe.tex'))
