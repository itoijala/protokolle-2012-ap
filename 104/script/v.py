import labtools
from labtools import *

gang, t_v, t_z = np.loadtxt(lt.file('data/v'), unpack=True)
t_v *= 1e-3
t_z *= 1e-3

s = np.loadtxt(lt.file('result/s'))
s = unc.ufloat((s[0], s[1]))

def meanofj(values, j):
    return lt.mean(np.split(values, len(values) // j), axis=-1)

t_v_mean = meanofj(t_v, 4)
t_z_mean = meanofj(t_z, 4)

v_v = s / t_v_mean
v_z = - s / t_z_mean

v_max = lt.mean([abs(max(v_v)), abs(min(v_z))])

np.savetxt(lt.new('result/v_v'), (lt.vals(v_v), lt.stds(v_v)))
np.savetxt(lt.new('result/v_z'), (lt.vals(v_z), lt.stds(v_z)))
np.savetxt(lt.new('result/v_max'), (lt.vals(v_max), lt.stds(v_max)))

def create_column(symbol, values, j=1, unit="", exp="", places=0):
    col = lt.SiColumn([], labtools.table.si.build_header(symbol, unit, exp, False))
    values = np.split(values, len(values) // j)
    for vals in values:
        cell = []
        for v in vals:
            cell.append(lt.round_places(str(v), places))
        col.add_cell(cell)
    return col

t = lt.Table()
for i in range(5):
    t.add_row(lt.Row())
t.add(create_column(r'\t{Gang}', np.arange(1, 6) * 6, 1, ))
t.add(create_column(r't_\t{v}', t_v[:20] * 1e3, 4, r'\milli\second'))
t.add(create_column(r't_\t{z}', t_z[:20] * 1e3, 4, r'\milli\second'))
t.add(create_column(r'\t{Gang}', np.arange(6, 11) * 6, 1, ))
t.add(create_column(r't_\t{v}', t_v[20:] * 1e3, 4, r'\milli\second'))
t.add(create_column(r't_\t{z}', t_z[20:] * 1e3, 4, r'\milli\second'))
t.savetable(lt.new('table/t.tex'))

t = lt.SymbolColumnTable()
t.add_si_column(r'\t{Gang}', np.arange(1, 11) * 6, places=0)
t.add_si_column(r'\mean{t_\t{v}}', t_v_mean * 1e3, r'\milli\second')
t.add_si_column(r'v_\t{v}', v_v * 1e2, r'\centi\meter\per\second')
t.add_si_column(r'\mean{t_\t{z}}', t_z_mean * 1e3, r'\milli\second')
t.add_si_column(r'v_\t{z}', v_z * 1e2, r'\centi\meter\per\second')
t.savetable(lt.new('table/v.tex'))
