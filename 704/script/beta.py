from labtools import *

eV = lt.constant('electron volt')

rho_Al = np.loadtxt(lt.file('data/rho_Al'))
rho_Al *= 1e3

T_0, Z_0 = np.loadtxt(lt.file('data/beta_0'), unpack=True)
Z_0 = unc.ufloat((Z_0, np.sqrt(Z_0)))

D, T, Z = np.loadtxt(lt.file('data/beta'), unpack=True)
D *= 1e-6
Z = unc.unumpy.uarray((Z, np.sqrt(Z)))

N_0 = Z_0 / T_0
N = Z / T - N_0

A1, B1 = lt.linregress(D[:5], np.log(lt.vals(N[:5])))
#A2, B2 = lt.linregress(D[5:], np.log(lt.vals(N[5:])))
A2, B2 = (0, unc.unumpy.log(N[6]))

D_max = (B2 - B1) / (A1 - A2)
R_max = rho_Al * D_max
E_max = 1.92e6 * eV * unc.unumpy.sqrt(1e-2 * R_max**2 + 0.022 * R_max)

t1 = lt.SymbolRowTable()
t1.add_si_row(r'\rho_\ce{Al}', rho_Al * 1e-3, r'\gram\per\cubic\centi\meter', places=5)
t1.add_hrule()
t1.add_si_row('T_0', T_0, r'\second', places=0)
t1.add_si_row('Z_0', lt.vals(Z_0), places=0)

t2 = lt.SymbolColumnTable()
t2.add_si_column('D', D * 1e6, r'\micro\meter', places=0)
t2.add_si_column('T', T, r'\second', places=0)
t2.add_si_column('Z', lt.vals(Z), places=0)
t2.add_si_column('N', N)

t3 = lt.Table()
t3.add(lt.Row())
t3.add_hrule()
t3.add(lt.Row())
t3.add(lt.Column([t1, t2], '@{} c @{}'))
t3.savetable(lt.new('table/beta-data.tex'))

t = lt.SymbolRowTable()
t.add_si_row(r'A_1', A1 * 1e-3, r'\per\milli\meter')
t.add_si_row(r'B_1', B1)
t.add_hrule()
t.add_si_row(r'A_2', A2 * 1e-3, r'\per\milli\meter')
t.add_si_row(r'B_2', B2)
t.add_hrule()
t.add_si_row(r'D_\t{max}', lt.vals(D_max) * 1e6, r'\micro\meter', places=0)
t.add_si_row(r'R_\t{max}', lt.vals(R_max), r'\kilogram\per\square\meter', figures=2)
t.add_si_row(r'E_\t{max}', lt.vals(E_max / eV) * 1e-3, '\kilo\electronvolt', places=0)
t.savetable(lt.new('table/beta-calc.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.axvline(x=lt.vals(D_max), c='c', ls='--', label=r'$D_\t{max}$')
x = np.array([0.95e-4, 5e-4])
ax.plot(x, np.exp(lt.vals(A2 * x + B2)), 'g-', label='Gerade 2')
ax.plot(x, np.exp(lt.vals(A1 * x + B1)), 'b-', label='Regressionsgerade 1')
ax.errorbar(D[5:-4], lt.vals(N[5:-4]), yerr=lt.stds(N[5:-4]), fmt='mx', label='Messwerte 2')
ax.errorbar(D[:5], lt.vals(N[:5]), yerr=lt.stds(N[:5]), fmt='rx', label='Messwerte 1')
ax.set_yscale('log')
ax.xaxis.set_view_interval(0.95e-4, 5e-4, True)
ax.yaxis.set_view_interval(0.01, 100, True)
ax.xaxis.set_major_formatter(lt.SiFormatter(factor=1e6))
ax.set_xlabel(r'\silabel{D}{\micro\meter}')
ax.set_ylabel(r'\silabel{N}{\per\second}')
lt.ax_reverse_legend(ax, 'upper right')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/beta.tex'))
