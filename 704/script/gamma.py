from labtools import *

N_A = lt.constant('Avogadro constant')
r_e = lt.constant('classical electron radius')
eV = lt.constant('electron volt')

T_0, Z_0 = np.loadtxt(lt.file('data/gamma_0'), unpack=True)
Z_0 = unc.ufloat((Z_0, np.sqrt(Z_0)))

N_0 = Z_0 / T_0

epsilon = np.loadtxt(lt.file('data/epsilon'))

sigma_Comp = 2 * np.pi * r_e**2 * ((1 + epsilon) / epsilon**2 * (2 * (1 + epsilon) / (1 + 2 * epsilon) - 1 / epsilon * np.log(1 + 2 * epsilon)) + 1 / 2 / epsilon * np.log(1 + 2 * epsilon) - (1 + 3 * epsilon) / (1 + 2 * epsilon)**2)

elements = ['Pb', # lt.file('data/Pb') lt.file('data/gamma-Pb') lt.new('table/Pb-data.tex') lt.new('table/Pb-calc.tex') savefig(lt.new('graph/Pb.tex'))
            'Zn'] # lt.file('data/Zn') lt.file('data/gamma-Zn') lt.new('table/Zn-data.tex') lt.new('table/Zn-calc.tex') savefig(lt.new('graph/Zn.tex'))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.array([0 * 1e-3, 31 * 1e-3])

for i in range(len(elements)):
    z, rho, M = np.loadtxt('data/' + elements[i], unpack=True)
    rho *= 1e3
    M *= 1e-3

    D, T, Z = np.loadtxt('data/gamma-' + elements[i], unpack=True)
    D *= 1e-3
    Z = unc.unumpy.uarray((Z, np.sqrt(Z)))

    N = Z / T - N_0

    A, B = lt.linregress(D, np.log(lt.vals(N)))
    mu = - A
    N0 = unc.unumpy.exp(B)

    mu_Comp = z * N_A * rho / M * sigma_Comp

    t1 = lt.SymbolRowTable()
    t1.add_si_row('z', z, places=0)
    t1.add_si_row(r'\rho', rho * 1e-3, r'\gram\per\cubic\centi\meter', figures=6)
    t1.add_si_row('M', M * 1e3, r'\gram\per\mole', figures=4)

    t2 = lt.SymbolColumnTable()
    t2.add_si_column('D', D * 1e3, r'\milli\meter', places=0)
    t2.add_si_column('T', T, r'\second', places=0)
    t2.add_si_column('Z', lt.vals(Z), places=0)
    t2.add_si_column('N', N)

    t3 = lt.Table()
    t3.add(lt.Row())
    t3.add_hrule()
    t3.add(lt.Row())
    t3.add(lt.Column([t1, t2], '@{} c @{}'))
    t3.savetable(lt.new('table/' + elements[i] + '-data.tex'))

    t = lt.SymbolRowTable()
    t.add_si_row('A', A, r'\per\meter')
    t.add_si_row('B', B)
    t.add_hrule()
    t.add_si_row(r'\mu', mu, r'\per\meter')
    t.add_si_row('N_0', N0, r'\per\second')
    t.add_hrule()
    t.add_si_row(r'\mu_\t{Comp}', mu_Comp, r'\per\meter')
    t.savetable(lt.new('table/' + elements[i] + '-calc.tex'))

    ax.plot(x, np.exp(lt.vals(A * x + B)), '-', label=r'Regressionsgerade \ce{{{:s}}}'.format(elements[i]))
    ax.errorbar(D, lt.vals(N), yerr=lt.stds(N), fmt='x', label=r'Messwerte \ce{{{:s}}}'.format(elements[i]))

ax.set_yscale('log')
ax.xaxis.set_view_interval(0 * 1e-3, 31 * 1e-3, True)
ax.yaxis.set_view_interval(1, 40, True)
ax.xaxis.set_major_formatter(lt.SiFormatter(factor=1e3))
ax.set_xlabel(r'\silabel{D}{\milli\meter}')
ax.set_ylabel(r'\silabel{N}{\per\second}')
lt.ax_reverse_legend(ax, 'lower left')
fig.tight_layout()
lt.savefig(fig, lt.new('graph/gamma.tex'))

t = lt.SymbolRowTable()
t.add_si_row(r'N_\t{A}', N_A * 1e-23, r'\per\mole', exp="e23")
t.add_si_row(r'r_\t{e}', r_e * 1e15, r'\meter', exp="e-15")
t.add_si_row(r'\si{\electronvolt}', eV * 1e19, r'\joule', exp="e19")
t.savetable(lt.new('table/constants.tex'))

t = lt.SymbolRowTable()
t.add_si_row(r'\epsilon', epsilon, places=3)
t.add_hrule()
t.add_si_row('T_0', T_0, r'\second', places=0)
t.add_si_row('Z_0', lt.vals(Z_0), places=0)
t.savetable(lt.new('table/gamma-data.tex'))
