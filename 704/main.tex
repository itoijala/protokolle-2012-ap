\input{header/report.tex}

\addbibresource{lit.bib}

\SetExperimentNumber{704}

\begin{document}
  \maketitlepage{\texorpdfstring{Absorption von $\particle{\beta}$- und $\particle{\gamma}$-Strahlung}{Absorption von β- und γ-Strahlung}}{19.06.2012}{26.06.2012}
  \section{Ziel}
    Das Ziel des Versuchs ist es, das Absorptionsgesetz für $\particle{\gamma}$-Strahlung zu bestätigen, den Absorptionskoeffizienten verschiedener Materialien zu bestimmen und die maximale Energie einer $\particle{\beta}$-Strahlungsquelle zu bestimmen.
  \section{Theorie}
    \subsection{Wirkungsquerschnitt und Absorptionsgesetz}
      Bei der Wechselwirkung eines Teilchenstrahls mit Materie kommt es zu einer Intensitätsabnahme, das heißt die Zahl der Teilchen pro Zeit und Flächeneinheit verringert sich.
      Für die quantitative Beschreibung der Häufigkeit der Wechselwirkung wird der Wirkungsquerschnit $\sigma$ eingeführt.
      Dieser beschreibt eine imaginäre Fläche $\sigma$ für ein Partikel des Targets, die genau so groß ist, dass Wechselwirkung eintritt, wenn das einfallende Teilchen die Scheibe trifft.
      Die Wahrscheinlichkeit, dass ein einfallendes Teilchen eine Reaktion auslöst ist gegeben durch
      \begin{eqn}
        W = n D \sigma ,
      \end{eqn}
      wobei $n$ die Anzahl der Materieteilchen pro Volumeneinheit und $D$ die Dicke des Materials sind.
      Daraus folgt für die Anzahl der Wechselwirkungen pro Zeiteinheit
      \begin{eqn}
        N = N_0 n D \sigma ;
      \end{eqn}
      die Größe $N_0$ bezeichnet die Anzal der einfallenden Teilchen.
      Die hergeleitete Formel gilt nur für den Idealfall, dass sich die absorbierenden Scheiben in Strahlrichtung nicht überlappen; im Experiment ist das durch die große Anzal der Atome pro Volumeneinheit nicht gegeben.
      Aus diesem Grund wird über eine infinitesimal dünne Schicht $\dif{x}$ von Scheiben summiert, in der die Überdeckung vernachlässigbar ist.
      Wenn angenommen wird, dass sich die besagte Schicht an der Stelle $x$ im Absorber der Dicke $D$ befindet, so ist die Anzahl der bisher noch nicht absorbierten Teilchen durch $\f{N}(x)$ gegeben und es gilt für die Anzahl der Reaktionen
      \begin{eqn}
        \dif{N} = - \f{N}(x) n \sigma \dif{x} .
      \end{eqn}
      Für die Anzahl der Teilchen nach Durchlaufen einer Dicke $D$ folgt nach Separation der Variablen
      \begin{eqn}
        \f{N}(D) = N_0 \exp(-n \sigma D) .
      \end{eqn}
      Dieses exponentielle Absorptionsgesetz ist streng gültig, falls pro einfallendem Teilchen höchstens eine Reaktion mit der gesamten Materieschicht stattfindet.
      Das ist nur der Fall, wenn es nach der Wechselwirkung vernichtet wird, oder die mittlere Entfernung zwischen zwei Reaktionen groß gegen die Dicke $D$ ist.
      Der Exponentialfaktor
      \begin{eqn}
        \mu := n \sigma \eqlabel{eqn:absorb}
      \end{eqn}
      wird als Absorptionskoeffizient bezeichnet.
      Nach Bestimmung von $\mu$ lässt sich der Wirkungsquerschnitt $\sigma$ mit Gleichung \eqref{eqn:absorb} abschätzen.
      Wird angenommen, dass die Elektronen eines Absorbermaterials die Wechselwirkungszentren darstellen, so gilt
      \begin{eqn}
        n = \frac{z N_\t{A} \rho}{M} .
      \end{eqn}
      Dabei ist $z$ die Ordnungszahl des Elements, $N_\t{A}$ die Avogadrokonstante, $M$ die molare Masse und $\rho$ die Dichte des Materials.
      Somit gilt für den Wirkungsquerschnitt
      \begin{eqn}
        \sigma = \frac{\mu M}{z N_\t{A} \rho} . \eqlabel{eqn:wirkquer}
      \end{eqn}
      Diese Formel ist nur eine grobe Näherung der Wirklichkeit; es wurde nicht beachtet dass $\sigma$ von der Energie abhängt und die Elektronen untereinander wechselwirken.
    \subsection{\texorpdfstring{$\particle{\gamma}$-Strahlung}{γ-Strahlung}}
      \subsubsection{Ursprung und Eigenschaften}
        Atomkerne besitzen in Analogie zu Elektronen ebenfalls diskrete Energieniveaus.
        Beim Übergang des Kerns von einem höheren in einen energetisch niedrigeren Zustand äußert sich die freiwerdende Energie in der Emission eines $\particle{\gamma}$-Quants.
        Der $\particle{\gamma}$-Quant unterliegt dem Teilchen-Welle-Dualismus der Quantenmechanik.
        Die Vorstellung des $\particle{\gamma}$-Quants als lokalisiertes Teilchen, also mit Masse, Impuls und Energie, ist im Rahmen dieses Experiments am dienlichsten; da der $\particle{\gamma}$-Quant lichtartig ist, besitzt er keine Ruhemasse, die Masse ist lediglich proportional zur Energie.
        Ebenso kann dem Quant eine Energie als Welle zugeordnet werden:
        \begin{eqn}
          E = \hbar \omega = \hbar \frac{c}{\lambda} . \eqlabel{eqn:energy}
        \end{eqn}
        Hierbei ist $\omega$ die Frequenz, $c$ die Lichtgeschwindigkeit, $\lambda$ die Wellenlänge und $\hbar$ das reduzierte plancksche Wirkungsquantum.
        Wegen der Energie-Zeit Unschärfe
        \begin{eqn}
          \Del{E} \Del{\tau} \approx \hbar ,
        \end{eqn}
        die aus der heisenbergschen Unschärferelation folgt, folgt auf Grund der sehr großen Lebensdauer der Kernzustände eine scharfe Bestimmung der Energie; deshalb sind die Spektrallinien deutlich schärfer als im optischen Bereich.
      \subsubsection{Wechselwirkung mit Materie}
        Die Quanten der $\particle{\gamma}$-Strahlung wechselwirken in Materie mit den Atomelektronen, den Kernen und ihren elektrischen Feldern; der Quant wird annihiliert, verliert Energie, oder ändert seine Ausbreitungsrichtung.
        Abhängig von Energie und Wechselwirkungspartner kommt es zu unterschiedlichen Effekten.
        Im hier interessanten Bereich von \SIrange{10}{10000}{\kilo\electronvolt} kommt es im Wesentlichen zu Annihilationseffekten, zu Richtungsänderung und Energieverlust an den Stoßpartner bei inelastischer Streuung und lediglich zu Richtungsänderung ohne Energieverlust bei elastischer Streuung.

        Die wichtigsten Prozesse sind der Photoeffekt bei Annihilationseffekten mit einem Elektron, der Compton-Effekt bei inelastischer Streuung an einem Elektron und die Paarbildung bei Annihilationseffekten mit dem elektrischen Feld.
        Beim Photoeffekt, der hier für innere Schalenelektronen betrachtet wird, wird ein $\particle{\gamma}$-Quant bei der Ionisation eines Atoms vernichtet; die kinetische Energie des befreiten Elektrons ergibt sich aus der Differenz der Photonenergie \eqref{eqn:energy} und der Bindungsenergie.
        Nach Anwendung des Impulssatzes ergibt sich die Tatsache, dass der Photoeffekt nur eintritt, wenn das Atom einen Teilimpuls beim Stoß aufnimmt.
        Dies funktioniert allerdings nur, wenn die Elektronen möglichst fest an den Kern gebunden sind, wie es bei inneren Schalenelektronen oder bei Atomen mit hoher Ordnungszahl der Fall ist, bei denen die Bindungsenegie proportional zum Quadrat der Ordnungszahl ist.
        Für innere Elektronen nimmt die Absorptionswahrscheinlichkeit bei festem $z$ ab, genauer gilt
        \begin{eqn}
          \sigma \propto z^5 E_{\particle{\gamma}}^{- \frac{7}{2}}
        \end{eqn}
        für $E_\particle{\gamma} > E_\t{B,K}$, wobei $E_\particle{\gamma}$ die Energie der $\particle{\gamma}$-Photonen und $E_\t{B,K}$ die Bindungsenergie von Elektronen der K-Schale ist.
        Beim Nachrücken von Elektronen höherer Bindungszustände in das entstandene Loch kommt es zu Emission von Röntgenquanten oder Auger-Elektronen, die entstehen wenn ein Röntgenquant aus dem Atominneren von einem äußeren Hüllenelektron absorbiert wird.

        Beim Compton-Effekt wird ein $\particle{\gamma}$-Quant an einem freien Elektron gestreut.
        Dies sind beispielsweise freie Elektronen in Leitern und äußere Hüllenelektronen für $E_\particle{\gamma} \gg E_\t{B}$.
        Der Compton-Effekt ist ein inelastischer Streuprozess, der sich mit Energie- und Impulserhaltungssätzen beschreiben lässt.
        Ein Ergebnis ist, dass das Photon nie unter Abgabe seiner ganzen Energie verschwindet.
        Jedoch fächert der $\particle{\gamma}$-Strahl auf, wodurch es zu einer Intensitätsabnahme kommt.
        Für den Wirkungsquerschnitt beim Compton-Effekt ergibt sich
        \begin{eqns}[rl]
          \sigma_\t{Comp} = 2 \PI r_\t{e}^2 \g{(}{\vphantom{\g(\frac{2\g(1 + \epsilon)}{1 + 2\epsilon})}}{.} & \frac{1 + \epsilon}{\epsilon^2} \g(\frac{2\g(1 + \epsilon)}{1 + 2\epsilon} - \frac{1}{\epsilon} \ln(1 + 2\epsilon)) \\
          & {} + \frac{1}{2\epsilon} \ln(1 + 2\epsilon) - \frac{1 + 3\epsilon}{\g(1 + 2\epsilon)^2}\g{.}{\vphantom{\g(\frac{2\g(1 + \epsilon)}{1 + 2\epsilon})}}{)} , \eqlabel{eqn:sigma_Comp}
        \end{eqns}
        wobei
        \begin{eqn}
          \epsilon := \frac{E_\particle{\gamma}}{m c^2}
        \end{eqn}
        und
        \begin{eqn}
          r_e = \frac{e^2}{4 \PI \epsilon_0 m c^2} .
        \end{eqn}
        Dabei ist $e$ die Elementarladung, $m$ die Ruhemasse des Elektrons und $\epsilon_0$ die elektrische Feldkonstante.
        Der klassische Elektronenradius $r_\t{e}$ gibt eine Streuwahrscheinlichkeit an und macht keine Aussagen über den tatsächlichen Radius.
        Mit Gleichung \eqref{eqn:wirkquer} und \eqref{eqn:sigma_Comp} ergibt sich für den Absorptionskoeffizienten
        \begin{eqn}
          \mu_\t{Comp} = \frac{z N_\t{A} \rho}{M} \sigma_\t{Comp} . \eqlabel{eqn:mu_Comp}
        \end{eqn}
        Für kleine Energien $E_\particle{\gamma} \ll m_0 c^2$ strebt $\sigma_\t{Comp}$ gegen den Grenzwert
        \begin{eqn}
          \lim_{\epsilon \to 0} \sigma_\t{Comp} = \frac{8}{3} \PI r_\t{e} =: \sigma_\t{Th} ,
        \end{eqn}
        wobei $\sigma_\t{Th}$ der thomsonsche Wirkungsquerschnitt ist.
        Für mittlere Quantenenergien besteht ein komplizierter Zusammenhang zwischen dem comptonschen Wirkungsquerschnitt $\sigma_\t{Comp}$ und $\epsilon$, der Wirkungsquerschnitt fällt jedoch monoton mit wachsendem $\epsilon$, dies wird insbesondere in Gleichung \eqref{eqn:sigma_Comp} deutlich.
        Ein typischer Kurvenverlauf des Comptoneffekts ist in Abbildung \ref{fig:absorptionskurven} veranschaulicht.
        \begin{figure}
          \includegraphics[scale=0.7]{graphic/absorptionskurven.pdf}
          \caption{Energieabhängigkeit des Absorptionskoeffizienten $\mu$ für Germanium getrennt nach den verschiedenen Wechselwirkungsmechanismen sowie Totaleffekt \cite{anleitung704}}
          \label{fig:absorptionskurven}
        \end{figure}

        Gilt für die Quantenenergie
        \begin{eqn}
          E_\particle{\gamma} > 2 m c^2 = \SI{1.02}{\mega\electronvolt} , \eqlabel{eqn:paarerzeugung}
        \end{eqn}
        so kann ein $\particle{\gamma}$-Photon annihiliert werden, indem ein Elektron und ein Positron erzeugt werden.
        Um Impulserhaltung zu erfüllen, muss ein weiterer Stoßpartner den überschüssigen Impuls aufnehmen; in der Regel sind dies die Atome des Absorbermaterials, in deren Coulomb-Feldern die Paarbildung abläuft.
        Es zeigt sich, dass für den Wirkunsquerschnitt der Parrbildung
        \begin{eqn}
          \sigma_\t{p} \propto z^2
        \end{eqn}
        gilt.

        Insgesamt überlagen sich alle Effekte beim Durchgang eines $\particle{\gamma}$-Quants durch die Materieschicht.
        Die Kurven der einzelnen Effekte sowie die Summe aller Kurven sind ebenfalls in Abbildung \ref{fig:absorptionskurven} aufgetragen.
        Es fällt auf, dass für kleine Energien der Photoeffekt dominiert, bei mittlerer Energie kann er bereits vernachlässigt werden, insbesondere für kleine $z$.
        Der Compton-Effekt ist für niedrige Energien wenig ausgeprägt, für mittlere, also ab \SI{200}{\electronvolt}, wird er dominant, für größere Energien geht der Wirkungsquerschnitt mit $E^{-1}$ gegen null.
        Die Paarbildung beginnt ab circa \SI{1}{\mega\electronvolt}, der Wirkungsquerschnitt erreicht erst im Bereich von \SI{5}{\mega\electronvolt} die selbe Größenordnung wie $\sigma_\t{Comp}$.
        Die Gesamtabsorption wächst proportional zu $\ln.E_\particle{\gamma}.$.
        Oberhalb von \SI{100}{\mega\electronvolt} findet fast ausschließlich Paarbildung statt.
    \subsection{\texorpdfstring{$\particle{\beta}$-Strahlung}{β-Strahlung}}
      \subsubsection{Ursprung und Eigenschaften}
        Die $\particle{\beta}$-Strahlung besteht aus Elektronen oder Positronen hoher kinetischer Energie, die der Kern des Atoms ausstößt.
        Die $\particle{\beta}$-Teilchen enstehen durch die zwei Prozesse
        \begin{eqns}
          \cee{\particle{n} &->& \particle{p} + \particle{\beta^-} + \particle{\overline{\nu_\particle{e}}}} \\
          \cee{\particle{p} &->& \particle{n} + \particle{\beta^+} + \particle{\nu_\particle{e}}} .
        \end{eqns}
        Bei der $\particle{\beta}$-Strahlung tritt ein kontinuierliches Spektrum auf, da als weiteres Elementarteilchen ein Antineutrino $\overline{\particle{\nu}_\particle{e}}$, oder ein Neutrino $\particle{\nu}_\particle{e}$ emittiert wird, verteilt sich die freiwerdende, konstante Energie statistisch auf das Elektron und das Neutrino, beziehungsweise dem Rückstoßkern.
        Die maximale Energie $E_\t{max}$ ist gleich der Energie, die beim Kernumwandlungsprozess frei wird.
        Durch die Emission des Neutrinos oder des Antineutrinos wird die Energieerhaltung, die Impulshaltung und die Erhaltung des Drehimpulses gewährleistet.
        Ein typisches Emissionsspektrum eines $\particle{\beta}$-Strahlers ist in Abbildung \ref{fig:emiss_beta} gezeigt.
        \begin{figure}
          \includegraphics{graphic/emiss_beta.pdf}
          \caption{Emissionsspektrum eines $\particle{\beta}$-Strahlers \cite{anleitung704}}
          \label{fig:emiss_beta}
        \end{figure}
      \subsubsection{Wechselwirkung mit Materie}
        Von entscheidender Bedeutung bei der Wechselwirkung der $\particle{\beta}$-Teilchen mit Materie ist ihre Ladung und die im Verhältnis zu anderen Teilchen geringe Masse.
        Aus diesem Grund kommt es im Gegensatz zur $\particle{\gamma}$-Strahlung zu einer Vielzahl von Wechselwirkungsprozessen.
        Im Prinzip treten die elastische Streuung am Atomkern und die inelastischen Streuungen an Atomkernen und Elektronen des Absorbermaterials auf.

        Die elastische Streuung am Atomkern hat auch den Namen Rutherford-Streuung.
        Dabei kommt es zu bemerkenswerten Ablenkungen aus der Bahnrichtung im Coulomb-Feld der Elektronen; der auffächernde Elektronenstrahl verliert an Intensität, gleichzeitig kommt es zu beträchtlich größeren Bahnlängen relativ zur Reichweite $R$, geradlinig gemessen vom Anfangspunkt zum Endpunkt der Bahn.
        Die rutherfordsche Streuformel muss wegen den hohen Geschwindigkeiten relativistische Korrekturen erhalten, außerdem herrscht auch kein reines Coulomb-Feld, vielmehr wird es durch Hüllenelektronen teilweise abgeschirmt.
        Schließlich spielt das magnetische Moment des Elektrons, insbesondere bei einem polarisierten $\particle{\beta}$-Strahl, bei elastischer Streuung eine Rolle.
        Bei diesem Effekt kommt es also hauptsächlich zu einer Richtungsablenkung und weniger zu einer Bremsung der Elektronen.

        Bei der inelastischen Streuung am Atomkern des Absorbermaterials kommt es durch die Beschleunigung der Ladung zur Abnahme der Energie in Vielfachen von $\hbar \omega$, da die Elektronen Photonen aussenden.
        Die Wahrscheinlichkeit für diesen Prozess ist bezogen auf einen Atomkern durch
        \begin{eqn}
          \sigma_\t{Br} = \alpha r_e^2 z^2,
        \end{eqn}
        mit der Sommerfeldschen Feinstrukturkonstante $\alpha$ gegeben.
        So spielt die Bremsstrahlung nur bei schweren Absorberkernen eine Rolle.
        Nach Integration über alle Streuwinkel ergibt sich
        \begin{eqn}
          E_\t{Br} \approx \SI{7e-7}{\kilo\electronvolt} z E_\particle{\beta}^2
        \end{eqn}
        mit der Energie $E_\particle{\beta}$ des einfallenden Beta-Teilchens.
        Für diese Formel gibt es einen Cut-Off bei etwa \SI{2500}{\electronvolt}, sie genügt daher für die üblichen Beta-Strahler.
        Energieverluste durch Bremsstrahlung spielen bei der Absorption der $\particle{\beta}$-Strahlung nur eine untergeordnete Rolle.

        Bei der inelastischen Streuung an den Elektronen kommt es zur Ionisation und Anregung der Absorberatome.
        Die Energie eines $\particle{\beta}$-Teilchens reicht für mehrere derartige Prozesse und die Wahrscheinlichkeit wächst proportional zu $z$ und der Anzahl der Elektronen pro Volumeneinheit, also der Atome pro Volumeneinheit.
        Für Energien der $\particle{\beta}$-Strahlung, die kleiner als die Ruheenergie des Elektrons sind, ist der Energieverlust pro Absorberschichtdicke durch die Beziehung
        \begin{eqn}
          \d{x}{E}; = - \frac{2 \PI r_\t{e}^2}{E_\particle{\beta}} \frac{z N_\t{A} \rho}{M} \frac{\ln.E_\particle{\beta}.}{I}
        \end{eqn}
        gegeben, wobei $I$ die Ionisationsenergie der Absorberatome ist.
      \subsubsection{Absorptionskurve}
        Bei der $\particle{\beta}$-Strahlung lassen sich schwerer Aussagen darüber treffen, wie eine Absorptionsfunktion abhängig von der Dicke des Materials aussehen könnte.
        Für hier betrachtete $\particle{\beta}$-Teilchen aus natürlichen Quellen ist aber für geringe Schichtdicken durch eine gute Näherung ein Absorptionsgesetz, ähnlich wie bei der Gammastrahlung, gültig.
        Erst für Schichtdicken nahe der maximalen Reichweiten gibt es starke Abweichungen von der Formel, oberhalb der maximalen Reichweite $R_\t{max}$ wird eine weitgehend schichtdickenunabhängige Intensität angezeigt, die zum Beispiel von der Bremsstrahlung stammt.
        Für die Massenbelegung gilt
        \begin{eqn}
          R = \rho D .
        \end{eqn}
        Eine Absorptionskurve wie in Abbildung \ref{fig:absorp_beta} dient dazu, die maximale Reichweite des Teilchens zu bestimmen; dabei werden lineare Teile der Kurve verlängert und zum Schnitt gebracht.
        Da die maximale Reichweite $R_\t{max}$, also die $x$-Koordinate des Schnittpunkts, im Wesentlichen durch die energiereichsten Elektronen bestimmt wird, kann man dadurch die maximale Energie, die beim $\particle{\beta}$-Zerfall frei wird, ermitteln.
        Empirisch gilt für dieses Experiment
        \begin{eqn}
          E_\t{max} = \SI{1.92}{\mega\electronvolt\meter\squared\per\kilogram} \sqrt{\num{0.01} R^2_\t{max} + \SI{0.022}{\kilogram\per\meter\squared} R_\t{max}} . \eqlabel{eqn:E_max}
        \end{eqn}
        \begin{figure}
          \includegraphics{graphic/absorp_beta.pdf}
          \caption{Absorptionskurve für einen natürlichen $\particle{\beta}$-Strahler \cite{anleitung704}}
          \label{fig:absorp_beta}
        \end{figure}
  \section{Aufbau und Durchführung}
    Zur Veranschaulichung des Versuchsaufbaus dient Abbildung \ref{fig:versuchsaufbau}.
    Die Intensität kann mit einem Geiger-Müller-Zählrohr vermessen werden.
    Die Messzeit hinter dem Verstärker des Geiger-Müller-Signals wird durch einen netzfrequenzgesteuerten Zeitgeber bestimmt.
    Zu Abschirmung ist das Experiment durch eine Bleiwand umringt.
    Es stehen Platten verschiedener Dicken $D$ aus den zu untersuchenden Materialien zur Verfügung.
    Die Aluminium-Platten für die $\particle{\beta}$-Strahlung haben bekannte Dicken, die Dicken der Platten für $\particle{\gamma}$-Strahlung müssen mit einer Schieblehre vermessen werden.
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/versuchsaufbau.pdf}
      \caption{Versuchsaufbau \cite{anleitung704}}
      \label{fig:versuchsaufbau}
    \end{figure}

    Bevor die Probe eingesetzt wird, wird die Nullrate gemessen, also die Anzahl $Z_0$ der Impulse in der Zeit $T_0 = \SI{900}{\second}$.
    Die Probe wird in die Apparatur eingesetzt und die Zählrate bei verschiedenen Platten gemessen.
    Dabei muss die Messzeit $T$ jeweils so gewählt werden, dass eine hinreichend hohe Anzahl $Z$ an Impulse registriert wird, um eine genaue Messung zu ermöglichen.
  \section{Messwerte}
    Die verwendeten Naturkonstanten stehen in Tabelle \ref{tab:constants}.
    \begin{table}
      \input{table/constants.tex}
      \caption{Verwendete Konstanten \cite{scipy}}
      \label{tab:constants}
    \end{table}
    \FloatBarrier
    \subsection{\texorpdfstring{$\particle{\gamma}$-Strahlung}{γ-Strahlung}}
      Der verwendete Wert von $\epsilon$ und die Ergebnisse der Nullratenmessung stehen in Tabelle \ref{tab:gamma-data}.
      Die Ordnungszahlen $z$, Literaturwerte der molaren Masse $M$ \cite{nist-chemistry-webbook} und der Dichte $\rho$ \cite{nist-star} sowie die Messwerte stehen in den Tabellen \ref{tab:Pb-data} für Blei und \ref{tab:Zn-data} für Zink.
      Dabei ist $D$ die Dicke der verwendeten Platte, $T$ die Messzeit und $Z$ die Anzahl der gemessenen Impulse.
      \begin{table}
        \input{table/gamma-data.tex}
        \caption{Nullratenmessung}
        \label{tab:gamma-data}
      \end{table}
      \begin{table}
        \input{table/Pb-data.tex}
        \caption{Literatur- und Messwerte für Blei}
        \label{tab:Pb-data}
      \end{table}
      \begin{table}
        \input{table/Zn-data.tex}
        \caption{Literatur- und Messwerte für Zink}
        \label{tab:Zn-data}
      \end{table}
      \FloatBarrier
    \subsection{\texorpdfstring{$\particle{\beta}$-Strahlung}{β-Strahlung}}
      Die Messwerte sowie ein Literaturwert für die Dichte $\rho_\ce{Al}$ von Aluminium \cite{nist-star} stehen in Tabelle \ref{tab:beta-data}.
      \begin{table}
        \input{table/beta-data.tex}
        \caption{Messwerte für $\particle{\beta}$-Strahlung}
        \label{tab:beta-data}
      \end{table}
  \section{Auswertung}
    \input{statistics.tex}
    \input{poisson.tex}
    \input{linregress.tex}
    \subsection{\texorpdfstring{$\particle{\gamma}$-Strahlung}{γ-Strahlung}}
      Für die Zählrate gilt
      \begin{eqn}
        N = \frac{Z}{T} - \frac{Z_0}{T_0} ,
      \end{eqn}
      wobei schon die Nullrate abgezogen wird.
      Um den Absorptionskoeffizienten $\mu$ zu bestimmen, wird eine lineare Regression
      \begin{eqn}
        \ln.N. = A D + B
      \end{eqn}
      verwendet.
      Damit gilt für $\mu$ und die Zählrate $N_0$ ohne Platten
      \begin{eqns}
        \mu &=& - A \\
        N_0 &=& \E^B .
      \end{eqns}
      Der Theoriewert $\mu_\t{Comp}$ kann über \eqref{eqn:mu_Comp} berechnet werden.
      Die Ergebnisse der linearen Regression und die berechneten Werte von $\mu$, $N_0$ und $\mu_\t{Comp}$ stehen in den Tabellen \ref{tab:Pb-calc} für Blei und \ref{tab:Zn-calc} für Zink.
      Die Messwerte und Regressionsgeraden sind in der Abbildung \ref{fig:gamma} abgebildet.
      \begin{table}
        \input{table/Pb-calc.tex}
        \caption{Ergebnisse der linearen Regression und berechnete Werte für Blei}
        \label{tab:Pb-calc}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/gamma.pdf}
        \caption{Messwerte und Regressionsgeraden}
        \label{fig:gamma}
      \end{figure}
      \begin{table}
        \input{table/Zn-calc.tex}
        \caption{Ergebnisse der linearen Regression und berechnete Werte für Zink}
        \label{tab:Zn-calc}
      \end{table}
      \FloatBarrier
    \subsection{\texorpdfstring{$\particle{\beta}$-Strahlung}{β-Strahlung}}
      Für die Zählrate $N$ und die Massenbelegung $R$ gelten
      \begin{eqns}
        N &=& \frac{Z}{T} - \frac{Z_0}{T_0} \\
        R &=& \rho_\ce{Al} D .
      \end{eqns}
      Für die Auswertung sollten zwei lineare Regressionen
      \begin{eqn}
        \ln.N. = A_i D + B_i
      \end{eqn}
      durchgeführt werden.
      Für den Schnittpunkt $D_\t{max}$ gilt
      \begin{eqn}
        D_\t{max} = \frac{B_2 - B_1}{A_1 - A_2}
      \end{eqn}
      und damit für die maximale Reichweite
      \begin{eqn}
        R_\t{max} = \rho_\ce{Al} D_\t{max}
      \end{eqn}
      Damit kann die maximale Energie $E_\t{max}$ nach \eqref{eqn:E_max} berechnet werden.
      Da aber die Nullrate größer als die letzten vier gemessenen Nullraten ist, ist die zweite lineare Regression nicht sinnvoll.
      Um einen groben Wert für die maximale Energie zu bekommen, wird eine konstante Funktion durch den verbleibenden Messwert bei $D \approx \SI{300}{\micro\meter}$ gelegt, also die Gerade
      \begin{eqn}
        \ln.N. = A_2 D + B_2
      \end{eqn}
      mit $A_2 = 0$.
      Die Ergebnisse der Rechnung sind in Tabelle \ref{tab:beta-calc} aufgeführt.
      Die Messwerte und die Gerade sind in Abbildung \ref{fig:beta} dargestellt, wobei die negativen Messwerte aufgrund der logarithmischen Skala nicht aufgetragen sind.
      \begin{table}
        \input{table/beta-calc.tex}
        \caption{Ergebnisse der Rechnung}
        \label{tab:beta-calc}
      \end{table}
      \begin{figure}
        \includegraphics[width=\textwidth]{graph/beta.pdf}
        \caption{Messwerte und Geraden}
        \label{fig:beta}
      \end{figure}
  \section{Diskussion}
    Der gemessene Absorptionskoeffizient für Blei ist um circa $\SI{47.2}{\percent}$ größer als der theoretisch berechnete für den Compton-Effekt.
    Dies bedeutet, dass bei der Energie der $\particle{\gamma}$-Strahlung, die hier verwendet wurde, die anderen in der Theorie genannten Effekte eine große Rolle spielen.
    Cäsium-134 hat eine Emissionsenergie von $\SI{1.6}{\mega\electronvolt}$ \cite{cesium}.
    Für $E_\particle{\gamma} = \SI{1.6}{\mega\electronvolt}$ gilt \cite{nist-xcom}
    \begin{eqns}
      R_\t{Comp} &=& \SI{1.186 e-3}{\square\centi\meter\per\gram} + \SI{3.936 e-2}{\square\centi\meter\per\gram} \\
      R_\t{Photo} &=& \SI{7.408 e-3}{\square\centi\meter\per\gram} \\
      R_\t{Paarbildung} &=& \SI{2.520 e-3}{\square\centi\meter\per\gram} .
    \end{eqns}
    Dies bedeutet, dass der Compton-Effekt um eine Größenordnung wichtiger als die anderen Effekte ist.
    Der Photoeffekt ist drei mal stärker als die Paarbildung.
    Bei Zink ist der gemessene Wert von $\mu$ um \SI{6}{\percent} kleiner als der theoretische Wert für den Compton-Effekt.
    Daraus folgt zum Einen, dass bei Zink und der verwendeten Energie nur der Compton-Effekt eine Rolle spielt, und zum Anderen, dass entweder ein systematischer Fehler nach unten vorliegen muss, oder dass der Fehler zu klein geschätzt wurde.

    Die Messung mit $\particle{\beta}$-Strahlung war nicht erfolgreich, da die vorher gemessene Nullrate größer ist, als vier der Messwerte mit Platte.
    Der Grund könnte darin liegen, dass die Platte vor dem Detektor die Nullrate absenkt, also die angenommene, ohne Platte gemessene, Nullrate zu groß ist.
    Eine Verbesserung des Versuchs wäre es, die Nullrate bei jeder Platte zu messen, um diesen Effekt zu vermeiden.
    Dies ist aber mit erhablich mehr Aufwand verbunden.
    Der Effekt trat bei der $\particle{\gamma}$-Strahlung nicht, oder nur geringfügig, auf, da die Platten einen größeren Abstand zur Detektoröffnung hatten, die Nullrate also weniger abgeschirmt wurde.
    Der sehr grobe Wert von $E_\t{max}$ stimmt ungefähr mit dem Literaturwert von $E_\t{mittel, lit} = \SI{85}{\kilo\electronvolt}$ \cite{technetium} für die mittlere Energie überein.
    Der Literaturwert für die Maximalenergie von $E_\t{max, lit} = \SI{294}{\kilo\electronvolt}$ \cite{technetium} stimmt gut mit den ermittelten Messwert überein.
  \makebibliography
\end{document}
