from labtools import *

g = np.loadtxt(lt.file('data/g'), unpack=True)
g *= 1e-9
g = unc.ufloat((g[0], g[1]))

lambda_l, lambda_r, delta_t_l, delta_t_r = np.loadtxt(lt.file('data/dublett_eichung'), unpack=True)
lambda_l = lambda_l * 1e-9
lambda_r = lambda_r * 1e-9

delta_t = lt.mean([delta_t_l, delta_t_r], axis=0)

zeta = unc.unumpy.fabs((lambda_l - lambda_r) / delta_t / unc.unumpy.cos(0.5 * unc.unumpy.arcsin(lambda_l / g) + unc.unumpy.arcsin(lambda_r / g)))
zeta_mean = lt.mean(zeta)

t = lt.SymbolColumnTable()
t.add_si_column(r"\lambda_\t{l}", lambda_l * 1e9, r"\nano\meter", places=0)
t.add_si_column(r"\lambda_\t{r}", lambda_r * 1e9, r"\nano\meter", places=0)
t.add_si_column(r"t_\t{l}", delta_t_l, r"$\zeta$", places=0)
t.add_si_column(r"t_\t{r}", delta_t_r, r"$\zeta$", places=0)
t.add_si_column(r"\zeta", lt.vals(zeta) * 1e9, r"\nano\meter", places=3)
t2 = lt.SymbolRowTable()
t2.add_si_row(r"\mean{\zeta}", zeta_mean * 1e9, r"\nano\meter")
t3 = lt.Table()
t3.add(lt.Row())
t3.add_hrule()
t3.add(lt.Row())
t3.add(lt.Column([t, t2], "@{}c@{}"))
t3.savetable(lt.new("table/eichung.tex"))

np.savetxt(lt.new('result/zeta'), (lt.vals(zeta_mean), lt.stds(zeta_mean)))
