from labtools import *

lambda_, phi_l, phi_r = np.loadtxt(lt.file('data/phi-lambda'), unpack=True)

@np.vectorize
def phi_shift(phi_l, phi_r):
    if phi_l > phi_r:
        return  (360 - phi_l + phi_r) / 2
    else:
        return  (phi_r - phi_l) / 2

phi = phi_shift(phi_l, phi_r)
phi_rad = np.radians(phi)

slope, intercept = lt.linregress(lambda_, np.sin(phi_rad))
g = 1 / slope

t = lt.SymbolColumnTable()
t.add(lt.Column([["dunkelrot & schwach",
                  "rot & stark",
                  "gelb & stark",
                  "grün & schwach",
                  "grün & stark",
                  "blaugrün & mittel",
                  "blau & stark",
                  "violett & stark",
                  "violett & schwach"]], "l l", header=r"Farbe & Intensität"))
t.add_si_column(r"\lambda", lambda_, r"\nano\meter",places=1)
t.add_si_column(r"\phi_\t{l}", phi_l, r"\degree", places=1)
t.add_si_column(r"\phi_\t{r}", phi_r, r"\degree", places=1)
t.add_si_column(r"\phi", phi, r"\degree", places=2)
t.savetable(lt.new("table/He_beugung.tex"))

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
x = np.linspace(400, 750, 10000)
ax.plot(x, lt.vals(slope) * x + lt.vals(intercept), 'r-', label='Regressionsgerade')
ax.plot(lambda_ , np.sin(phi_rad), 'bx', label='Messwerte')
ax.xaxis.set_major_formatter(lt.SiFormatter(0))
ax.yaxis.set_major_formatter(lt.SiFormatter(2))
ax.set_ylabel(r'$\sin.\phi.$')
ax.set_xlabel(r'\silabel{\lambda}{\nano\meter}')
lt.ax_reverse_legend(ax, 'lower right')
fig.tight_layout()
fig.savefig(lt.new('graph/beugung.tex'), bbox='tight', pad_inches=0)

t = lt.SymbolRowTable()
t.add_si_row("A", slope * 1e3 , r"\per\pico\meter")
t.add_si_row("B", intercept)
t.add_si_row("g", g, r"\nano\meter")
t.savetable(lt.new("table/gitterkonstante.tex"))
