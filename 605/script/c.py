from labtools import *

alpha = sp.constants.physical_constants['fine-structure constant'][0]
R_infty = sp.constants.physical_constants['Rydberg constant times hc in J'][0]

g = np.loadtxt(lt.file('data/g'), unpack=True)
g *= 1e-9
g = unc.ufloat((g[0], g[1]))

zeta = np.loadtxt(lt.file('result/zeta'))
zeta = unc.ufloat((zeta[0], zeta[1]))

t = lt.SymbolRowTable()
t.add_si_row(r"\alpha", alpha * 1e3, exp="e-3", places=4)
t.add_si_row(r"R_\infty", R_infty * 1e18, r"\atto\joule", places=4)
t.add_si_row("g", g * 1e9, r"\nano\meter")
t.savetable(lt.new("table/konstanten.tex"))

Ka_ge_d_1 = np.loadtxt(lt.file('data/Ka_ge_w_1'), unpack=True)
Ka_ge_d_2 = np.loadtxt(lt.file('data/Ka_ge_w_2'), unpack=True)
Ka_gr_d_1 = np.loadtxt(lt.file('data/Ka_gr_w_1'), unpack=True)
Ka_gr_d_2 = np.loadtxt(lt.file('data/Ka_gr_w_2'), unpack=True)
Na_ge_d = np.loadtxt(lt.file('data/Na_ge_w'), unpack=True)
Na_gr_d = np.loadtxt(lt.file('data/Na_gr_w'), unpack=True)
Na_r_d = np.loadtxt(lt.file('data/Na_r_w'), unpack=True)
Rb_r_d = np.loadtxt(lt.file('data/Rb_r_w'), unpack=True)

Ka_ge_ab_12 = np.loadtxt(lt.file('data/Ka_ge_12_ab'), unpack=True)
Ka_ge_ab_34 = np.loadtxt(lt.file('data/Ka_ge_34_ab'), unpack=True)
Ka_gr_ab_12 = np.loadtxt(lt.file('data/Ka_gr_12_ab'), unpack=True)
Ka_gr_ab_34 = np.loadtxt(lt.file('data/Ka_gr_34_ab'), unpack=True)
Na_ge_ab = np.loadtxt(lt.file('data/Na_ge_ab'), unpack=True)
Na_gr_ab = np.loadtxt(lt.file('data/Na_gr_ab'), unpack=True)
Na_r_ab = np.loadtxt(lt.file('data/Na_r_ab'), unpack=True)
Rb_r_ab = np.loadtxt(lt.file('data/Rb_r_ab'), unpack=True)

elem = [r"\ce{Ka} & gelb (1, 2)",
        r"\ce{Ka} & gelb (3, 4)",
        r"\ce{Ka} & grün (1, 2)",
        r"\ce{Ka} & grün (3, 4)",
        r"\ce{Na} & gelb",
        r"\ce{Na} & grüngelb",
        r"\ce{Na} & rot",
        r"\ce{Rb} & rot"]

delta = np.array([Ka_ge_d_1,
                  Ka_ge_d_2,
                  Ka_gr_d_1,
                  Ka_gr_d_2,
                  [Na_ge_d] * 2,
                  [Na_gr_d] * 2,
                  [Na_r_d] * 2,
                  [Rb_r_d] * 2])

ab = np.array([Ka_ge_ab_12,
               Ka_ge_ab_34,
               Ka_gr_ab_12,
               Ka_gr_ab_34,
               Na_ge_ab,
               Na_gr_ab,
               Na_r_ab,
               Rb_r_ab])

t = lt.Table("c l", headerRow=lt.Row('Element & Dublett'))
for e in elem:
    t.add(lt.Row(e))
c = lt.SiColumn([], r"\multicolumn{1}{c}{$\delta\:/\:\si{\degree}$}")
for d in delta:
    cell = []
    if np.shape(d) == ():
        cell.append(lt.round_places(str(d), 1))
    else:
        for v in d:
            cell.append(lt.round_places(str(v), 1))
    c.add_cell(cell)
t.add(c)
c = lt.SiColumn([], r"\multicolumn{1}{c}{$s\:/\:\si{$\zeta$}$}")
for d in ab:
    cell = []
    if np.shape(d) == ():
        cell.append(lt.round_places(str(d), 0))
    else:
        for v in d:
            cell.append(lt.round_places(str(v), 0))
    c.add_cell(cell)
t.add(c)
t.savetable(lt.new("table/messung_dublett.tex"))

delta_mean = lt.mean(delta, axis=1)
ab_mean = lt.mean(ab, axis=1)

@np.vectorize
def converter(angle):
    return (400 - angle) / 2 - 90

lambda_ = unc.unumpy.fabs(unc.unumpy.sin(unc.unumpy.radians(converter(delta_mean))) * g)
delta_lambda = unc.unumpy.cos(unc.unumpy.radians(converter(delta_mean))) * ab_mean * zeta
delta_E_D = sp.constants.h * sp.constants.c * delta_lambda / lambda_**2
z = np.array([19, 19, 19, 19, 11, 11, 11, 37])
n = np.array([4, 4, 4, 4, 3, 3, 3, 5])
sigma_2 = z - (delta_E_D * 2 * n**3 / R_infty / alpha**2)**(0.25)

t = lt.SymbolColumnTable()
t.add(lt.Column([elem], "c l", header="Element & Dublett"))
t.add_si_column("n", n, places=0)
t.add_si_column("z", z, places=0)
t.add_si_column(r"\lambda", lt.vals(lambda_) * 1e9, r"\nano\meter", places=0)
t.add_si_column(r"\Del{\lambda}", lt.vals(delta_lambda) * 1e9, r"\nano\meter", places=2)
t.add_si_column(r"\Del{E_\t{D}}", lt.vals(delta_E_D) * 1e21, r"\zepto\joule", places=3)
t.add_si_column(r"\sigma_2", sigma_2)

t2 = lt.SymbolRowTable()
t2.add_si_row(r"\sigma_{2, \ce{Ka}}", lt.mean(lt.vals(sigma_2[:4])))
t2.add_si_row(r"\sigma_{2, \ce{Na}}", lt.mean(lt.vals(sigma_2[4:7])))
t2.add_si_row(r"\sigma_{2, \ce{Rb}}", sigma_2[7:8])

t3 = lt.Table()
t3.add(lt.Row())
t3.add_hrule()
t3.add(lt.Row())
t3.add(lt.Column([t, t2], "@{}c@{}"))
t3.savetable(lt.new("table/sigma.tex"))
