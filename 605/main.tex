\input{header/report.tex}

\SetExperimentNumber{605}

\begin{document}
  \maketitlepage{Die Spektren der Alkali-Atome}{10.04.2012}{17.04.2012}
  \section{Ziel}
    Ziel des Versuchs ist es, mit Hilfe von sichtbaren Emissionsspektren, Informationen über den äußeren Schalenaufbau zu gewinnen und dabei insbesondere die Abschirmungszahl $\sigma_2$ für verschiedene Alkali-Atome zu bestimmen.
  \section{Theorie}
    Die Alkali-Atome besitzen auf der äußersten Schale nur ein Elektron.
    Dieses erzeugt die Spektrallinien im sichtbaren Bereich und wird Leuchtelektron genannt.
    Das Atom wird von der (zeitunabhängingen) Schrödingergleichung
    \begin{eqn}
      \f{H}(\v{r}) \f{\psi}(\v{r}, t) = E \f{\psi}(\v{r}, t)
    \end{eqn}
    beschrieben, wobei $H$ der Hamiltonoperator
    \begin{eqn}
      \f{H}(\v{r}) = \sum_i \frac{\v{p}_i^2}{2 m_i} + \f{V}(\v{r}),
    \end{eqn}
    $\psi$ die Wellenfunktion des Atoms, $E$ die Energie des Atoms, $\v{p}_i$ und $m_i$ der Impulsoperator und die Masse des $i$-ten Teilchens des Atoms und $\f{V}(\v{r})$ das Potential an der Stelle $\v{r}$ sind.

    In der Ein-Elektron-Näherung wird nur das Leuchtelektron betrachtet und die anderen Elektronen und der Atomkern werden als effektiver Kern genähert.
    Dann gilt mit
    \begin{eqns}
      V &=& - \frac{\g(z - \sigma) e^2}{4 \PI \epsilon_0 r}
      \itext{und}
      \v{p} &=& \frac{\hbar}{\I} \nabla
    \end{eqns}
    \begin{eqn}
      \g(- \frac{\hbar^2}{2 m_\t{e}} \Laplacian; - \frac{\g(z - \sigma) e^2}{4 \PI \epsilon_0 r}) \psi = E \psi ,
    \end{eqn}
    wobei $z$ die Ladungszahl des Kerns und $\sigma$ die Abschirmungszahl ist, die aus der Abschirmung des Kerns durch die Elektronenhülle resultiert.
    Mit $e$, $\epsilon_0$ und $\hbar$ werden die Elementarladung, die elektrische Feldkonstante und das Planck'sche Wirkungsquantum bezeichnet.
    Wegen der Kugelsymmetrie des Problems kann der Laplace-Operator in Kugelkoordinaten durch den Drehimpulsoperator $\v{L}$ ausgedrückt werden.
    Für die Schrödingergleichung ergibt sich
    \begin{eqn}
      \g(- \frac{\hbar^2}{2 m_\t{e}} \g(\frac{2}{r} \p{r}; + \p{r}[2];) + \frac{1}{2 m_\t{e}} \frac{1}{r^2} \v{L}^2 - \frac{\g(z - \sigma) e^2}{4 \PI \epsilon_0} \frac{1}{r}) \psi = E \psi .
    \end{eqn}
    Die Eigenfunktionen von $\v{L}^2$ sind die Kugelflächenfunktionen $\SphHarmY{\ell}{m};$ zu den Eigenwerten $\hbar^2 \ell \g(\ell + 1)$, wobei $\ell$ die Drehimpulsquantenzahl ist.
    Mit
    \begin{eqn}
      \f{\psi}(r, \theta, \phi) = \f{R}(r) \SphHarmY{\ell}{m}(\theta, \phi)
    \end{eqn}
    ergibt sich die Gleichung
    \begin{eqn}
      \g(- \frac{\hbar^2}{2 m_\t{e}} \g(\frac{2}{r} \p{r}; + \p{r}[2];) + \frac{\hbar^2}{2 m_\t{e}} \ell \g(\ell + 1) \frac{1}{r^2} - \frac{\g(z - \sigma) e^2}{4 \PI \epsilon_0} \frac{1}{r}) \f{R}(r) = E \f{R}(r) ,
    \end{eqn}
    die von den Eigenwerten
    \begin{eqns}
      E_{\ell, k} &=& - R_\infty \frac{\g(z - \sigma)^2}{\g(\ell + k + 1)^2} , \quad k \in \setN_0 \\
    \end{eqns}
    gelöst wird.
    Die Rydberg-Energie $R_\infty$ ist als
    \begin{eqn}
      R_\infty = \frac{m_\t{e} e^4}{8 h^2 \epsilon_0^2}
    \end{eqn}
    definiert.

    Werden auch noch relativistische Effekte berücksichtigt, so wird der Hamiltonoperator
    \begin{eqn}
      H = \sqrt{m_\t{e}^2 c^4 + c^2 \v{p}^2} - m_\t{e} c^2 + V
    \end{eqn}
    verwendet.
    Es ergeben sich die Energien
    \begin{eqns}
      E_{n, \ell} &=& E_n - \frac{1}{8 m_\t{e}^3 c^2} \braket{\psi}[\v{p}^4]{\psi} \\
      &=& - R_\infty \g(\frac{\g(z - \sigma)^2}{n^2} + \alpha^2 \frac{\g(z - \sigma)^4}{n^3} \g(\frac{2}{2 \ell + 1} - \frac{3}{4n})) ,
    \end{eqns}
    wobei $\alpha$ die Sommerfeld'sche Feinstrukturkonstante
    \begin{eqn}
      \alpha = \frac{e^2}{2 h c \epsilon_0}
    \end{eqn}
    ist.
    Für jedes $n$ existiert für jedes $\ell$ ein Unterniveau, das Energieniveau zur Hauptquantenzahl $n$ wird also in $n$ Unterniveaus aufgespalten.

    Das Elektron besitzt noch einen Spin $\v{S}$, also einen eigenen Drehimpuls, der mit einem magnetischen Moment verknüpft ist, das mit dem Bahndrehimpuls $\v{L}$ wechselwirkt.
    Dieses Phänomen wird Spin-Bahn-Kopplung genannt.
    Die Drehimpulse addieren sich zum Gesamtdrehimpuls $\v{J} = \v{L} + \v{S}$, wobei $\v{S}$ nur parallel oder anti-parallel zu $\v{L}$ sein kann.
    Für den Hamiltonoperator ergibt sich
    \begin{eqn}
      H = \frac{1}{2 m_\t{e}^2 c^2} \frac{1}{r} \d{r}{V}; \Dot{\v{S}}{\v{L}} - \frac{\g(z - \sigma) e^2 \hbar^2}{32 \PI \epsilon_0 m_\t{e}^2 c^2} \Div{\frac{\v{r}}{r^3}} .
    \end{eqn}
    Für die Energien gilt jetzt
    \begin{eqns}
      E_{n, \ell, j} &=& E_n + \braket{\psi}[H]{\psi} \\
      &=& - R_\infty \g(\frac{\g(z - \sigma)^2}{n^2} + \alpha^2 \frac{\g(z - \sigma)^4}{n^3} \g(\frac{2}{2 \ell + 1} - \frac{3}{4 n})) \\
      && {} + R_\infty \alpha^2 \frac{\g(z - \sigma)^4}{n^3}
      \begin{cases}
        \frac{j \g(j + 1) - \ell \g(\ell + 1) - \frac{3}{4}}{\ell \g(\ell + 1) \g(2 \ell + 1)} , & \ell \neq 0 \\
                1 , & \ell = 0
      \end{cases} \\
      &=& - R_\infty \frac{\g(z - \sigma)^2}{n^2} - R_\infty \alpha^2 \frac{\g(z - \sigma)^4}{n^3}
      \begin{cases}
        \frac{1}{j + \frac{1}{2}} - \frac{3}{4 n} , & \ell \neq 0 \\
        1 - \frac{3}{4 n} , & \ell = 0
      \end{cases} .
    \end{eqns}
    Diese Beziehung wird als Sommerfeld'sche Feinstrukturformel bezeichnet.

    Für Übergänge von Elektronen zwischen den Zuständen gilt, dass sich der Bahndrehimpuls um 1 ändern muss, also $\Del{\ell} = \pm 1$.
    Alle mit dieser Regel kompatiblen Übergänge sind möglich, aber die mit $\Del{j} = 0$ sind sehr unwahrscheinlich.
    Die Wahrscheinlichkeit eines Übergangs nimmt auch mit wachsendem $\Del{n}$ ab.
    Wegen des geringen Energieunterschieds der Niveaus mit gleichem $\ell$ aber unterschiedlichem $j$ werden immer zwei Linien nebeneinander beobachtet, die Dublett genannt werden.

    Eine genauere Näherung ergibt sich, wenn die Abschirmungszahl $\sigma$ durch zwei Abschirmungszahlen $\sigma_1$ und $\sigma_2$ ersetzt wird.
    Jetzt ergibt sich für die Energien
    \begin{eqn}
      E_{n, \ell, j} = - R_\infty \frac{\g(z - \sigma_1)^2}{n^2} - R_\infty \alpha^2 \frac{\g(z - \sigma_2)^4}{n^3}
      \begin{cases}
        \frac{1}{j + \frac{1}{2}} - \frac{3}{4 n} , & \ell \neq 0 \\
        1 - \frac{3}{4 n} , & \ell = 0
      \end{cases} .
    \end{eqn}
    Die Abschirmungszahl $\sigma_1$ ist die Konstante der vollständigen Abschirmung und beschreibt den Einfluss aller Elektronen auf das elektrische Feld.
    Die Konstante der inneren Abschirmung beschreibt den Einfluss aller Elektronen, die auf Schalen unterhalb des Leuchtelektrons liegen.
    Sie ist kleiner als $\sigma_1$, aber der Unterschied macht sich nur bei der Betrachtung innerer Schalen bemerkbar.
    Die Konstante der inneren Abschirmung kann durch Messung der Energie der Linien eines Dubletts bestimmt werden.
    Für den Energieunterschied ergibt sich die Formel
    \begin{eqn}
      \Del{E_\t{D}} \coloneq E_{n, \ell, j} - E_{n, \ell, j + 1} = \frac{R_\infty \alpha^2}{n^3} \frac{\g(z - \sigma_2)^4}{\ell \g(\ell + 1)} ,
    \end{eqn}
    die nach $\sigma_2$ umgeformt werden kann
    \begin{eqn}
      \sigma_2 = z - \sqrt[4]{\frac{\Del{E_\t{D}} n^3 \ell \g(\ell + 1)}{R_\infty \alpha^2}} .
    \end{eqn}
    Mit der Energie des Photons
    \begin{eqns}
      E &=& \frac{h c}{\lambda}
      \itext{ergibt sich}
      \Del{E_\t{D}} &\approx& h c \frac{\Del{\lambda}}{\lambda^2} , \eqlabel{eqn:delta_E}
      \itext{und damit}
      \sigma_2 &=& z - \sqrt[4]{\frac{h c n^3 \ell \g(\ell + 1)}{R_\infty \alpha^2} \frac{\Del{\lambda}}{\lambda^2}} . \eqlabel{eqn:sigma}
    \end{eqns}

    Im Experiment wird ein Beugungsgitter zur Wellenlängenbestimmung verwendet.
    Das Gitter ist durch die Spaltbreite $b$ und der Gitterkonstante $g$, also dem Abstand zwischen den Spaltmitten bestimmt.
    Die Spaltbreite muss in der Größenordnung der Wellenlänge sein, um Beugung zu ermöglichen.
    Durch Interferenz entstehen beobachtbare Maxima und Minima der Lichtintensität, die aber wegen der abnehmenden Intensität nur bis zur ersten Ordnung sichtbar sind.
    Die Maxima befinden sich an den Stellen
    \begin{eqn}
      \sin(\phi) = k \frac{\lambda}{g} , \quad k \in \setN_0 , \eqlabel{eqn:maxima}
    \end{eqn}
    wobei $\phi$ von der nullten Ordnung in der Mitte aus gemessen wird.
  \section{Aufbau}
    Der Aufbau des Versuchs (Abbildung \ref{fig:aufbau}) besteht im Wesentlichen aus einem Gitterspektralapparat und den verschiedenen Lampen, die aus den zu untersuchenden Elementen bestehen.
    Ein Gitterspektralapparat ist aus einem Kollimator mit einer Blende, einem Gitter, einem schwenkbaren Fernrohr und einem Goniometer aufgebaut.
    In dem Versuch wurden wegen Ausfalls der ersten Apparatur zwei Apparaturen verwendet, erstens mit einem Transmissionsgitter und zweitens mit einem Reflexionsgitter.
    Der Beugungswinkel der im Fernrohr sichtbaren Linie kann am Goniometer abgelesen werden.
    Außerdem ist im Okular des Fernrohrs ein Fadenkreuz in Kombination mit einer Mikrometerschraube angebracht.
    Dies dient zur genaueren Winkelbestimmung, muss aber vorher geeicht werden.
    \begin{figure}
      \includegraphics{graphic/aufbau.pdf}
      \caption{Versuchsaufbau \cite{anleitung605}}
      \label{fig:aufbau}
    \end{figure}
  \section{Durchführung}
    Vor der Messung muss die Apparatur so justiert werden, dass die Linien scharf zu beobachten sind.
    Hierzu sind verschiedene Schrauben an der Apparatur angebracht.

    Als erstes werden die Beugungswinkel von bekannten Linien des Heliums gemessen, um daraus die unbekannte Gitterkonstante $g$ zu berechnen.
    Dann wird das Okularmikrometer geeicht, indem die Winkeldifferenz verschiedener bekannter Linien, sowie deren Abstand mit dem Okular emessen wird.
    Als drittes werden die Beugungswinkel und Abstände von Dublettlinien verschiedener Alkali-Atome gemessen, um ihre Abschirmungskonstanten $\sigma_2$ zu berechnen.
    Hier wird der Beugungswinkel der Mitte der Linien mit dem Goniometer und der Abstand mit dem Okularmikrometer bestimmt.
  \section{Messwerte und Auswertung}
    \input{statistics.tex}
    \input{linregress.tex}
    \subsection{Ermittlung der Gitterkonstante}
      In Tabelle \ref{tab:mes-He_beug} sind die Messwerte für die Beugungswinkel $\phi$ der $\ce{He}$\-/Spektrallinien aufgelistet.
      Diese Werte wurden an einem Transmissionsgitter aufgenommen und sind in Abbildung \ref{fig:plot} geplottet.
      
      \begin{table}
        \input{table/He_beugung.tex}
        \caption{Beugungswinkel für die $\ce{He}$-Spektrallinien}
        \label{tab:mes-He_beug}
      \end{table}
      Die lineare Regression ergibt für die Gitterkonstante, die sich laut Gleichung \eqref{eqn:maxima} mit $k = 1$ (nur Maxima erster Ordnung sind beobachtbar) aus dem Reziproken von $A$ bestimmt, einen Wert, der in Tabelle \ref{tab:g} zusammen mit den ermittelten Fitparametern, sowie allen statistischen Fehlern aufgelistet ist.
      \begin{figure}
        \includesgraphics{graph/beugung.pdf}
        \caption{Plot der Messwerte und der Regressionsgeraden zur Bestimmung der Gitterkonstante $g$}
        \label{fig:plot}
      \end{figure}
      \begin{table}
        \input{table/gitterkonstante.tex}
        \caption{Fitparamater und deren Fehler, sowie resultierende Gitterkonstante $g$ mit Fehler}
        \label{tab:g}
      \end{table}
    \subsection{Ermittlung einer Eichgröße für die Skala des Okularmillimeters}
      In Tabelle \ref{tab:konstanten} sind die bei der Auswertung im Folgenden benötigten Konstanten aufgeführt.
      Wegen des Ausfalls der Messapparatur ist in dieser Tabelle ebenfalls die nun verwendete Gitterkonstante der anderen Apparatur, also des Reflexionsgitters aufgeführt.
      In Tabelle \ref{tab:mes_dub} stehen die abgelesenen Winkel $\delta$ des Reflexionsgitters und der ermittelte Abstand $s$ der Dublettlinien in Einheiten der Eichgröße $\zeta$.
      Für den Winkel $\phi$ gilt die Formel
      \begin{eqn}
        \phi = \frac{\g(\SI{400}{\degree} - \delta)}{2} - \SI{90}{\degree} . \eqlabel{eqn:winkel_umrechnung}
      \end{eqn}
      \begin{table}
        \input{table/konstanten.tex}
        \caption{Benötigte Konstanten}
        \label{tab:konstanten}
      \end{table}
      Um den Abstand der Linien in SI-Einheiten zu eichen, wird die Eichgröße $\zeta$ für verschiedene Wellenlängen, vornehmlich im grün-violetten Bereich, da die Linien hier wegen ihres Abstands jeweils paarweise im Okular sichtbar sind, ermittelt.
      Es gilt 
      \begin{eqn}
        \zeta = \frac{\g(\lambda_1 - \lambda_2)}{t \cos(\mean{\phi_{1,2}})},
      \end{eqn}
      wobei die beiden Wellenlängen zu den paarweise beobachtbaren Spektrallinien gehören, $t$ der Abstand in Einheiten von $\zeta$, und der Winkel $\mean{\phi_{1,2}}$ der gemittelte Winkel der Dublettlinien ist.
      \begin{table}
        \input{table/eichung.tex}
        \caption{Daten zur Ermittlung der Eichgröße $\zeta$, sowie dessen Mittelwert}
        \label{tab:eichgröße}
      \end{table}
      Die Eichgröße $\zeta$ und die dafür gemessenen Größen, sowie der Mittelwert von $\zeta$ stehen in Tabelle \ref{tab:eichgröße}.
      Im Folgenden wird mit dem Mittelwert von $\zeta$ gerechnet.
    \subsection{Ermittlung der Abschirmungszahlen}
      Für die Ermittlung der Abschirmungszahl werden die gemittelten Winkel $\delta$ zwischen den Dublettlinien und der mit dem Okularmillimeter ermitteltete Abstand der Dublettlinien $s$ gemessen.
      Die Messwerte sind in Tabelle \ref{tab:mes_dub} aufgeführt.
      \begin{table}
        \input{table/messung_dublett.tex}
        \caption{Abgelesene Winkel $\delta$ und Abstand $s$ der Dublettlinien}
        \label{tab:mes_dub}
      \end{table}
      Die mit Formel \eqref{eqn:winkel_umrechnung} umgerechneteten Winkel, sowie die mittleren Wellenlängen, die mit 
      \begin{eqn}
        \Del{\lambda} = \mean{\zeta} s \cos(\mean{\phi_{1,2}}) , 
      \end{eqn} 
      errechnet wurden, die jeweiligen Energiedifferenzen, ausgehend von Formel \eqref{eqn:delta_E}  und schließlich die nach Formel \eqref{eqn:sigma} ermittelten Abschirmungszahlen sind in Tabelle \ref{tab:ergebnisse} aufgelistet.
      \begin{table}
        \OverfullCenter{\input{table/sigma.tex}}
        \caption{Umgerechnete Winkel, mittlere Wellenlängen, Energiedifferenzen und Abschirmungszahlen}
        \label{tab:ergebnisse}
      \end{table}
      Für die jeweiligen Elemente sind nochmals Mittelwerte der Abschirmungszahlen gebildet worden.
  \section{Diskussion}
      Die Messwerte der ersten Messung passen gut mit der Regressionsgeraden zusammen, die Messmethode stellt also ein gutes Verfahren dar, die Gitterkonstante $g$ zu ermitteln.
      Geringe Fehlerquellen sind beim Ablesen oder in der Präzision der Apparatur zu suchen.

      Bei der zweiten Messung ist die Messapparatur ausgefallen, hier mussten vorgegebene Werte verwendet werden.
      Die Größe $\zeta$ eignet sich wegen des kleinen Fehlers gut als Eichgröße zu Bestimmung des Abstandes in SI-Einheiten.

      Bei den Abschirmungszahlen ist auffallend, dass sie mit zunehmender Kernladungszahl zunehmen, da mehrere Elektronenhüllen unter dem äußersten Leuchtelektron liegen.
      Fehlerquellen liegen hier ebenfalls in der Messgenauigkeit, die statistischen Schwankungen sind jedoch gering, was auf eine gute Messung hindeutet.
  \makebibliography
\end{document}
